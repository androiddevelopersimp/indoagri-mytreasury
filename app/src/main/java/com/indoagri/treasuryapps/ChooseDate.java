package com.indoagri.treasuryapps;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.indoagri.treasuryapps.Retrofit.DB.DatabaseQuery;
import com.indoagri.treasuryapps.Retrofit.Model.TreasuryArModel;
import com.indoagri.treasuryapps.widget.calendar.activity.CustomCalendar;

import java.util.ArrayList;
import java.util.List;


public class ChooseDate extends AppCompatActivity implements CustomCalendar.CalendarListener{

    DatabaseQuery query;
    Toolbar toolbar;
    TextView mTextToolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_date);
        toolbar = findViewById(R.id.toolbar);
        query = new DatabaseQuery(ChooseDate.this);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        fab.hide();
        setUpToolbar();
        DataSourceAR();
    }

        private void DataSourceAR(){
        CustomCalendar custom = findViewById(R.id.mycalendar);
        query.openTransaction();
        List<Object> listObject;
        List<TreasuryArModel> listTemp = new ArrayList<TreasuryArModel>();
        listTemp.clear();
        listObject = null;
        String sqldb_query = "SELECT idx,HKONT,GSBER,KUNNR,NAME1,TXT50,KVGR5,BUKRS,WAERS,BEZEI,ERDAT,sum(DEB00) as DEB00,sum(DEB10) as DEB10,sum(DEB20) as DEB20, " +
                "sum(DEB30) as DEB30,sum(DEB60) as DEB60,sum(DEB90) as DEB90, " +
                "sum(DEB99) as DEB99,sum(DEBTT) as DEBTT" +
                " From TREASURY_AR Group By ERDAT";
        listObject = query.getListDataRawQuery(sqldb_query, TreasuryArModel.TABLE_NAME,null);
        query.closeTransaction();
        if(listObject==null){
            Toast.makeText(this, "Data Tidak ada", Toast.LENGTH_SHORT).show();
        }
        else{
            if(listObject.size() > 0){
                for (int i = 0; i < listObject.size(); i++) {
                    TreasuryArModel md = (TreasuryArModel) listObject.get(i);
                    String tgl = md.getErdat().substring(0,10);
                    String message = md.getNamE1();
                    custom.setCalendarListener(this);
                    custom.setEventDate(tgl, message, "All Day");
                }
                custom.refresh();
            }else{
                Toast.makeText(this, "Data Tidak ada", Toast.LENGTH_SHORT).show();
            }
        }

    }
    private void setUpToolbar() {
        mTextToolbar= (findViewById(R.id.txt_Toolbar));
        mTextToolbar.setText("Choose Date");
        toolbar = (findViewById(R.id.toolbar));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed(){
        Bundle mBundle = new Bundle();
        mBundle.putString("tanggal", null);
        mBundle.putString("message","Kirim Tanggal");
        Intent intent=new Intent();
        intent.putExtras(mBundle);
        setResult(99,intent);
        finish();//finishing activity
        }

    @Override
    public void onDayClick(String date) {
        Bundle mBundle = new Bundle();
        mBundle.putString("tanggal", date);
        mBundle.putString("message","Kirim Tanggal");
        Intent intent=new Intent();
        intent.putExtras(mBundle);
        setResult(99,intent);
        finish();//finishing activity
        //Toast.makeText(getApplicationContext(), "Anda Memilih Tanggal " + date.substring(0,10), Toast.LENGTH_SHORT).show();
    }
}
