package com.indoagri.treasuryapps.Common;

public class DateLocal {

    public String getMonthAlphabet(String monthVal){

        int month = new Converter(monthVal).StrToInt();

        switch (month) {
            case 1:
                return "January";
            case 2:
                return "February";
            case 3:
                return "March";
            case 4:
                return "April";
            case 5:
                return "May";
            case 6:
                return "Juni";
            case 7:
                return "July";
            case 8:
                return "Agustus";
            case 9:
                return "September";
            case 10:
                return "October";
            case 11:
                return "November";
            case 12:
                return "December";
            default:
                return "";
        }
    }
}
