package com.indoagri.treasuryapps.SaldoBank;

import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.indoagri.treasuryapps.R;

public class SaldoBankActivity extends AppCompatActivity {

    TabLayout tabLayout;
    ViewPager mViewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_saldo_bank);

        tabLayout = findViewById(R.id.tab_layout);
        mViewPager = findViewById(R.id.tab_pager);

        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        tabLayout.addTab(tabLayout.newTab().setText("Summary"));
        tabLayout.addTab(tabLayout.newTab().setText("IDR"));
        tabLayout.addTab(tabLayout.newTab().setText("USD"));
        tabLayout.addTab(tabLayout.newTab().setText("EUR"));

    }
}
