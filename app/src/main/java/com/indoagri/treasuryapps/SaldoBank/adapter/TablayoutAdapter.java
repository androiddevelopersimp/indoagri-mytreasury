package com.indoagri.treasuryapps.SaldoBank.adapter;

import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.indoagri.treasuryapps.SaldoBank.fragement.SaldoBankFragment;

public class TablayoutAdapter extends FragmentPagerAdapter {

    private int numbOfTab = 0;

    public TablayoutAdapter(@NonNull FragmentManager fm, int numbOfTab) {
        super(fm);
        this.numbOfTab = numbOfTab;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {
        SaldoBankFragment item = new SaldoBankFragment();
        return item.newInstance(position);
    }

    @Override
    public int getItemPosition(@NonNull Object object) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return numbOfTab;
    }
}
