package com.indoagri.treasuryapps.SaldoBank.fragement;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indoagri.treasuryapps.R;

public class SaldoBankFragment extends Fragment {

    private View view;
    private int tabPosition;

    public static SaldoBankFragment newInstance(int tabPosition) {
        SaldoBankFragment fragment = new SaldoBankFragment();
        Bundle args = new Bundle();
        args.putInt("tabname", tabPosition);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            tabPosition = getArguments().getInt("tabname");
        }
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_saldo_bank, container, false);

        return view;
    }
}
