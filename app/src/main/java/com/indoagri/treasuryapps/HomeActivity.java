package com.indoagri.treasuryapps;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.indoagri.treasuryapps.AR.ActivityAR;
import com.indoagri.treasuryapps.Common.Constants;
import com.indoagri.treasuryapps.Common.DeviceUtils;
import com.indoagri.treasuryapps.Retrofit.ApiServices;
import com.indoagri.treasuryapps.Retrofit.DB.DatabaseQuery;
import com.indoagri.treasuryapps.Retrofit.Model.UserModel;
import com.indoagri.treasuryapps.Retrofit.NetClient;
import com.indoagri.treasuryapps.Retrofit.SharePreference;
import com.indoagri.treasuryapps.Retrofit.UsersLoginResponse;
import com.indoagri.treasuryapps.SaldoBank.SaldoBankActivity;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class HomeActivity extends AppCompatActivity implements View.OnClickListener {
    int month = Calendar.getInstance().get(Calendar.MONTH);
    int year = Calendar.getInstance().get(Calendar.YEAR);
    SharePreference sharePreference;
    ApiServices apiServices;
    DatabaseQuery database;
    Spinner spinnerCompany;
    ImageView imgLogout;
    LinearLayout clickAR;
    LinearLayout clickCashEqv;
    LinearLayout clickCashFlow;
    LinearLayout clickExchange;
    LinearLayout clickInsurance;
    LinearLayout clickLoan;
    LinearLayout clickMarket;
    LinearLayout clickPaymnet;
    TextView ValueARTotal;
    boolean isShowing;
    ImageView imgInfoVersion;
    ProgressDialog pDialog;
    UserModel userModel;
    UsersLoginResponse usersLoginResponse;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        database = new DatabaseQuery(HomeActivity.this);
        apiServices = NetClient.DataProduction().create(ApiServices.class);
        spinnerCompany = (findViewById(R.id.spinCompany));
        imgLogout = (findViewById(R.id.imgLogout));
        sharePreference = new SharePreference(HomeActivity.this);
        pDialog = new ProgressDialog(HomeActivity.this);
        isShowing = pDialog.isShowing();
        clickAR = (findViewById(R.id.clickAR));
        clickCashEqv = (findViewById(R.id.clickCashEqv));
        clickCashFlow = (findViewById(R.id.clickCashFlow));
        clickExchange = (findViewById(R.id.clickExchange));
        clickInsurance = (findViewById(R.id.clickInsurance));
        clickLoan = (findViewById(R.id.clickLoan));
        clickMarket = (findViewById(R.id.clickMarket));
        clickPaymnet = (findViewById(R.id.clickPayment));
        clickAR.setOnClickListener(this);
        clickCashEqv.setOnClickListener(this);
        clickCashFlow.setOnClickListener(this);
        clickExchange.setOnClickListener(this);
        clickInsurance.setOnClickListener(this);
        clickLoan.setOnClickListener(this);
        clickMarket.setOnClickListener(this);
        clickPaymnet.setOnClickListener(this);
        ValueARTotal = (TextView) findViewById(R.id.valueareof);
        ValueARTotal.setTextColor(getResources().getColor(R.color.colorOcean));
        imgInfoVersion = (findViewById(R.id.imgInfoVersion));
        if (sharePreference.isConfirmationUpdate()) {
            imgInfoVersion.setVisibility(View.GONE);
        } else {
            imgInfoVersion.setVisibility(View.VISIBLE);
        }
        imgInfoVersion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialogInfoVersion();
            }
        });
        setSpinner();
        imgLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = sharePreference.isUserName();
                String email = sharePreference.isEmail();
                String Domain = sharePreference.isDomain();
                String password = sharePreference.isPassword();
                String DeviceId = DeviceUtils.getDeviceID(HomeActivity.this);
                AsyncLogout(HomeActivity.this, email, "Logout Process Initialitation", password, username, Domain);
            }
        });
        setView();
        TextView txtVersion = (findViewById(R.id.txtVersion));
        txtVersion.setText("Vers. " + Constants.versionApps);
    }

    private void setView() {
        NumberFormat formatterValue = new DecimalFormat("#,###,###,###,###,###");
        String TotalAR = new SharePreference(HomeActivity.this).isTOTALAR();
        double TotalARDouble = Double.parseDouble(TotalAR) / 1000;
        ValueARTotal.setText("Rp. " + formatterValue.format(TotalARDouble));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if (requestCode == 1) {
            String message = data.getStringExtra("MESSAGE");
            NumberFormat formatterValue = new DecimalFormat("#,###,###,###,###,###");
            String TotalAR = new SharePreference(HomeActivity.this).isTOTALAR();
            double TotalARDouble = Double.parseDouble(TotalAR) / 1000;
            ValueARTotal.setText("Rp. " + formatterValue.format(TotalARDouble));
        }
    }

    void setSpinner() {
        ArrayAdapter adapter = ArrayAdapter.createFromResource(
                getApplicationContext(), R.array.domainspinner, R.layout.item_spinner_home);
        adapter.setDropDownViewResource(R.layout.item_spinner_home);
        spinnerCompany.setAdapter(adapter);
        spinnerCompany.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView1, View selectedItemView, int position, long id) {
                // your code here
                String dataCompany = parentView1.getItemAtPosition(position).toString();
                new SharePreference(HomeActivity.this).setFormGroupcompany(dataCompany);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }

    private void Logout() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Are you sure you want to exit?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        sharePreference.setLogin(false);
                        Intent a = new Intent(HomeActivity.this, LoginActivity.class);
                        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        a.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        finish();
                        startActivity(a);
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public void onClick(View v) {

        clickAR.setOnClickListener(this);
        clickCashEqv.setOnClickListener(this);
        clickCashFlow.setOnClickListener(this);
        clickExchange.setOnClickListener(this);
        clickInsurance.setOnClickListener(this);
        clickLoan.setOnClickListener(this);
        clickMarket.setOnClickListener(this);
        clickPaymnet.setOnClickListener(this);
        if (v == clickAR) {
            Intent intent = new Intent(HomeActivity.this, ActivityAR.class);
            startActivityForResult(intent, 1);// Activity is started with requestCode 2
        }
        if (v == clickCashEqv) {
//            Toast.makeText(HomeActivity.this, "CASH EQ", Toast.LENGTH_LONG).show();
            Intent intent = new Intent(HomeActivity.this, SaldoBankActivity.class);
//            startActivityForResult(intent, 1);// Activity is started with requestCode 2
            startActivity(intent);
        }
        if (v == clickCashFlow) {
            Toast.makeText(HomeActivity.this, "CASH FLOW", Toast.LENGTH_LONG).show();
        }
        if (v == clickInsurance) {
            Toast.makeText(HomeActivity.this, "INSURANCE", Toast.LENGTH_LONG).show();
        }
        if (v == clickLoan) {
            Toast.makeText(HomeActivity.this, "LOAN", Toast.LENGTH_LONG).show();
        }
        if (v == clickMarket) {
            Toast.makeText(HomeActivity.this, "MARKET", Toast.LENGTH_LONG).show();
        }
        if (v == clickExchange) {
            Toast.makeText(HomeActivity.this, "EXHANGE RATE", Toast.LENGTH_LONG).show();
        }
        if (v == clickPaymnet) {
            Toast.makeText(HomeActivity.this, "PAYMENT", Toast.LENGTH_LONG).show();
        }
    }

    private void showDialogInfoVersion() {
        final ArrayAdapter adapter = ArrayAdapter.createFromResource(
                getApplicationContext(), R.array.information, R.layout.item_spinner);

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        dialogBuilder.setTitle("Informasi Pembaruan v." + BuildConfig.VERSION_NAME + " ");
        dialogBuilder.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                sharePreference.setKonfirmationUpdate(true);
            }
        });
        dialogBuilder.setAdapter(adapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        //Create alert dialog object via builder
        AlertDialog alertDialogObject = dialogBuilder.create();
        //Show the dialog
        alertDialogObject.show();
    }

    private void AsyncLogout(Context context, final String email, String DialogMessage, String password, final String username, String domain) {
        pDialog = new ProgressDialog(context);
        pDialog.setMessage(DialogMessage);
        pDialog.setIndeterminate(true);
        pDialog.setCancelable(false);
        pDialog.show();
        Log.d("LOGOUT", "loadFirstPage: ");

        getUsersMobile(email, password, username, domain).enqueue(new Callback<UsersLoginResponse>() {
            @Override
            public void onResponse(Call<UsersLoginResponse> call, Response<UsersLoginResponse> response) {
                pDialog.dismiss();
                if (response.code() == 200) {
                    usersLoginResponse = fetchResults(response);
                    userModel = usersLoginResponse.getUserLogout();
                    Logout();
                } else {
                    Log.e("Error", "Logout Error");
                }
            }

            @Override
            public void onFailure(Call<UsersLoginResponse> call, Throwable t) {
                t.printStackTrace();
                pDialog.dismiss();

            }
        });

    }

    private UsersLoginResponse fetchResults(Response<UsersLoginResponse> response) {
        UsersLoginResponse loginResponse = response.body();
        return loginResponse;
    }

    private Call<UsersLoginResponse> getUsersMobile(String email, String password, String username, String domain) {
        return apiServices.LogoutPermission(Constants.APIKEY, username, password, domain, "939328493284983");
    }

    public void showYearDialog() {

        final Dialog d = new Dialog(HomeActivity.this);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.setContentView(R.layout.dialog_month_year);
        Button set = (Button) d.findViewById(R.id.btnOk);
        Button cancel = (Button) d.findViewById(R.id.btnCancel);
        TextView year_text = (TextView) d.findViewById(R.id.year_text);
        year_text.setText("Set Month");
        final NumberPicker monthpicker = (NumberPicker) d.findViewById(R.id.picker_month);
        final NumberPicker yearpicker = (NumberPicker) d.findViewById(R.id.picker_year);

        monthpicker.setMaxValue(12);
        monthpicker.setMinValue(1);
        monthpicker.setWrapSelectorWheel(false);
        monthpicker.setValue(month);
        monthpicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);


        yearpicker.setMinValue(2016);
        yearpicker.setMaxValue(2025);
        yearpicker.setWrapSelectorWheel(false);
        yearpicker.setValue(year);

        set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Months = String.valueOf(monthpicker.getValue() < 10 ? ("0" + monthpicker.getValue()) : (monthpicker.getValue()));
                String Years = String.valueOf(yearpicker);
                new SharePreference(HomeActivity.this).setRefreshMonth(Months);
                new SharePreference(HomeActivity.this).setRefreshYear(Years);
                Toast.makeText(HomeActivity.this, Months + " " + Years, Toast.LENGTH_SHORT).show();
                //   GetDataSalesMonth(Dates);
                d.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });
        d.show();
    }
}
