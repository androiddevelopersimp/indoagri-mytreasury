package com.indoagri.treasuryapps.AR;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextClock;
import android.widget.TextView;
import android.widget.Toast;

import com.indoagri.treasuryapps.ChooseDate;
import com.indoagri.treasuryapps.HomeActivity;
import com.indoagri.treasuryapps.R;
import com.indoagri.treasuryapps.Retrofit.ApiServices;
import com.indoagri.treasuryapps.Retrofit.DB.DatabaseQuery;
import com.indoagri.treasuryapps.Retrofit.Model.TreasuryArModel;
import com.indoagri.treasuryapps.Retrofit.NetClient;
import com.indoagri.treasuryapps.Retrofit.SharePreference;
import com.indoagri.treasuryapps.Retrofit.TreasuryARResponse;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ActivityAR extends AppCompatActivity implements View.OnClickListener{
    int month = Calendar.getInstance().get(Calendar.MONTH);
    int year = Calendar.getInstance().get(Calendar.YEAR);
    private static final String TAGCOMPANY = "GET AR";
    Toolbar toolbar;
    TextView mTextToolbar;
    ImageView imgReload;
    Date today;
    String dateToStr,UpdatedDateToStr;
    ApiServices apiServices;
    ProgressBar progressBar;
    TreasuryARResponse treasuryARResponse;
    List<TreasuryArModel>treasuryArModels;
    DatabaseQuery DBquery;
    TextView lastUpdateTime,companyGroup,txtInfoDate;
    EditText et_year;
    private SimpleDateFormat dateFormatter;
    SimpleDateFormat formatUpdate = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
    SimpleDateFormat formatUpdateDated = new SimpleDateFormat("dd-MM-yyyy");
    SimpleDateFormat formatToday = new SimpleDateFormat("yyyy-MM-dd");
    Date c;
    TableLayout tableMainLayout,tableLayoutfooter,tableHeaderLayout,tableGrandTotalLayout;
    TableLayout tableMainLayoutNonGroup,tableLayoutfooterNonGroup;
    private RadioGroup radioGroupNb;
    String dataTipe ="Group";
    int Page = 1;
    int firstPage = 8;
    int Limit = 8;
    int Offset = 0;
    int TotalRowPage = 8;
    ImageView imgBack,imgNext;
    TextView txtInitPage;
    int TotalRow;
    RelativeLayout layarPage;
    String initTypeAr = "Default";
    RadioButton radioGroupType,radioNonGroupType;

    Button btn30hari,btnlbh30hari;
    RelativeLayout layarHari;
    LinearLayout LayarForm;
    ImageView ImgSearch;
    RelativeLayout layarChooseTypeGroup,relativeLayoutBA;
    Spinner spinnerBA;
    Spinner spinnerOverdue;
    double GrandTotalSummary = 0;
    double GrandShip1Summary = 0;
    double GrandShip2Summary=0;
    double GrandShip3Summary=0;

    double GrandTotal30Day = 0;
    double GrandShip130Day = 0;
    double GrandShip230Day=0;
    double GrandShip330Day=0;

    double GrandTotalMore30Day = 0;
    double GrandShip1More30Day = 0;
    double GrandShip2More30Day=0;
    double GrandShip3More30Day=0;


    double GrandTotalSummaryNonGroup = 0;
    double GrandShip1SummaryNonGroup = 0;
    double GrandShip2SummaryNonGroup =0;
    double GrandShip3SummaryNonGroup =0;

    double GrandTotal30DayNonGroup = 0;
    double GrandShip130DayNonGroup = 0;
    double GrandShip230DayNonGroup =0;
    double GrandShip330DayNonGroup =0;

    double GrandTotalMore30DayNonGroup = 0;
    double GrandShip1More30DayNonGroup = 0;
    double GrandShip2More30DayNonGroup =0;
    double GrandShip3More30DayNonGroup =0;

    RelativeLayout relativeLayoutOverdue,relativeLayoutYear,relativeInfoDate;
    boolean sTartStatus = false;
    int orientation = 0;

    boolean runningScript = false;
    ConstraintLayout layoutEmpty;
    SharePreference sharePreference;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ar);
        DBquery = new DatabaseQuery(ActivityAR.this);
        sharePreference = new SharePreference(ActivityAR.this);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.hide();
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        today = new Date();
        dateToStr = formatUpdate.format(today);
        UpdatedDateToStr = formatUpdateDated.format(today);
        apiServices = NetClient.DataProduction().create(ApiServices.class);
        progressBar = findViewById(R.id.progress_bar);
        lastUpdateTime = (findViewById(R.id.lastUpdateTime));
        companyGroup = (findViewById(R.id.companyGroup));
        txtInfoDate = (findViewById(R.id.txtinfoDate));
        txtInfoDate.setVisibility(View.GONE);
        et_year = (findViewById(R.id.et_year));
        tableMainLayout = (TableLayout) findViewById(R.id.main_tableGroup);
        tableLayoutfooter = (TableLayout) findViewById(R.id.main_tableFooterGroup);
        tableMainLayoutNonGroup = (TableLayout) findViewById(R.id.main_tableNonGroup);
        tableLayoutfooterNonGroup = (TableLayout) findViewById(R.id.main_tableFooterNonGroup);
        tableHeaderLayout = (TableLayout)findViewById(R.id.tableHeaderLayout);
        tableGrandTotalLayout = (TableLayout)findViewById(R.id.tableGrandTotalLayout);
        layoutEmpty = (ConstraintLayout)findViewById(R.id.layoutEmpty);
        radioGroupNb = (findViewById(R.id.rg));
        radioGroupNb.setVisibility(View.GONE);
        radioGroupType  = (findViewById(R.id.radioGroupType));
        radioNonGroupType  = (findViewById(R.id.radioNonGroupType));
        txtInitPage = (findViewById(R.id.txtInitPage));
        txtInitPage.setVisibility(View.GONE);
        imgBack=(ImageView)findViewById(R.id.backIcon);
        ImgSearch = (ImageView)findViewById(R.id.imgSearch);
        ImgSearch.setOnClickListener(this);
        imgBack.setVisibility(View.GONE);
        imgNext=(ImageView)findViewById(R.id.nextIcon);
        layarPage = (RelativeLayout)findViewById(R.id.layarPage);
        layarHari = (RelativeLayout)findViewById(R.id.pilihHari);
        layarHari.setVisibility(View.GONE);
        btn30hari = (Button)findViewById(R.id.btn30days);
        btnlbh30hari = (Button)findViewById(R.id.btnnext30days);
        spinnerBA = (Spinner)findViewById(R.id.spinBA);
        spinnerOverdue = (Spinner)findViewById(R.id.spinOverDue);
        relativeLayoutOverdue = (RelativeLayout)findViewById(R.id.relativeLayoutOverdue);
        relativeLayoutOverdue.setVisibility(View.GONE);
        relativeInfoDate = (RelativeLayout)findViewById(R.id.relativeLayoutDate);
        relativeInfoDate.setVisibility(View.GONE);
        relativeLayoutYear = (RelativeLayout)findViewById(R.id.relativeLayoutYear);
        layarChooseTypeGroup = (RelativeLayout)findViewById(R.id.layarChooseTypeGroup);
        relativeLayoutBA= (RelativeLayout)findViewById(R.id.relativeLayoutBA);
        btn30hari.setOnClickListener(this);
        btnlbh30hari.setOnClickListener(this);
        layarPage.setVisibility(View.GONE);
        imgBack.setOnClickListener(this);
        imgNext.setOnClickListener(this);
        LayarForm = (LinearLayout)findViewById(R.id.layarForm);
        dateFormatter = new SimpleDateFormat("yyyy-MM-dd", Locale.US);
        c= Calendar.getInstance().getTime();
        et_year.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ActivityAR.this, ChooseDate.class);
                startActivityForResult(intent,99);// Activity is started with requestCode 2
            }
        });
        new SharePreference(ActivityAR.this).setFORMOverdue("All");
        new SharePreference(ActivityAR.this).setFORMBA("All");
        setUpToolbar();
        setSpinnerOverdue();
        setSpinnerBA();
        INITPAGE();
        radioGroupNb.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch(checkedId){
                    case R.id.radioGroupType:
                        dataTipe = "Group";
                        Limit=8;
                        Offset=0;
                        Page=1;
                        TotalRowPage=8;
                        CallAR();
                        break;
                    case R.id.radioNonGroupType:
                        dataTipe = "NonGroup";
                        Limit=8;
                        Offset=0;
                        TotalRowPage=8;
                        Page=1;
                        CallAR();
                        break;
                }
            }
        });

        orientation = this.getResources().getConfiguration().orientation;
    }

    void setSpinnerBA(){
        ArrayAdapter adapter = ArrayAdapter.createFromResource(
                getApplicationContext(), R.array.baspinner, R.layout.item_spinner_home);
        adapter.setDropDownViewResource(R.layout.item_spinner_home);
        spinnerBA.setAdapter(adapter);
        spinnerBA.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView1, View selectedItemView, int position, long id) {
                // your code here
                String dataBA = parentView1.getItemAtPosition(position).toString();
                new SharePreference(ActivityAR.this).setFORMBA(dataBA);
                CallAR();
                setSpinnerOverdue();
                Offset=0;
                TotalRowPage=8;
                Page=1;
                initTypeAr="Default";
            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }

    void setSpinnerOverdue(){
        List<String> overDueData = new ArrayList<String>();
        String overdue = new SharePreference(ActivityAR.this).isFormOverdue();
        if(overdue==null || overdue.equalsIgnoreCase("")){
            overDueData.add("All");
            overDueData.add("30 Day");
            overDueData.add("> 30 Days");
        }
        if(overdue.equalsIgnoreCase("All")){
            overDueData.add("All");
            overDueData.add("30 Day");
            overDueData.add("> 30 Days");
        }if(overdue.equalsIgnoreCase("30 Day")){
            overDueData.add("30 Days");
            overDueData.add("All");
            overDueData.add("> 30 Days");
        }if(overdue.equalsIgnoreCase("> 30 Days")){
            overDueData.add("> 30 Days");
            overDueData.add("30 Day");
            overDueData.add("All");
        }
        ArrayAdapter<String> dtOverdue = new ArrayAdapter<String>(ActivityAR.this, R.layout.item_spinner_home, overDueData);
        dtOverdue.setDropDownViewResource(R.layout.item_spinner_home);
        // attaching data adapter to spinner
        spinnerOverdue.setAdapter(dtOverdue);
        spinnerOverdue.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parentView1, View selectedItemView, int position, long id) {

                if(sTartStatus){
                    String show = parentView1.getItemAtPosition(position).toString();
                    setHalaman(show);
                }else{
                    sTartStatus=true;
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        Log.d("tag", "config changed");
        super.onConfigurationChanged(newConfig);
        String UpdatedDataSyncDate = null;
        orientation = newConfig.orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT){
            //Toast.makeText(this, "POTRAIT", Toast.LENGTH_SHORT).show();
            if(new SharePreference(ActivityAR.this).isSyncdataTime_AR().equalsIgnoreCase("")){
                if(checkRow()>0){
                    GetDataARThisMonth();
                }else{
                    GetDataAR();
                }
            }else{
                UpdatedDataSyncDate = new SharePreference(ActivityAR.this).isSyncdataTime_AR();
                if(LongUpdatedDate(dateToStr)==LongUpdatedDate(UpdatedDataSyncDate)){
                    lastUpdateTime.setText(new SharePreference(ActivityAR.this).isFormUpdateSyncDate_AR());
                    if(new SharePreference(ActivityAR.this).isFormGroupCompany().equalsIgnoreCase("INDOFOOD")){
                        companyGroup.setText("SIMP");
                    }else{
                        companyGroup.setText(new SharePreference(ActivityAR.this).isFormGroupCompany());
                    }
                    CallAR();
                }else{
                    if(checkRow()>0){
                        GetDataARThisMonth();
                    }else{
                        GetDataAR();
                    }
                }
            }
            Log.d("tag", "Portrait");
        }if (orientation == Configuration.ORIENTATION_LANDSCAPE){
        //Toast.makeText(this, "LANDSCAPE", Toast.LENGTH_SHORT).show();
        if(new SharePreference(ActivityAR.this).isSyncdataTime_AR().equalsIgnoreCase("")){
            if(checkRow()>0){
                GetDataARThisMonth();
            }else{
                GetDataAR();
            }
        }else{
            UpdatedDataSyncDate = new SharePreference(ActivityAR.this).isSyncdataTime_AR();
            if(LongUpdatedDate(dateToStr)==LongUpdatedDate(UpdatedDataSyncDate)){
                lastUpdateTime.setText(new SharePreference(ActivityAR.this).isFormUpdateSyncDate_AR());
                if(new SharePreference(ActivityAR.this).isFormGroupCompany().equalsIgnoreCase("INDOFOOD")){
                    companyGroup.setText("SIMP");
                }else{
                    companyGroup.setText(new SharePreference(ActivityAR.this).isFormGroupCompany());
                }
                CallAR();
            }else{
                if(checkRow()>0){
                    GetDataARThisMonth();
                }else{
                    GetDataAR();
                }
            }
        }
        Log.d("tag", "Landscape");
    }else{
        Log.w("tag", "other: " + orientation);
    }
    }

    void INITPAGE(){
        initTypeAr="Default";
        String UpdatedDataSyncDate = null;
        if(new SharePreference(ActivityAR.this).isSyncdataTime_AR().equalsIgnoreCase("")){
            if(checkRow()>0){
                GetDataARThisMonth();
            }else{
                GetDataAR();
            }
        }else{
            UpdatedDataSyncDate = new SharePreference(ActivityAR.this).isSyncdataTime_AR();
            if(LongUpdatedDate(dateToStr)==LongUpdatedDate(UpdatedDataSyncDate)){
                if(InitLastCreatedData()){
                    lastUpdateTime.setText(new SharePreference(ActivityAR.this).isFormUpdateSyncDate_AR());
                    if(new SharePreference(ActivityAR.this).isFormGroupCompany().equalsIgnoreCase("INDOFOOD")){
                        companyGroup.setText("SIMP");
                        CallAR();
                    }else{
                        companyGroup.setText(new SharePreference(ActivityAR.this).isFormGroupCompany());
                        CallAR();
                    }
                }
            }else{
                if(checkRow()>0){
                    GetDataARThisMonth();
                }else{
                    GetDataAR();
                }
            }
        }
    }

    private int checkRow(){
        int totalRow = 0;
        totalRow =  DBquery.COUNT(TreasuryArModel.TABLE_NAME,"SELECT * FROM "+TreasuryArModel.TABLE_NAME);
        return totalRow;
    }

    private int deleteThisMonthtable(){
        int result = 0;
        DBquery.openTransaction();
        try{
            String[] a = new String[2];
            a[0] = new SharePreference(ActivityAR.this).isSyncdataTime_AR().substring(3,5);
            result = DBquery.deleteDataTemporary(TreasuryArModel.TABLE_NAME,"substr(ERDAT,6,2)= ?",a);
        }finally {
            DBquery.commitTransaction();
            DBquery.closeTransaction();
        }
        return result;
    }
    private int deletetable(){
        int result = 0;
        DBquery.openTransaction();
        try{
            String[] a = new String[2];
            a[0] = new SharePreference(ActivityAR.this).isRefreshMonth();
            a[1] = new SharePreference(ActivityAR.this).isRefreshYear();
            result = DBquery.deleteDataTemporary(TreasuryArModel.TABLE_NAME,"substr(ERDAT,6,2)= ? AND substr(ERDAT,0,5)= ?",a);
        }finally {
            DBquery.commitTransaction();
            DBquery.closeTransaction();
        }
        return result;
    }
    private void setUpToolbar() {
        toolbar = (findViewById(R.id.toolbar));
        mTextToolbar= (findViewById(R.id.txt_Toolbar));
        imgReload =(findViewById(R.id.imgReload));
        imgReload.setOnClickListener(this);
        mTextToolbar.setText("AR");
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                onBackPressed();
            }
        });
    }

    public void onBackPressed(){
        String message="AR";
        Intent intent=new Intent();
        intent.putExtra("MESSAGE",message);
        setResult(1,intent);
        finish();//finishing activity
    }

    @Override
    public void onClick(View v) {
        if(v==txtInfoDate){
            relativeLayoutOverdue.setVisibility(View.GONE);
            relativeInfoDate.setVisibility(View.GONE);
            LayarForm.setVisibility(View.VISIBLE);
        }
        if(v==imgReload){
            showYearDialog();
            initTypeAr="Default";
            mTextToolbar.setText("AR");

        }
        if(v==imgBack){
            Page=Page-1;
            if(Page==1){
                Offset = 0;
            }else{
                Offset = Offset-8;
            }
            TotalRowPage = TotalRowPage-8;
            CallAR();

        }
        if(v==imgNext){
            Page=Page+1;
            TotalRowPage = TotalRowPage+8;
            if(Page==2){
                Offset = Offset+8+1;
            }else{
                Offset = Offset+8;
            }
            CallAR();
        }
        if(v==btn30hari){
            initTypeAr ="30Days";
            mTextToolbar.setText("AR (1-30Days)");
            CallAR();
            Offset=0;
            TotalRowPage=8;
            Page=1;
        }
        if(v==btnlbh30hari){
            initTypeAr ="More30Days";
            mTextToolbar.setText("AR ( > 30 Days)");
            CallAR();
            Offset=0;
            TotalRowPage=8;
            Page=1;
        }

        if(v==ImgSearch){
            if(LayarForm.getVisibility()==View.GONE){
                relativeLayoutOverdue.setVisibility(View.GONE);
                relativeInfoDate.setVisibility(View.GONE);
                LayarForm.setVisibility(View.VISIBLE);
            }
            else if(LayarForm.getVisibility()==View.VISIBLE){
                relativeLayoutOverdue.setVisibility(View.VISIBLE);
                relativeInfoDate.setVisibility(View.VISIBLE);
                LayarForm.setVisibility(View.GONE);
            }

        }
    }

    void setHalaman(String show){
        if(show.equalsIgnoreCase("All")){
            sTartStatus=false;
            initTypeAr ="Default";
            new SharePreference(ActivityAR.this).setFORMOverdue("All");
            mTextToolbar.setText("AR");
            CallAR();
            Offset=0;
            TotalRowPage=8;
            Page=1;
            setSpinnerOverdue();
        }
        if(show.equalsIgnoreCase("30 Day")){
            sTartStatus=false;
            initTypeAr ="30Days";
            new SharePreference(ActivityAR.this).setFORMOverdue("30 Day");
            mTextToolbar.setText("AR (1-30 Day)");
            CallAR();
            Offset=0;
            TotalRowPage=8;
            Page=1;
            setSpinnerOverdue();
        }
        if(show.equalsIgnoreCase("> 30 Days")){
            sTartStatus=false;
            initTypeAr ="More30Days";
            mTextToolbar.setText("AR ( >30 Days)");
            new SharePreference(ActivityAR.this).setFORMOverdue("> 30 Days");
            CallAR();
            Offset=0;
            TotalRowPage=8;
            Page=1;
            setSpinnerOverdue();
        }
    }

    private void GetDataAR(){
        if (progressBar.getVisibility() == View.GONE) {
            progressBar.setVisibility(View.VISIBLE);
        }
        Log.d(TAGCOMPANY, "LOADAGE: ");

        GetARList().enqueue(new Callback<TreasuryARResponse>() {
            @Override
            public void onResponse(Call<TreasuryARResponse> call, Response<TreasuryARResponse> response) {
                // Got data. Send it to adapter
                progressBar.setVisibility(View.GONE);
                if(response.code()==200){
                    treasuryARResponse = fetchResults(response);
                    treasuryArModels = treasuryARResponse.getTreasury();
                    SetupFirst(treasuryArModels);

                }else{
                    Toast.makeText(getApplicationContext(),"Toast Gagal", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<TreasuryARResponse> call, Throwable t) {
                t.printStackTrace();
                progressBar.setVisibility(View.GONE);
                // TODO: 08/11/16 handle failure
                Toast.makeText(getApplicationContext(),t.toString(),Toast.LENGTH_SHORT).show();
            }
        });

    }

    private void GetDataARThisMonth(){
        if(deleteThisMonthtable()>0){
            if (progressBar.getVisibility() == View.GONE) {
                progressBar.setVisibility(View.VISIBLE);
            }
            Log.d(TAGCOMPANY, "LOADAGE: ");

            GetARListThisMonth().enqueue(new Callback<TreasuryARResponse>() {
                @Override
                public void onResponse(Call<TreasuryARResponse> call, Response<TreasuryARResponse> response) {
                    // Got data. Send it to adapter
                    progressBar.setVisibility(View.GONE);
                    if(response.code()==200){
                        treasuryARResponse = fetchResults(response);
                        treasuryArModels = treasuryARResponse.getTreasury();
                        SetupThisMonth(treasuryArModels);

                    }else{
                        Toast.makeText(getApplicationContext(),"Toast Gagal", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<TreasuryARResponse> call, Throwable t) {
                    t.printStackTrace();
                    progressBar.setVisibility(View.GONE);
                    // TODO: 08/11/16 handle failure
                    Toast.makeText(getApplicationContext(),t.toString(),Toast.LENGTH_SHORT).show();
                }
            });
        }else{
            if (progressBar.getVisibility() == View.GONE) {
                progressBar.setVisibility(View.VISIBLE);
            }
            Log.d(TAGCOMPANY, "LOADAGE: ");

            GetARListThisMonth().enqueue(new Callback<TreasuryARResponse>() {
                @Override
                public void onResponse(Call<TreasuryARResponse> call, Response<TreasuryARResponse> response) {
                    // Got data. Send it to adapter
                    progressBar.setVisibility(View.GONE);
                    if(response.code()==200){
                        treasuryARResponse = fetchResults(response);
                        treasuryArModels = treasuryARResponse.getTreasury();
                        SetupThisMonth(treasuryArModels);

                    }else{
                        Toast.makeText(getApplicationContext(),"Toast Gagal", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<TreasuryARResponse> call, Throwable t) {
                    t.printStackTrace();
                    progressBar.setVisibility(View.GONE);
                    // TODO: 08/11/16 handle failure
                    Toast.makeText(getApplicationContext(),t.toString(),Toast.LENGTH_SHORT).show();
                }
            });
        }


    }

    private void GetDataARMonthYear(){
        if(deletetable()>0){
            if (progressBar.getVisibility() == View.GONE) {
                progressBar.setVisibility(View.VISIBLE);
            }
            Log.d(TAGCOMPANY, "LOADAGE: ");

            GetARListMonthYear().enqueue(new Callback<TreasuryARResponse>() {
                @Override
                public void onResponse(Call<TreasuryARResponse> call, Response<TreasuryARResponse> response) {
                    // Got data. Send it to adapter
                    progressBar.setVisibility(View.GONE);
                    if(response.code()==200){
                        treasuryARResponse = fetchResults(response);
                        treasuryArModels = treasuryARResponse.getTreasury();
                        SetupMonthYear(treasuryArModels);

                    }else{
                        Toast.makeText(getApplicationContext(),"Toast Gagal", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<TreasuryARResponse> call, Throwable t) {
                    t.printStackTrace();
                    progressBar.setVisibility(View.GONE);
                    // TODO: 08/11/16 handle failure
                    Toast.makeText(getApplicationContext(),t.toString(),Toast.LENGTH_SHORT).show();
                }
            });
        }else{
            if (progressBar.getVisibility() == View.GONE) {
                progressBar.setVisibility(View.VISIBLE);
            }
            Log.d(TAGCOMPANY, "LOADAGE: ");

            GetARListMonthYear().enqueue(new Callback<TreasuryARResponse>() {
                @Override
                public void onResponse(Call<TreasuryARResponse> call, Response<TreasuryARResponse> response) {
                    // Got data. Send it to adapter
                    progressBar.setVisibility(View.GONE);
                    if(response.code()==200){
                        treasuryARResponse = fetchResults(response);
                        treasuryArModels = treasuryARResponse.getTreasury();
                        SetupMonthYear(treasuryArModels);

                    }else{
                        Toast.makeText(getApplicationContext(),"Toast Gagal", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<TreasuryARResponse> call, Throwable t) {
                    t.printStackTrace();
                    progressBar.setVisibility(View.GONE);
                    // TODO: 08/11/16 handle failure
                    Toast.makeText(getApplicationContext(),t.toString(),Toast.LENGTH_SHORT).show();
                }
            });
          //  Toast.makeText(this, "Periode saat ini tidak ada data", Toast.LENGTH_SHORT).show();
        }


    }

    private TreasuryARResponse fetchResults(Response<TreasuryARResponse> response) {
        TreasuryARResponse treasuryARResponse = response.body();
        return treasuryARResponse;
    }

    private Call<TreasuryARResponse> GetARList() {
        return apiServices.getAllAR();
    }
    private Call<TreasuryARResponse> GetARListThisMonth() {
        return apiServices.getThisMonthAR();
    }
    private Call<TreasuryARResponse> GetARListMonthYear() {
        String year = sharePreference.isRefreshYear();
        String month = sharePreference.isRefreshMonth();
        return apiServices.getMonthYear(month,year);
    }

    private void SetupFirst(List<TreasuryArModel>treasuryArModels){
        DBquery.openTransaction();
        for (int i = 0; i < treasuryArModels.size(); i++) {
            TreasuryArModel treasuryArModel=treasuryArModels.get(i);
            ContentValues values = new ContentValues();
            values.put(TreasuryArModel.XML_IDX,treasuryArModel.getIdx());
            values.put(TreasuryArModel.XML_GLACCOUNT,treasuryArModel.getHkont());
            values.put(TreasuryArModel.XML_BA,treasuryArModel.getGsber());
            values.put(TreasuryArModel.XML_CUSTOMERCODE,treasuryArModel.getKunnr());
            values.put(TreasuryArModel.XML_CUSTOMERNAME,treasuryArModel.getNamE1());
            values.put(TreasuryArModel.XML_ACCOUTNNAME,treasuryArModel.getTxT50());
            values.put(TreasuryArModel.XML_GROUPNONGROUP,treasuryArModel.getKvgR5());
            values.put(TreasuryArModel.XML_COMPANYGROUP,treasuryArModel.getBukrs());
            values.put(TreasuryArModel.XML_CURRENCY,treasuryArModel.getWaers());
            values.put(TreasuryArModel.XML_CUSTOMERGROUP,treasuryArModel.getBezei());
            values.put(TreasuryArModel.XML_CURRENTDEBT,treasuryArModel.getDeB00());
            values.put(TreasuryArModel.XML_OVERDUE10,treasuryArModel.getDeB10());
            values.put(TreasuryArModel.XML_OVERDUE20,treasuryArModel.getDeB20());
            values.put(TreasuryArModel.XML_OVERDUE30,treasuryArModel.getDeB30());
            values.put(TreasuryArModel.XML_OVERDUE60,treasuryArModel.getDeB60());
            values.put(TreasuryArModel.XML_OVERDUE90,treasuryArModel.getDeB90());
            values.put(TreasuryArModel.XML_OVERDUE99,treasuryArModel.getDeB99());
            values.put(TreasuryArModel.XML_OVERDUETOTAL,treasuryArModel.getDebtt());
            values.put(TreasuryArModel.XML_CREATEDDATE,treasuryArModel.getErdat());
            values.put(TreasuryArModel.XML_LASTYEAR,treasuryArModel.getLastyear());

            DBquery.insertDataSQL(TreasuryArModel.TABLE_NAME,values);
        }
        DBquery.commitTransaction();
        DBquery.closeTransaction();
        today = new Date();
        dateToStr = formatUpdate.format(today);
        UpdatedDateToStr = formatUpdateDated.format(today);
        new SharePreference(ActivityAR.this).setFormUpdatesyncdate_AR(dateToStr);
        new SharePreference(ActivityAR.this).setSyncDataTime_AR(UpdatedDateToStr);
        lastUpdateTime.setText(new SharePreference(ActivityAR.this).isFormUpdateSyncDate_AR());
        if(new SharePreference(ActivityAR.this).isFormGroupCompany().equalsIgnoreCase("INDOFOOD")){
            companyGroup.setText("SIMP");
        }else{
            companyGroup.setText(new SharePreference(ActivityAR.this).isFormGroupCompany());
        }
        if(InitLastCreatedData()){
            CallAR();
        }else{
            Toast.makeText(this, "Ada Kesalahan Data", Toast.LENGTH_SHORT).show();
        }
        ARDashboard();
    }
    private void SetupThisMonth(List<TreasuryArModel>treasuryArModels){
        DBquery.openTransaction();
        for (int i = 0; i < treasuryArModels.size(); i++) {
            TreasuryArModel treasuryArModel=treasuryArModels.get(i);
            ContentValues values = new ContentValues();
            values.put(TreasuryArModel.XML_IDX,treasuryArModel.getIdx());
            values.put(TreasuryArModel.XML_GLACCOUNT,treasuryArModel.getHkont());
            values.put(TreasuryArModel.XML_BA,treasuryArModel.getGsber());
            values.put(TreasuryArModel.XML_CUSTOMERCODE,treasuryArModel.getKunnr());
            values.put(TreasuryArModel.XML_CUSTOMERNAME,treasuryArModel.getNamE1());
            values.put(TreasuryArModel.XML_ACCOUTNNAME,treasuryArModel.getTxT50());
            values.put(TreasuryArModel.XML_GROUPNONGROUP,treasuryArModel.getKvgR5());
            values.put(TreasuryArModel.XML_COMPANYGROUP,treasuryArModel.getBukrs());
            values.put(TreasuryArModel.XML_CURRENCY,treasuryArModel.getWaers());
            values.put(TreasuryArModel.XML_CUSTOMERGROUP,treasuryArModel.getBezei());
            values.put(TreasuryArModel.XML_CURRENTDEBT,treasuryArModel.getDeB00());
            values.put(TreasuryArModel.XML_OVERDUE10,treasuryArModel.getDeB10());
            values.put(TreasuryArModel.XML_OVERDUE20,treasuryArModel.getDeB20());
            values.put(TreasuryArModel.XML_OVERDUE30,treasuryArModel.getDeB30());
            values.put(TreasuryArModel.XML_OVERDUE60,treasuryArModel.getDeB60());
            values.put(TreasuryArModel.XML_OVERDUE90,treasuryArModel.getDeB90());
            values.put(TreasuryArModel.XML_OVERDUE99,treasuryArModel.getDeB99());
            values.put(TreasuryArModel.XML_OVERDUETOTAL,treasuryArModel.getDebtt());
            values.put(TreasuryArModel.XML_CREATEDDATE,treasuryArModel.getErdat());
            values.put(TreasuryArModel.XML_LASTYEAR,treasuryArModel.getLastyear());

            DBquery.insertDataSQL(TreasuryArModel.TABLE_NAME,values);
        }
        DBquery.commitTransaction();
        DBquery.closeTransaction();
        today = new Date();
        dateToStr = formatUpdate.format(today);
        UpdatedDateToStr = formatUpdateDated.format(today);
        new SharePreference(ActivityAR.this).setFormUpdatesyncdate_AR(dateToStr);
        new SharePreference(ActivityAR.this).setSyncDataTime_AR(UpdatedDateToStr);
        lastUpdateTime.setText(new SharePreference(ActivityAR.this).isFormUpdateSyncDate_AR());
        if(new SharePreference(ActivityAR.this).isFormGroupCompany().equalsIgnoreCase("INDOFOOD")){
            companyGroup.setText("SIMP");
        }else{
            companyGroup.setText(new SharePreference(ActivityAR.this).isFormGroupCompany());
        }
        CallAR();
        ARDashboard();
    }
    private void SetupMonthYear(List<TreasuryArModel>treasuryArModels){
        DBquery.openTransaction();
        for (int i = 0; i < treasuryArModels.size(); i++) {
            TreasuryArModel treasuryArModel=treasuryArModels.get(i);
            ContentValues values = new ContentValues();
            values.put(TreasuryArModel.XML_IDX,treasuryArModel.getIdx());
            values.put(TreasuryArModel.XML_GLACCOUNT,treasuryArModel.getHkont());
            values.put(TreasuryArModel.XML_BA,treasuryArModel.getGsber());
            values.put(TreasuryArModel.XML_CUSTOMERCODE,treasuryArModel.getKunnr());
            values.put(TreasuryArModel.XML_CUSTOMERNAME,treasuryArModel.getNamE1());
            values.put(TreasuryArModel.XML_ACCOUTNNAME,treasuryArModel.getTxT50());
            values.put(TreasuryArModel.XML_GROUPNONGROUP,treasuryArModel.getKvgR5());
            values.put(TreasuryArModel.XML_COMPANYGROUP,treasuryArModel.getBukrs());
            values.put(TreasuryArModel.XML_CURRENCY,treasuryArModel.getWaers());
            values.put(TreasuryArModel.XML_CUSTOMERGROUP,treasuryArModel.getBezei());
            values.put(TreasuryArModel.XML_CURRENTDEBT,treasuryArModel.getDeB00());
            values.put(TreasuryArModel.XML_OVERDUE10,treasuryArModel.getDeB10());
            values.put(TreasuryArModel.XML_OVERDUE20,treasuryArModel.getDeB20());
            values.put(TreasuryArModel.XML_OVERDUE30,treasuryArModel.getDeB30());
            values.put(TreasuryArModel.XML_OVERDUE60,treasuryArModel.getDeB60());
            values.put(TreasuryArModel.XML_OVERDUE90,treasuryArModel.getDeB90());
            values.put(TreasuryArModel.XML_OVERDUE99,treasuryArModel.getDeB99());
            values.put(TreasuryArModel.XML_OVERDUETOTAL,treasuryArModel.getDebtt());
            values.put(TreasuryArModel.XML_CREATEDDATE,treasuryArModel.getErdat());
            values.put(TreasuryArModel.XML_LASTYEAR,treasuryArModel.getLastyear());

            DBquery.insertDataSQL(TreasuryArModel.TABLE_NAME,values);
        }
        DBquery.commitTransaction();
        DBquery.closeTransaction();
        today = new Date();
        dateToStr = formatUpdate.format(today);
        UpdatedDateToStr = formatUpdateDated.format(today);
        new SharePreference(ActivityAR.this).setFormUpdatesyncdate_AR(dateToStr);
        new SharePreference(ActivityAR.this).setSyncDataTime_AR(UpdatedDateToStr);
        lastUpdateTime.setText(new SharePreference(ActivityAR.this).isFormUpdateSyncDate_AR());
        if(new SharePreference(ActivityAR.this).isFormGroupCompany().equalsIgnoreCase("INDOFOOD")){
            companyGroup.setText("SIMP");
        }else{
            companyGroup.setText(new SharePreference(ActivityAR.this).isFormGroupCompany());
        }
        CallAR();
        ARDashboard();
    }

    private void DataSourceAR(){
        runningScript = true;
        DBquery.openTransaction();
        List<Object> listObject = new ArrayList<>();
        List<TreasuryArModel> listTemp = new ArrayList<TreasuryArModel>();
        listTemp.clear();
        String[] a = new String[2];
        a[0] = "Group";
        a[1] = new SharePreference(ActivityAR.this).isFormYear();
        String sqldb_query = "SELECT idx,HKONT,GSBER,KUNNR,NAME1,TXT50,KVGR5,BUKRS,WAERS,BEZEI,ERDAT,sum(DEB00) as DEB00,sum(DEB10) as DEB10,sum(DEB20) as DEB20, " +
                "sum(DEB30) as DEB30,sum(DEB60) as DEB60,sum(DEB90) as DEB90, " +
                "sum(DEB99) as DEB99,sum(DEBTT) as DEBTT" +
                " From TREASURY_AR where KVGR5 = ? and substr(ERDAT,0,11)=? Group By BEZEI";
        listObject = DBquery.getListDataRawQuery(sqldb_query, TreasuryArModel.TABLE_NAME,a);
        DBquery.closeTransaction();
        if(listObject==null){
            InitDataNullGroup();
        }
        else{
            if(listObject.size() > 0){
                layoutEmpty.setVisibility(View.GONE);
                LayarForm.setVisibility(View.GONE);
                relativeLayoutOverdue.setVisibility(View.VISIBLE);
                relativeInfoDate.setVisibility(View.VISIBLE);
                AsyncDetail asynGroup = new AsyncDetail(listObject);
                asynGroup.execute();
            }else{
                InitDataNullGroup();
            }
        }

    }
    private void DataSourceARNonGroup(){
        DBquery.openTransaction();
        List<Object> listObject = new ArrayList<>();
        List<TreasuryArModel> listTemp = new ArrayList<TreasuryArModel>();
        listTemp.clear();
        String[] a = new String[2];
        a[0] = "Non-Group";
        a[1] = new SharePreference(ActivityAR.this).isFormYear();
        String sqldb_query = "SELECT idx,HKONT,GSBER,KUNNR,NAME1,TXT50,KVGR5,BUKRS,WAERS,BEZEI,ERDAT,sum(DEB00) as DEB00,sum(DEB10) as DEB10,sum(DEB20) as DEB20, " +
                "sum(DEB30) as DEB30,sum(DEB60) as DEB60,sum(DEB90) as DEB90, " +
                "sum(DEB99) as DEB99,sum(DEBTT) as DEBTT" +
                " From TREASURY_AR where KVGR5 = ? and substr(ERDAT,0,11)=? Group By BEZEI";
        listObject = DBquery.getListDataRawQuery(sqldb_query, TreasuryArModel.TABLE_NAME,a);
        DBquery.closeTransaction();
        if(listObject==null){
            InitDataNullNonGroup();
        }
        else{
            if(listObject.size() > 0){
                layoutEmpty.setVisibility(View.GONE);
                relativeLayoutOverdue.setVisibility(View.VISIBLE);
                relativeInfoDate.setVisibility(View.VISIBLE);
                LayarForm.setVisibility(View.GONE);
                AsyncDetailNonGroup asynNonGroup = new AsyncDetailNonGroup(listObject);
                asynNonGroup.execute();
            }else{
                InitDataNullNonGroup();
            }
        }

    }

    private void DataSourceARBA(){
        runningScript = true;
        DBquery.openTransaction();
        String[] a = null;
        String sqldb_query = null;
        List<Object> listObject = new ArrayList<>();
        List<TreasuryArModel> listTemp = new ArrayList<TreasuryArModel>();
        listTemp.clear();
        String BAData = new SharePreference(ActivityAR.this).isBA();
        if(BAData.equalsIgnoreCase("Jakarta")){
            a = new String[2];
            a[0] = "Group";
            a[1] = new SharePreference(ActivityAR.this).isFormYear();
            sqldb_query= "SELECT idx,HKONT,GSBER,KUNNR,NAME1,TXT50,KVGR5,BUKRS,WAERS,BEZEI,ERDAT,sum(DEB00) as DEB00,sum(DEB10) as DEB10,sum(DEB20) as DEB20, " +
                    "sum(DEB30) as DEB30,sum(DEB60) as DEB60,sum(DEB90) as DEB90, " +
                    "sum(DEB99) as DEB99,sum(DEBTT) as DEBTT" +
                    " From TREASURY_AR where KVGR5 = ? and substr(ERDAT,0,11)=? and ( GSBER='3571' or GSBER='3575') Group By BEZEI ";
        }
        if(BAData.equalsIgnoreCase("Surabaya")){
            a = new String[2];
            a[0] = "Group";
            a[1] = new SharePreference(ActivityAR.this).isFormYear();
            sqldb_query= "SELECT idx,HKONT,GSBER,KUNNR,NAME1,TXT50,KVGR5,BUKRS,WAERS,BEZEI,ERDAT,sum(DEB00) as DEB00,sum(DEB10) as DEB10,sum(DEB20) as DEB20, " +
                    "sum(DEB30) as DEB30,sum(DEB60) as DEB60,sum(DEB90) as DEB90, " +
                    "sum(DEB99) as DEB99,sum(DEBTT) as DEBTT" +
                    " From TREASURY_AR where KVGR5 = ? and substr(ERDAT,0,11)=? and  GSBER='3572' Group By BEZEI ";
        }if(BAData.equalsIgnoreCase("Bitung")){
            a = new String[2];
            a[0] = "Group";
            a[1] = new SharePreference(ActivityAR.this).isFormYear();
            sqldb_query= "SELECT idx,HKONT,GSBER,KUNNR,NAME1,TXT50,KVGR5,BUKRS,WAERS,BEZEI,ERDAT,sum(DEB00) as DEB00,sum(DEB10) as DEB10,sum(DEB20) as DEB20, " +
                    "sum(DEB30) as DEB30,sum(DEB60) as DEB60,sum(DEB90) as DEB90, " +
                    "sum(DEB99) as DEB99,sum(DEBTT) as DEBTT" +
                    " From TREASURY_AR where KVGR5 = ? and substr(ERDAT,0,11)=? and GSBER='3576' Group By BEZEI ";
        }if(BAData.equalsIgnoreCase("Medan")){
            a = new String[2];
            a[0] = "Group";
            a[1] = new SharePreference(ActivityAR.this).isFormYear();
            sqldb_query= "SELECT idx,HKONT,GSBER,KUNNR,NAME1,TXT50,KVGR5,BUKRS,WAERS,BEZEI,ERDAT,sum(DEB00) as DEB00,sum(DEB10) as DEB10,sum(DEB20) as DEB20, " +
                    "sum(DEB30) as DEB30,sum(DEB60) as DEB60,sum(DEB90) as DEB90, " +
                    "sum(DEB99) as DEB99,sum(DEBTT) as DEBTT" +
                    " From TREASURY_AR where KVGR5 = ? and substr(ERDAT,0,11)=? and GSBER='3573' Group By BEZEI ";
        }

        listObject = DBquery.getListDataRawQuery(sqldb_query, TreasuryArModel.TABLE_NAME,a);
        DBquery.closeTransaction();
        if(listObject==null){
            InitDataNullGroup();
        }
        else{
            if(listObject.size() > 0){
                LayarForm.setVisibility(View.GONE);
                relativeLayoutOverdue.setVisibility(View.VISIBLE);
                relativeInfoDate.setVisibility(View.VISIBLE);
                AsyncDetail asynGroup = new AsyncDetail(listObject);
                asynGroup.execute();
                layoutEmpty.setVisibility(View.GONE);
            }else{
                InitDataNullGroup();
            }
        }

    }
    private void DataSourceARNonGroupBA(){
        DBquery.openTransaction();
        String[] a = null;
        String sqldb_query = null;
        List<Object> listObject = new ArrayList<>();
        List<TreasuryArModel> listTemp = new ArrayList<TreasuryArModel>();
        listTemp.clear();
        String BAData = new SharePreference(ActivityAR.this).isBA();
        if(BAData.equalsIgnoreCase("Jakarta")){
            a = new String[2];
            a[0] = "Non-Group";
            a[1] = new SharePreference(ActivityAR.this).isFormYear();
            sqldb_query= "SELECT idx,HKONT,GSBER,KUNNR,NAME1,TXT50,KVGR5,BUKRS,WAERS,BEZEI,ERDAT,sum(DEB00) as DEB00,sum(DEB10) as DEB10,sum(DEB20) as DEB20, " +
                    "sum(DEB30) as DEB30,sum(DEB60) as DEB60,sum(DEB90) as DEB90, " +
                    "sum(DEB99) as DEB99,sum(DEBTT) as DEBTT" +
                    " From TREASURY_AR where KVGR5 = ? and substr(ERDAT,0,11)=? and ( GSBER='3571' or GSBER='3575') Group By BEZEI ";
        }
        if(BAData.equalsIgnoreCase("Surabaya")){
            a = new String[2];
            a[0] = "Non-Group";
            a[1] = new SharePreference(ActivityAR.this).isFormYear();
            sqldb_query= "SELECT idx,HKONT,GSBER,KUNNR,NAME1,TXT50,KVGR5,BUKRS,WAERS,BEZEI,ERDAT,sum(DEB00) as DEB00,sum(DEB10) as DEB10,sum(DEB20) as DEB20, " +
                    "sum(DEB30) as DEB30,sum(DEB60) as DEB60,sum(DEB90) as DEB90, " +
                    "sum(DEB99) as DEB99,sum(DEBTT) as DEBTT" +
                    " From TREASURY_AR where KVGR5 = ? and substr(ERDAT,0,11)=? and GSBER='3572' Group By BEZEI ";
        }if(BAData.equalsIgnoreCase("Bitung")){
            a = new String[2];
            a[0] = "Non-Group";
            a[1] = new SharePreference(ActivityAR.this).isFormYear();
            sqldb_query= "SELECT idx,HKONT,GSBER,KUNNR,NAME1,TXT50,KVGR5,BUKRS,WAERS,BEZEI,ERDAT,sum(DEB00) as DEB00,sum(DEB10) as DEB10,sum(DEB20) as DEB20, " +
                    "sum(DEB30) as DEB30,sum(DEB60) as DEB60,sum(DEB90) as DEB90, " +
                    "sum(DEB99) as DEB99,sum(DEBTT) as DEBTT" +
                    " From TREASURY_AR where KVGR5 = ? and substr(ERDAT,0,11)=? and GSBER='3576' Group By BEZEI ";
        }if(BAData.equalsIgnoreCase("Medan")){
            a = new String[2];
            a[0] = "Non-Group";
            a[1] = new SharePreference(ActivityAR.this).isFormYear();
            sqldb_query= "SELECT idx,HKONT,GSBER,KUNNR,NAME1,TXT50,KVGR5,BUKRS,WAERS,BEZEI,ERDAT,sum(DEB00) as DEB00,sum(DEB10) as DEB10,sum(DEB20) as DEB20, " +
                    "sum(DEB30) as DEB30,sum(DEB60) as DEB60,sum(DEB90) as DEB90, " +
                    "sum(DEB99) as DEB99,sum(DEBTT) as DEBTT" +
                    " From TREASURY_AR where KVGR5 = ? and substr(ERDAT,0,11)=? and GSBER='3573' Group By BEZEI ";
        }
        listObject = DBquery.getListDataRawQuery(sqldb_query, TreasuryArModel.TABLE_NAME,a);
        DBquery.closeTransaction();
        if(listObject==null){
            InitDataNullNonGroup();
        }
        else{
            if(listObject.size() > 0){
                layoutEmpty.setVisibility(View.GONE);
                relativeLayoutOverdue.setVisibility(View.VISIBLE);
                relativeInfoDate.setVisibility(View.VISIBLE);
                LayarForm.setVisibility(View.GONE);
                AsyncDetailNonGroup asynNonGroup = new AsyncDetailNonGroup(listObject);
                asynNonGroup.execute();
            }else{
                InitDataNullNonGroup();
            }
        }

    }
    private class AsyncDetail extends AsyncTask<Void, Integer, Void> {
        TreasuryArModel treasuryArModelGroup;
        List<Object> objectListGroup;
        public AsyncDetail(List<Object> objects) {
            objectListGroup = objects;
        }
        @Override
        protected void onPreExecute() {

            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            //        progressBar.setProgress(values[0]);

        }
        @Override
        protected void onPostExecute(Void result) {
            // code for portrait mode
            if (initTypeAr.equalsIgnoreCase("Default")) {
                loadHeader(objectListGroup, treasuryArModelGroup);
                loadDataSummary(objectListGroup, treasuryArModelGroup);

                //    loadFooter(objectList,treasuryArModel);
            }
            if (initTypeAr.equalsIgnoreCase("30Days")) {
                loadHeader30Days(objectListGroup, treasuryArModelGroup);
                loadData30DaysLimit(objectListGroup, treasuryArModelGroup);
            }
            if (initTypeAr.equalsIgnoreCase("More30Days")) {
                loadHeaderMore30Days(objectListGroup, treasuryArModelGroup);
                loadData90DaysLimit(objectListGroup, treasuryArModelGroup);
            }
            progressBar.setVisibility(View.GONE);

        }

        @Override
        protected Void doInBackground(Void... params) {
            for (int i = 0; i<objectListGroup.size(); i++) {
                publishProgress(i);
                treasuryArModelGroup= (TreasuryArModel) objectListGroup.get(i);
            }

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }
    }
    private class AsyncDetailNonGroup extends AsyncTask<Void, Integer, Void> {
        TreasuryArModel treasuryArModelNonGroup;
        List<Object> objectListNonGroup;
        public AsyncDetailNonGroup(List<Object> objects) {
            objectListNonGroup = objects;
        }
        @Override
        protected void onPreExecute() {

            progressBar.setVisibility(View.VISIBLE);
        }

        @Override
        protected void onProgressUpdate(Integer... values) {
            super.onProgressUpdate(values);
            progressBar.setProgress(values[0]);

        }
        @Override
        protected void onPostExecute(Void result) {

            if(initTypeAr.equalsIgnoreCase("Default")){
                loadDataSummaryNonGroup(objectListNonGroup,treasuryArModelNonGroup);
                loadGrandTotalSummary();
            }
            if(initTypeAr.equalsIgnoreCase("30Days")){
                loadData30DaysLimitNonGroup(objectListNonGroup,treasuryArModelNonGroup);
                loadGrandTotal30Days();
            }
            if(initTypeAr.equalsIgnoreCase("More30Days")){
                loadData90DaysLimitNonGroup(objectListNonGroup,treasuryArModelNonGroup);
                loadGrandTotalMore30Days();
            }
            progressBar.setVisibility(View.GONE);

        }

        @Override
        protected Void doInBackground(Void... params) {
            for (int i = 0; i<objectListNonGroup.size(); i++) {
                publishProgress(i);
                treasuryArModelNonGroup= (TreasuryArModel) objectListNonGroup.get(i);
            }

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return null;
        }
    }

    public void loadHeader(List<Object> objectList, TreasuryArModel data) {
        int leftRowMargin = 0;
        int topRowMargin = 0;
        int rightRowMargin = 0;
        int bottomRowMargin = 0;
        tableHeaderLayout.removeAllViews();
        TextView textSpacer = new TextView(this);
        textSpacer.setText("");
        int textSize = (int) getResources().getDimension(R.dimen.font_size_large);
        LinearLayout linearTitleCustomerType= new LinearLayout(this);
        linearTitleCustomerType.setOrientation(LinearLayout.VERTICAL);
        linearTitleCustomerType.setGravity(Gravity.LEFT);
        linearTitleCustomerType.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT));
        TextView txtCustomerType = new TextView(this);
        txtCustomerType.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        txtCustomerType.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        txtCustomerType.setText("Type Of Customer");
        txtCustomerType.setLines(2);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            txtCustomerType.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            txtCustomerType.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            txtCustomerType.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            txtCustomerType.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        txtCustomerType.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        txtCustomerType.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        txtCustomerType.setPadding(15, 15, 0, 15);
        linearTitleCustomerType.addView(txtCustomerType);
        //      LayarHeaderCustomerType.addView(txtCustomerType);

        LinearLayout linearTotalAR= new LinearLayout(this);
        linearTotalAR.setOrientation(LinearLayout.VERTICAL);
        linearTotalAR.setGravity(Gravity.CENTER);
        linearTotalAR.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT));
        TextView txtTotalAR = new TextView(this);
        txtTotalAR.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        txtTotalAR.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        txtTotalAR.setGravity(Gravity.RIGHT);
        txtTotalAR.setText("Total");
        txtTotalAR.setLines(2);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            txtTotalAR.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            txtTotalAR.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            txtTotalAR.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            txtTotalAR.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        txtTotalAR.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        txtTotalAR.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        txtTotalAR.setPadding(15, 15, 0, 15);
        linearTotalAR.addView(txtTotalAR);


        LinearLayout linearTitleCurrent= new LinearLayout(this);
        linearTitleCurrent.setOrientation(LinearLayout.VERTICAL);
        linearTitleCurrent.setGravity(Gravity.LEFT);
        linearTitleCurrent.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT));
        TextView txtCurrent = new TextView(this);
        txtCurrent.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        txtCurrent.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        txtCurrent.setGravity(Gravity.RIGHT);
        txtCurrent.setText("Current");
        txtCurrent.setLines(2);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            txtCurrent.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            txtCurrent.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            txtCurrent.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            txtCurrent.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        txtCurrent.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        txtCurrent.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        txtCurrent.setPadding(15, 15, 0, 15);
        linearTitleCurrent.addView(txtCurrent);


        LinearLayout linearTitle30Days= new LinearLayout(this);
        linearTitle30Days.setOrientation(LinearLayout.VERTICAL);
        linearTitle30Days.setGravity(Gravity.LEFT);
        linearTitle30Days.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT));
        TextView txt30Days = new TextView(this);
        txt30Days.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        txt30Days.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        txt30Days.setGravity(Gravity.RIGHT);
        txt30Days.setText("1-30 Day");
        txt30Days.setLines(2);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            txt30Days.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            txt30Days.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            txt30Days.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            txt30Days.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        txt30Days.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        txt30Days.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        txt30Days.setPadding(15, 15, 0, 15);
        linearTitle30Days.addView(txt30Days);


        LinearLayout linearTitleMore30Days= new LinearLayout(this);
        linearTitleMore30Days.setOrientation(LinearLayout.VERTICAL);
        linearTitleMore30Days.setGravity(Gravity.LEFT);
        linearTitleMore30Days.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT));
        TextView txtmore30Days = new TextView(this);
        txtmore30Days.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        txtmore30Days.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        txtmore30Days.setGravity(Gravity.RIGHT);
        txtmore30Days.setText("> 30 Days");
        txtmore30Days.setLines(2);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            txtmore30Days.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            txtmore30Days.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            txtmore30Days.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            txtmore30Days.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        txtmore30Days.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        txtmore30Days.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        txtmore30Days.setPadding(15, 15, 15, 15);
        linearTitleMore30Days.addView(txtmore30Days);


        LinearLayout linearTitleLastYear= new LinearLayout(this);
        linearTitleLastYear.setOrientation(LinearLayout.VERTICAL);
        linearTitleLastYear.setGravity(Gravity.RIGHT);
        linearTitleLastYear.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT));
        TextView txtTotalLastYear = new TextView(this);
        txtTotalLastYear.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT));
        txtTotalLastYear.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        txtTotalLastYear.setGravity(Gravity.RIGHT);
        txtTotalLastYear.setText("Last Year");
        txtTotalLastYear.setLines(2);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            txtTotalLastYear.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            txtTotalLastYear.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            txtTotalLastYear.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            txtTotalLastYear.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        txtTotalLastYear.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        txtTotalLastYear.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        txtTotalLastYear.setPadding(15, 15, 0, 15);
        linearTitleLastYear.addView(txtTotalLastYear);


        final TableRow tr = new TableRow(this);
        TableLayout.LayoutParams trParams = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                TableLayout.LayoutParams.MATCH_PARENT);
        trParams.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);
        tr.setPadding(0,0,0,0);
        tr.setLayoutParams(trParams);
        //tr.addView(tv);
        tr.addView(linearTitleCustomerType);
        tr.addView(linearTotalAR);
        tr.addView(linearTitleCurrent);
        tr.addView(linearTitle30Days);
        tr.addView(linearTitleMore30Days);
        //  tr.addView(linearTitleLastYear);
        tableHeaderLayout.addView(tr, trParams);
        final TableRow trSep = new TableRow(this);
        TableLayout.LayoutParams trParamsSep = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                TableLayout.LayoutParams.WRAP_CONTENT);
        trParamsSep.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);

        trSep.setLayoutParams(trParamsSep);
        TextView tvSep = new TextView(this);
        TableRow.LayoutParams tvSepLay = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT);
        tvSepLay.span = 5;
        tvSep.setLayoutParams(tvSepLay);
        tvSep.setHeight(1);
        trSep.addView(tvSep);
        tableHeaderLayout.addView(trSep, trParamsSep);
    }
    public void loadHeader30Days(List<Object> objectList, TreasuryArModel data) {
        int leftRowMargin = 0;
        int topRowMargin = 0;
        int rightRowMargin = 0;
        int bottomRowMargin = 0;
        tableHeaderLayout.removeAllViews();
        TextView textSpacer = new TextView(this);
        textSpacer.setText("");
        int textSize = (int) getResources().getDimension(R.dimen.font_size_large);
        LinearLayout linearTitleCustomerType= new LinearLayout(this);
        linearTitleCustomerType.setOrientation(LinearLayout.VERTICAL);
        linearTitleCustomerType.setGravity(Gravity.LEFT);
        linearTitleCustomerType.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT));
        TextView txtCustomerType = new TextView(this);
        txtCustomerType.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        txtCustomerType.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        txtCustomerType.setText("Type Of Customer");
        txtCustomerType.setLines(2);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            txtCustomerType.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            txtCustomerType.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            txtCustomerType.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            txtCustomerType.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        txtCustomerType.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        txtCustomerType.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        txtCustomerType.setPadding(15, 15, 0, 15);
        linearTitleCustomerType.addView(txtCustomerType);
        //      LayarHeaderCustomerType.addView(txtCustomerType);

        LinearLayout linearTotalAR= new LinearLayout(this);
        linearTotalAR.setOrientation(LinearLayout.VERTICAL);
        linearTotalAR.setGravity(Gravity.LEFT);
        linearTotalAR.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT));
        TextView txtTotalAR = new TextView(this);
        txtTotalAR.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        txtTotalAR.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        txtTotalAR.setGravity(Gravity.RIGHT);
        txtTotalAR.setText("Total");
        txtTotalAR.setLines(2);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            txtTotalAR.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            txtTotalAR.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            txtTotalAR.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            txtTotalAR.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        txtTotalAR.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        txtTotalAR.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        txtTotalAR.setPadding(15, 15, 0, 15);
        linearTotalAR.addView(txtTotalAR);


        LinearLayout linearTitleCurrent= new LinearLayout(this);
        linearTitleCurrent.setOrientation(LinearLayout.VERTICAL);
        linearTitleCurrent.setGravity(Gravity.LEFT);
        linearTitleCurrent.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT));
        TextView txtCurrent = new TextView(this);
        txtCurrent.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        txtCurrent.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        txtCurrent.setGravity(Gravity.RIGHT);
        txtCurrent.setText("1-10");
        txtCurrent.setLines(2);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            txtCurrent.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            txtCurrent.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            txtCurrent.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            txtCurrent.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        txtCurrent.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        txtCurrent.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        txtCurrent.setPadding(15, 15, 0, 15);
        linearTitleCurrent.addView(txtCurrent);


        LinearLayout linearTitle30Days= new LinearLayout(this);
        linearTitle30Days.setOrientation(LinearLayout.VERTICAL);
        linearTitle30Days.setGravity(Gravity.LEFT);
        linearTitle30Days.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT));
        TextView txt30Days = new TextView(this);
        txt30Days.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        txt30Days.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        txt30Days.setGravity(Gravity.RIGHT);
        txt30Days.setText("21-30 Day");
        txt30Days.setLines(2);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            txt30Days.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            txt30Days.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            txt30Days.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            txt30Days.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        txt30Days.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        txt30Days.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        txt30Days.setPadding(15, 15, 0, 15);
        linearTitle30Days.addView(txt30Days);


        LinearLayout linearTitleMore30Days= new LinearLayout(this);
        linearTitleMore30Days.setOrientation(LinearLayout.VERTICAL);
        linearTitleMore30Days.setGravity(Gravity.LEFT);
        linearTitleMore30Days.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT));
        TextView txtmore30Days = new TextView(this);
        txtmore30Days.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        txtmore30Days.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        txtmore30Days.setGravity(Gravity.RIGHT);
        txtmore30Days.setText("31-60 Days");
        txtmore30Days.setLines(2);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            txtmore30Days.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            txtmore30Days.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            txtmore30Days.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            txtmore30Days.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        txtmore30Days.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        txtmore30Days.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        txtmore30Days.setPadding(15, 15, 15, 15);
        linearTitleMore30Days.addView(txtmore30Days);


        LinearLayout linearTitleLastYear= new LinearLayout(this);
        linearTitleLastYear.setOrientation(LinearLayout.VERTICAL);
        linearTitleLastYear.setGravity(Gravity.RIGHT);
        linearTitleLastYear.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT));
        TextView txtTotalLastYear = new TextView(this);
        txtTotalLastYear.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT));
        txtTotalLastYear.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        txtTotalLastYear.setGravity(Gravity.RIGHT);
        txtTotalLastYear.setText("Last Year");
        txtTotalLastYear.setLines(2);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            txtTotalLastYear.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            txtTotalLastYear.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            txtTotalLastYear.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            txtTotalLastYear.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        txtTotalLastYear.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        txtTotalLastYear.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        txtTotalLastYear.setPadding(15, 15, 0, 15);
        linearTitleLastYear.addView(txtTotalLastYear);


        final TableRow tr = new TableRow(this);
        TableLayout.LayoutParams trParams = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                TableLayout.LayoutParams.MATCH_PARENT);
        trParams.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);
        tr.setPadding(0,0,0,0);
        tr.setLayoutParams(trParams);
        //tr.addView(tv);
        tr.addView(linearTitleCustomerType);
        tr.addView(linearTotalAR);
        tr.addView(linearTitleCurrent);
        tr.addView(linearTitle30Days);
        tr.addView(linearTitleMore30Days);
        //  tr.addView(linearTitleLastYear);
        tableHeaderLayout.addView(tr, trParams);
        final TableRow trSep = new TableRow(this);
        TableLayout.LayoutParams trParamsSep = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                TableLayout.LayoutParams.WRAP_CONTENT);
        trParamsSep.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);

        trSep.setLayoutParams(trParamsSep);
        TextView tvSep = new TextView(this);
        TableRow.LayoutParams tvSepLay = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT);
        tvSepLay.span = 5;
        tvSep.setLayoutParams(tvSepLay);
        tvSep.setHeight(1);
        trSep.addView(tvSep);
        tableHeaderLayout.addView(trSep, trParamsSep);
    }
    public void loadHeaderMore30Days(List<Object> objectList, TreasuryArModel data) {
        int leftRowMargin = 0;
        int topRowMargin = 0;
        int rightRowMargin = 0;
        int bottomRowMargin = 0;
        tableHeaderLayout.removeAllViews();
        TextView textSpacer = new TextView(this);
        textSpacer.setText("");
        int textSize = (int) getResources().getDimension(R.dimen.font_size_large);
        LinearLayout linearTitleCustomerType= new LinearLayout(this);
        linearTitleCustomerType.setOrientation(LinearLayout.VERTICAL);
        linearTitleCustomerType.setGravity(Gravity.LEFT);
        linearTitleCustomerType.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT));
        TextView txtCustomerType = new TextView(this);
        txtCustomerType.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        txtCustomerType.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        txtCustomerType.setLines(2);
        txtCustomerType.setText("Type Of Customer");
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            txtCustomerType.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            txtCustomerType.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            txtCustomerType.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            txtCustomerType.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        txtCustomerType.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        txtCustomerType.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        txtCustomerType.setPadding(15, 15, 0, 15);
        linearTitleCustomerType.addView(txtCustomerType);
        //      LayarHeaderCustomerType.addView(txtCustomerType);

        LinearLayout linearTotalAR= new LinearLayout(this);
        linearTotalAR.setOrientation(LinearLayout.VERTICAL);
        linearTotalAR.setGravity(Gravity.LEFT);
        linearTotalAR.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT));
        TextView txtTotalAR = new TextView(this);
        txtTotalAR.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        txtTotalAR.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        txtTotalAR.setGravity(Gravity.RIGHT);
        txtTotalAR.setText("Total");
        txtTotalAR.setLines(2);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            txtTotalAR.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            txtTotalAR.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            txtTotalAR.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            txtTotalAR.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        txtTotalAR.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        txtTotalAR.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        txtTotalAR.setPadding(15, 15, 0, 15);
        linearTotalAR.addView(txtTotalAR);


        LinearLayout linearTitleCurrent= new LinearLayout(this);
        linearTitleCurrent.setOrientation(LinearLayout.VERTICAL);
        linearTitleCurrent.setGravity(Gravity.LEFT);
        linearTitleCurrent.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT));
        TextView txtCurrent = new TextView(this);
        txtCurrent.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        txtCurrent.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        txtCurrent.setGravity(Gravity.RIGHT);
        txtCurrent.setText("31-60");
        txtCurrent.setLines(2);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            txtCurrent.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            txtCurrent.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            txtCurrent.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            txtCurrent.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        txtCurrent.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        txtCurrent.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        txtCurrent.setPadding(15, 15, 0, 15);
        linearTitleCurrent.addView(txtCurrent);


        LinearLayout linearTitle30Days= new LinearLayout(this);
        linearTitle30Days.setOrientation(LinearLayout.VERTICAL);
        linearTitle30Days.setGravity(Gravity.LEFT);
        linearTitle30Days.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT));
        TextView txt30Days = new TextView(this);
        txt30Days.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        txt30Days.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        txt30Days.setGravity(Gravity.RIGHT);
        txt30Days.setText("60-90");
        txt30Days.setLines(2);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            txt30Days.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            txt30Days.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            txt30Days.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            txt30Days.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        txt30Days.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        txt30Days.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        txt30Days.setPadding(15, 15, 0, 15);
        linearTitle30Days.addView(txt30Days);


        LinearLayout linearTitleMore30Days= new LinearLayout(this);
        linearTitleMore30Days.setOrientation(LinearLayout.VERTICAL);
        linearTitleMore30Days.setGravity(Gravity.LEFT);
        linearTitleMore30Days.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT));
        TextView txtmore30Days = new TextView(this);
        txtmore30Days.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        txtmore30Days.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        txtmore30Days.setGravity(Gravity.RIGHT);
        txtmore30Days.setText("> 90 Days");
        txtmore30Days.setLines(2);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            txtmore30Days.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            txtmore30Days.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            txtmore30Days.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            txtmore30Days.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        txtmore30Days.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        txtmore30Days.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        txtmore30Days.setPadding(15, 15, 15, 15);
        linearTitleMore30Days.addView(txtmore30Days);


        LinearLayout linearTitleLastYear= new LinearLayout(this);
        linearTitleLastYear.setOrientation(LinearLayout.VERTICAL);
        linearTitleLastYear.setGravity(Gravity.RIGHT);
        linearTitleLastYear.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT));
        TextView txtTotalLastYear = new TextView(this);
        txtTotalLastYear.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT));
        txtTotalLastYear.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        txtTotalLastYear.setGravity(Gravity.RIGHT);
        txtTotalLastYear.setText("Last Year");
        txtTotalLastYear.setLines(2);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            txtTotalLastYear.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            txtTotalLastYear.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            txtTotalLastYear.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            txtTotalLastYear.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        txtTotalLastYear.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        txtTotalLastYear.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        txtTotalLastYear.setPadding(15, 15, 0, 15);
        linearTitleLastYear.addView(txtTotalLastYear);


        final TableRow tr = new TableRow(this);
        TableLayout.LayoutParams trParams = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                TableLayout.LayoutParams.MATCH_PARENT);
        trParams.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);
        tr.setPadding(0,0,0,0);
        tr.setLayoutParams(trParams);
        //tr.addView(tv);
        tr.addView(linearTitleCustomerType);
        tr.addView(linearTotalAR);
        tr.addView(linearTitleCurrent);
        tr.addView(linearTitle30Days);
        tr.addView(linearTitleMore30Days);
        //  tr.addView(linearTitleLastYear);
        tableHeaderLayout.addView(tr, trParams);
        final TableRow trSep = new TableRow(this);
        TableLayout.LayoutParams trParamsSep = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                TableLayout.LayoutParams.WRAP_CONTENT);
        trParamsSep.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);

        trSep.setLayoutParams(trParamsSep);
        TextView tvSep = new TextView(this);
        TableRow.LayoutParams tvSepLay = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT);
        tvSepLay.span = 5;
        tvSep.setLayoutParams(tvSepLay);
        tvSep.setHeight(1);
        trSep.addView(tvSep);
        tableHeaderLayout.addView(trSep, trParamsSep);
    }

    public void loadDataSummary(List<Object> objectList, TreasuryArModel data) {
        TotalRow = DBquery.COUNT(TreasuryArModel.TABLE_NAME,"Select * From TREASURY_AR where KVGR5 = '"+dataTipe+"'");
        refreshPage();
        int leftRowMargin = 0;
        int topRowMargin = 0;
        int rightRowMargin = 0;
        int bottomRowMargin = 0;
        int textSize = 0, smallTextSize = 0, mediumTextSize = 0;
        TextView textSpacer = null;
        textSize = (int) getResources().getDimension(R.dimen.font_size_large);
        smallTextSize = (int) getResources().getDimension(R.dimen.font_size_small);
        mediumTextSize = (int) getResources().getDimension(R.dimen.font_size_large);

        int rows;
        int totaldata = objectList.size();
        String TypeOfCustomer = null;
        String TotalAR = null;
        double dTotalAR = 0;
        String Current = null;
        double dCurrent = 0;
        String Days1s30 = null;
        double dDays1s30 = 0;
        String Dayslbh30 = null;
        double dDayslbh30 = 0;
        String TotalYear = null;
        double dTotalYear = 0;

        double  totalARSum = 0 ;
        String formatARTotalSum = null;
        double  totalCurrentSum = 0 ;
        String formatCurrentTotalSum = null;
        double  total130day = 0 ;
        String format130dayTotalSum= null;
        double  totallbh30day = 0 ;
        String formatlbh30dayTotalSum= null;
        double  totalsumYear = 0 ;
        String formatYearTotalSum= null;


        // linear //

        LinearLayout linearCustomerType;
        LinearLayout linearTotalAR;
        LinearLayout linearTotalYear;
        LinearLayout linearCurrent;
        LinearLayout linear30Days;
        LinearLayout linearmore30Days;
        tableMainLayout.removeAllViews();
        TextView tvTypeOfCustomer;
        TextView tvTotalAR;
        TextView tvCurrent;
        LinearLayout layarCurrent;
        TextView tv1s30;
        LinearLayout layar130;
        TextView tvlbh30;
        LinearLayout layarlbh130;
        TextView tvTotalYear;
        LinearLayout layarTotalYear;
        NumberFormat formatterValue = new DecimalFormat("#,###,###,###,###,###");
        for (rows= 0; rows< totaldata; rows++) {
            data = (TreasuryArModel) objectList.get(rows);
            TypeOfCustomer = data.getBezei();
//                dTotalAR= Double.parseDouble(data.getTotaldebt())/1000000;
//                TotalAR = formatterValue.format(dTotalAR);
            dCurrent= Double.parseDouble(data.getDeB00())/1000000;
            Current = formatterValue.format(dCurrent);
            dDays1s30= Double.parseDouble(data.getDeB10())/1000000+
                    Double.parseDouble(data.getDeB20())/1000000+Double.parseDouble(data.getDeB30())/1000000;
            Days1s30 = formatterValue.format(dDays1s30);
            dDayslbh30= Double.parseDouble(data.getDeB60())/1000000+
                    Double.parseDouble(data.getDeB90())/1000000+Double.parseDouble(data.getDeB99())/1000000;
            Dayslbh30 = formatterValue.format(dDayslbh30);
            TotalYear = formatterValue.format(dTotalAR);
            dTotalAR= (dCurrent+dDays1s30+dDayslbh30);
            TotalAR = formatterValue.format(dTotalAR);
              /*  totalARSum += dTotalAR;
                formatARTotalSum = formatterValue.format(totalARSum);*/
            totalCurrentSum+= dCurrent;
            formatCurrentTotalSum= formatterValue.format(totalCurrentSum);
            total130day += dDays1s30;
            format130dayTotalSum= formatterValue.format(total130day);
            totallbh30day += dDayslbh30;
            formatlbh30dayTotalSum= formatterValue.format(totallbh30day);
            totalsumYear += dTotalAR;
            formatYearTotalSum = formatterValue.format(totalsumYear);
            totalARSum = totalCurrentSum+total130day+totallbh30day;
            formatARTotalSum = formatterValue.format(totalARSum);

            /* Type Of Customer */
            linearCustomerType= new LinearLayout(this);
            linearCustomerType.setOrientation(LinearLayout.VERTICAL);
            linearCustomerType.setGravity(Gravity.LEFT);
            linearCustomerType.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT));
            tvTypeOfCustomer = new TextView(this);
            tvTypeOfCustomer.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT));
            tvTypeOfCustomer.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            tvTypeOfCustomer.setGravity(Gravity.LEFT);
            tvTypeOfCustomer.setPadding(30, 15, 0, 15);
            tvTypeOfCustomer.setLines(2);
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                tvTypeOfCustomer.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
                tvTypeOfCustomer.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            }else{
                tvTypeOfCustomer.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
                tvTypeOfCustomer.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            }
            tvTypeOfCustomer.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            tvTypeOfCustomer.setTextColor(getResources().getColor(R.color.colorBlackGray));
            tvTypeOfCustomer.setText(TypeOfCustomer);
            linearCustomerType.addView(tvTypeOfCustomer);
            /* Type Of Customer */

            /* Total AR */
            linearTotalAR= new LinearLayout(this);
            linearTotalAR.setOrientation(LinearLayout.VERTICAL);
            linearTotalAR.setGravity(Gravity.LEFT);
            linearTotalAR.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT));
            tvTotalAR = new TextView(this);
            tvTotalAR.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT));
            tvTotalAR.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            tvTotalAR.setGravity(Gravity.RIGHT);
            tvTotalAR.setLines(2);
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                tvTotalAR.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
                tvTotalAR.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            }else{
                tvTotalAR.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
                tvTotalAR.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            }
            tvTotalAR.setPadding(15, 15, 0, 15);
            tvTotalAR.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            tvTotalAR.setTextColor(getResources().getColor(R.color.colorBlackGray));
            tvTotalAR.setText(TotalAR);
            linearTotalAR.addView(tvTotalAR);
            /* Total AR */

            /* Total CURRENT */
            linearCurrent= new LinearLayout(this);
            linearCurrent.setOrientation(LinearLayout.VERTICAL);
            linearCurrent.setGravity(Gravity.LEFT);
            linearCurrent.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT));
            tvCurrent = new TextView(this);
            tvCurrent.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT));
            tvCurrent.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            tvCurrent.setGravity(Gravity.RIGHT);
            tvCurrent.setLines(2);
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                tvCurrent.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
                tvCurrent.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            }else{
                tvCurrent.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
                tvCurrent.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            }
            tvCurrent.setPadding(15, 15, 0, 15);
            tvCurrent.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            tvCurrent.setTextColor(getResources().getColor(R.color.colorBlackGray));
            tvCurrent.setText(Current);
            linearCurrent.addView(tvCurrent);
            /* Total CURRENT */

            /* Total 1-30 */
            linear30Days= new LinearLayout(this);
            linear30Days.setOrientation(LinearLayout.VERTICAL);
            linear30Days.setGravity(Gravity.LEFT);
            linear30Days.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT));
            tv1s30 = new TextView(this);
            tv1s30.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT));
            tv1s30.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            tv1s30.setGravity(Gravity.RIGHT);
            tv1s30.setLines(2);
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                tv1s30.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
                tv1s30.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            }else{
                tv1s30.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
                tv1s30.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            }
            tv1s30.setPadding(15, 15, 0, 15);
            tv1s30.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            tv1s30.setTextColor(getResources().getColor(R.color.colorBlackGray));
            tv1s30.setText(Days1s30);
            linear30Days.addView(tv1s30);
            /* Total 1-30 */

            /* Total >30 */
            linearmore30Days= new LinearLayout(this);
            linearmore30Days.setOrientation(LinearLayout.VERTICAL);
            linearmore30Days.setGravity(Gravity.LEFT);
            linearmore30Days.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT));
            tvlbh30 = new TextView(this);
            tvlbh30.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT));
            tvlbh30.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            tvlbh30.setGravity(Gravity.RIGHT);
            tvlbh30.setLines(2);
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                tvlbh30.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
                tvlbh30.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            }else{
                tvlbh30.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
                tvlbh30.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            }
            tvlbh30.setPadding(15, 15, 15, 15);
            tvlbh30.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            tvlbh30.setTextColor(getResources().getColor(R.color.colorBlackGray));
            tvlbh30.setText(Dayslbh30);
            linearmore30Days.addView(tvlbh30);
            /* Total 1-30 */

            /* Total  */
            linearTotalYear= new LinearLayout(this);
            linearTotalYear.setOrientation(LinearLayout.VERTICAL);
            linearTotalYear.setGravity(Gravity.LEFT);
            linearTotalYear.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT));
            tvTotalYear = new TextView(this);
            tvTotalYear.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT));
            tvTotalYear.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            tvTotalYear.setGravity(Gravity.RIGHT);
            tvTotalYear.setLines(2);
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                tvTotalYear.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
                tvTotalYear.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            }else{
                tvTotalYear.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
                tvTotalYear.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            }
            tvTotalYear.setPadding(15, 15, 0, 15);
            tvTotalYear.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            tvTotalYear.setTextColor(getResources().getColor(R.color.colorBlackGray));
            tvTotalYear.setText("n/a");
            linearTotalYear.addView(tvTotalYear);
            /* Total  */


// add table row
            final TableRow tr = new TableRow(this);
            tr.setId(rows + 1);
            TableLayout.LayoutParams trParams = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                    TableLayout.LayoutParams.WRAP_CONTENT);
            trParams.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);
            tr.setPadding(0,0,0,0);
            tr.setLayoutParams(trParams);

            //tr.addView(tv);
            tr.addView(linearCustomerType);
            tr.addView(linearTotalAR);
            tr.addView(linearCurrent);
            tr.addView(linear30Days);
            tr.addView(linearmore30Days);
            //   tr.addView(linearTotalYear);
            tableMainLayout.addView(tr, trParams);

            // add separator row
            final TableRow trSep = new TableRow(this);
            TableLayout.LayoutParams trParamsSep = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                    TableLayout.LayoutParams.WRAP_CONTENT);
            trParamsSep.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);

            trSep.setLayoutParams(trParamsSep);
            TextView tvSep = new TextView(this);
            TableRow.LayoutParams tvSepLay = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT);
            tvSepLay.span = 5;
            tvSep.setLayoutParams(tvSepLay);
            tvSep.setHeight(1);
            trSep.addView(tvSep);
            tableMainLayout.addView(trSep, trParamsSep);
        }


        tableLayoutfooter.removeAllViews();
        final TextView tvTotal = new TextView(this);
        tvTotal.setPadding(15, 15, 0, 15);
        tvTotal.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));

        tvTotal.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumTextSize);
        tvTotal.setGravity(Gravity.LEFT);
        tvTotal.setText("Group");
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            tvTotal.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            tvTotal.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            tvTotal.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            tvTotal.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        tvTotal.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        tvTotal.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        //layTotalTitle.addView(tvTotal);

        final TextView tvTotalARSum = new TextView(this);
        tvTotalARSum.setPadding(15, 15, 0, 15);
        tvTotalARSum.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        tvTotalARSum.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumTextSize);
        tvTotalARSum.setGravity(Gravity.RIGHT);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            tvTotalARSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            tvTotalARSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            tvTotalARSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            tvTotalARSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        tvTotalARSum.setText(formatARTotalSum);

        tvTotalARSum.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        tvTotalARSum.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        //layTotalAR.addView(tvTotalARSum);

        final TextView tvTotalCurrentSum = new TextView(this);
        tvTotalCurrentSum.setPadding(15, 15, 0, 15);
        tvTotalCurrentSum.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        tvTotalCurrentSum.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumTextSize);
        tvTotalCurrentSum.setGravity(Gravity.RIGHT);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            tvTotalCurrentSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            tvTotalCurrentSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            tvTotalCurrentSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            tvTotalCurrentSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        tvTotalCurrentSum.setText(formatCurrentTotalSum);
        tvTotalCurrentSum.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        tvTotalCurrentSum.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        //layCurrentSum.addView(tvTotalCurrentSum);


        final TextView tv30daysSum = new TextView(this);
        tv30daysSum.setPadding(15, 15, 0, 15);
        tv30daysSum.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        tv30daysSum.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumTextSize);
        tv30daysSum.setGravity(Gravity.RIGHT);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            tv30daysSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            tv30daysSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            tv30daysSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            tv30daysSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        tv30daysSum.setText(format130dayTotalSum);

        tv30daysSum.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        tv30daysSum.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        // layTotal30DaySum.addView(tv30daysSum);


        final TextView tvlbh30daysSum = new TextView(this);
        tvlbh30daysSum.setPadding(0, 15, 15, 15);
        tvlbh30daysSum.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        tvlbh30daysSum.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumTextSize);
        tvlbh30daysSum.setGravity(Gravity.RIGHT);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            tvlbh30daysSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            tvlbh30daysSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            tvlbh30daysSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            tvlbh30daysSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        tvlbh30daysSum.setText(formatlbh30dayTotalSum);
        tvlbh30daysSum.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        tvlbh30daysSum.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        // layTotallbh30DaySum.addView(tvlbh30daysSum);

        GrandTotalSummary=totalARSum;
        new SharePreference(ActivityAR.this).setTotalGroupAR(String.valueOf(totalARSum));
        GrandShip1Summary=totalCurrentSum;
        GrandShip2Summary=total130day;
        GrandShip3Summary=totallbh30day;

        final TextView tvPriceFooter = new TextView(this);
        tvPriceFooter.setPadding(15, 15, 0, 15);
        tvPriceFooter.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        tvPriceFooter.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumTextSize);
        tvPriceFooter.setGravity(Gravity.RIGHT);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            tvPriceFooter.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            tvPriceFooter.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            tvPriceFooter.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            tvPriceFooter.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        tvPriceFooter.setText(formatYearTotalSum);
        tvPriceFooter.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        tvPriceFooter.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        //   layPricesFooter.addView(tvPriceFooter);

        // add table row
        final TableRow trFooter = new TableRow(this);
        TableLayout.LayoutParams trParamsFooter = new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT,
                TableLayout.LayoutParams.WRAP_CONTENT);
        trParamsFooter.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);
        trFooter.setPadding(0,0,0,0);
        trFooter.setLayoutParams(trParamsFooter);

        //tr.addView(tv);
        trFooter.addView(tvTotal);
        trFooter.addView(tvTotalARSum);
        trFooter.addView(tvTotalCurrentSum);
        trFooter.addView(tv30daysSum);
        trFooter.addView(tvlbh30daysSum);
        //   trFooter.addView(tvPriceFooter);
        tableLayoutfooter.addView(trFooter, trParamsFooter);
        final TableRow trSep = new TableRow(this);
        TableLayout.LayoutParams trParamsSep = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                TableLayout.LayoutParams.WRAP_CONTENT);
        trParamsSep.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);

        trSep.setLayoutParams(trParamsSep);
        TextView tvSep = new TextView(this);
        TableRow.LayoutParams tvSepLay = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT);
        tvSepLay.span = 5;
        tvSep.setLayoutParams(tvSepLay);
        tvSep.setHeight(1);
        trSep.addView(tvSep);
        tableLayoutfooter.addView(trSep, trParamsSep);
    }

    public void loadData30DaysLimit(List<Object> objectList, TreasuryArModel data) {
        TotalRow = DBquery.COUNT(TreasuryArModel.TABLE_NAME,"Select * From TREASURY_AR where KVGR5 = '"+dataTipe+"'");
        refreshPage();
        int leftRowMargin = 0;
        int topRowMargin = 0;
        int rightRowMargin = 0;
        int bottomRowMargin = 0;
        int textSize = 0, smallTextSize = 0, mediumTextSize = 0;
        TextView textSpacer = null;
        textSize = (int) getResources().getDimension(R.dimen.font_size_medium);
        smallTextSize = (int) getResources().getDimension(R.dimen.font_size_large);
        mediumTextSize = (int) getResources().getDimension(R.dimen.font_size_large);

        int rows;
        int totaldata = objectList.size();
        String TypeOfCustomer = null;
        String TotalAR= null;
        double dTotalAR = 0;
        String S110 = null;
        double d110 = 0;
        String S1120 = null;
        double d1120 = 0;
        String S2130 = null;
        double d2130 = 0;
        String TotalYear = null;
        double dTotalYear = 0;

        double  totalARSum = 0 ;
        String formatARTotalSum = null;
        double  totalS110 = 0 ;
        String formatS110Sum = null;
        double  totalS1120 = 0 ;
        String formatS1120Sum= null;
        double  totalS2130 = 0 ;
        String formatS2130Sum= null;
        double  totalsumYear = 0 ;
        String formatYearTotalSum= null;

        // linear //

        LinearLayout linearCustomerType;
        LinearLayout linearTotalAR;
        LinearLayout linearTotalYear;
        LinearLayout linearCurrent;
        LinearLayout linear30Days;
        LinearLayout linearmore30Days;
        tableMainLayout.removeAllViews();
        TextView tvTypeOfCustomer;
        TextView tvTotalAR;
        TextView tvCurrent;
        LinearLayout layarCurrent;
        TextView tv1s30;
        LinearLayout layar130;
        TextView tvlbh30;
        LinearLayout layarlbh130;
        TextView tvTotalYear;
        LinearLayout layarTotalYear;
        NumberFormat formatterValue = new DecimalFormat("#,###,###,###,###,###");
        for (rows= 0; rows< totaldata; rows++) {
            data = (TreasuryArModel) objectList.get(rows);
            TypeOfCustomer = data.getBezei();
//            dTotalAR= Double.parseDouble(data.getTotaldebt())/1000000;
//            TotalAR = formatterValue.format(dTotalAR);
            d110= Double.parseDouble(data.getDeB10())/1000000;
            S110 = formatterValue.format(d110);
            d1120= Double.parseDouble(data.getDeB20())/1000000;
            S1120 = formatterValue.format(d1120);
            d2130= Double.parseDouble(data.getDeB30())/1000000;
            S2130 = formatterValue.format(d2130);
            dTotalAR= (d110+d1120+d2130);
            TotalAR = formatterValue.format(dTotalAR);
            TotalYear = formatterValue.format(dTotalAR);
//            totalARSum += dTotalAR;
//            formatARTotalSum = formatterValue.format(totalARSum);
            totalS110+= d110;
            formatS110Sum= formatterValue.format(totalS110);
            totalS1120 += d1120;
            formatS1120Sum= formatterValue.format(totalS1120);
            totalS2130 += d2130;
            formatS2130Sum= formatterValue.format(totalS2130);
            totalsumYear += dTotalAR;
            formatYearTotalSum = formatterValue.format(totalsumYear);
            totalARSum = totalS110+totalS1120+totalS2130;
            formatARTotalSum = formatterValue.format(totalARSum);
            /* Type Of Customer */
            linearCustomerType= new LinearLayout(this);
            linearCustomerType.setOrientation(LinearLayout.VERTICAL);
            linearCustomerType.setGravity(Gravity.LEFT);
            linearCustomerType.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT));
            tvTypeOfCustomer = new TextView(this);
            tvTypeOfCustomer.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT));
            tvTypeOfCustomer.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            tvTypeOfCustomer.setGravity(Gravity.LEFT);
            tvTypeOfCustomer.setPadding(30, 15, 0, 15);
            tvTypeOfCustomer.setLines(2);
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                tvTypeOfCustomer.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
                tvTypeOfCustomer.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            }else{
                tvTypeOfCustomer.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
                tvTypeOfCustomer.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            }
            tvTypeOfCustomer.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            tvTypeOfCustomer.setTextColor(getResources().getColor(R.color.colorBlackGray));
            tvTypeOfCustomer.setText(TypeOfCustomer);
            linearCustomerType.addView(tvTypeOfCustomer);
            /* Type Of Customer */

            /* Total AR */
            linearTotalAR= new LinearLayout(this);
            linearTotalAR.setOrientation(LinearLayout.VERTICAL);
            linearTotalAR.setGravity(Gravity.LEFT);
            linearTotalAR.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT));
            tvTotalAR = new TextView(this);
            tvTotalAR.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT));
            tvTotalAR.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            tvTotalAR.setGravity(Gravity.RIGHT);
            tvTotalAR.setLines(2);
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                tvTotalAR.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
                tvTotalAR.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            }else{
                tvTotalAR.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
                tvTotalAR.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            }
            tvTotalAR.setPadding(15, 15, 0, 15);
            tvTotalAR.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            tvTotalAR.setTextColor(getResources().getColor(R.color.colorBlackGray));
            tvTotalAR.setText(TotalAR);
            linearTotalAR.addView(tvTotalAR);
            /* Total AR */

            /* Total CURRENT */
            linearCurrent= new LinearLayout(this);
            linearCurrent.setOrientation(LinearLayout.VERTICAL);
            linearCurrent.setGravity(Gravity.LEFT);
            linearCurrent.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT));
            tvCurrent = new TextView(this);
            tvCurrent.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT));
            tvCurrent.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            tvCurrent.setGravity(Gravity.RIGHT);
            tvCurrent.setLines(2);
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                tvCurrent.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
                tvCurrent.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            }else{
                tvCurrent.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
                tvCurrent.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            }
            tvCurrent.setPadding(15, 15, 0, 15);
            tvCurrent.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            tvCurrent.setTextColor(getResources().getColor(R.color.colorBlackGray));
            tvCurrent.setText(S110);
            linearCurrent.addView(tvCurrent);
            /* Total CURRENT */

            /* Total 1-30 */
            linear30Days= new LinearLayout(this);
            linear30Days.setOrientation(LinearLayout.VERTICAL);
            linear30Days.setGravity(Gravity.LEFT);
            linear30Days.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT));
            tv1s30 = new TextView(this);
            tv1s30.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT));
            tv1s30.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            tv1s30.setGravity(Gravity.RIGHT);
            tv1s30.setLines(2);
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                tv1s30.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
                tv1s30.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            }else{
                tv1s30.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
                tv1s30.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            }
            tv1s30.setPadding(15, 15, 0, 15);
            tv1s30.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            tv1s30.setTextColor(getResources().getColor(R.color.colorBlackGray));
            tv1s30.setText(S1120);
            linear30Days.addView(tv1s30);
            /* Total 1-30 */

            /* Total >30 */
            linearmore30Days= new LinearLayout(this);
            linearmore30Days.setOrientation(LinearLayout.VERTICAL);
            linearmore30Days.setGravity(Gravity.LEFT);
            linearmore30Days.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT));
            tvlbh30 = new TextView(this);
            tvlbh30.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT));
            tvlbh30.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            tvlbh30.setGravity(Gravity.RIGHT);
            tvlbh30.setLines(2);
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                tvlbh30.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
                tvlbh30.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            }else{
                tvlbh30.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
                tvlbh30.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            }
            tvlbh30.setPadding(15, 15, 15, 15);
            tvlbh30.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            tvlbh30.setTextColor(getResources().getColor(R.color.colorBlackGray));
            tvlbh30.setText(S2130);
            linearmore30Days.addView(tvlbh30);
            /* Total 1-30 */

            /* Total  */
            linearTotalYear= new LinearLayout(this);
            linearTotalYear.setOrientation(LinearLayout.VERTICAL);
            linearTotalYear.setGravity(Gravity.LEFT);
            linearTotalYear.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT));
            tvTotalYear = new TextView(this);
            tvTotalYear.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT));
            tvTotalYear.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            tvTotalYear.setGravity(Gravity.RIGHT);
            tvTotalYear.setLines(2);
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                tvTotalYear.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
                tvTotalYear.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            }else{
                tvTotalYear.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
                tvTotalYear.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            }
            tvTotalYear.setPadding(15, 15, 0, 15);
            tvTotalYear.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            tvTotalYear.setTextColor(getResources().getColor(R.color.colorBlackGray));
            tvTotalYear.setText("n/a");
            linearTotalYear.addView(tvTotalYear);
            /* Total  */


// add table row
            final TableRow tr = new TableRow(this);
            tr.setId(rows + 1);
            TableLayout.LayoutParams trParams = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                    TableLayout.LayoutParams.WRAP_CONTENT);
            trParams.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);
            tr.setPadding(0,0,0,0);
            tr.setLayoutParams(trParams);

            //tr.addView(tv);
            tr.addView(linearCustomerType);
            tr.addView(linearTotalAR);
            tr.addView(linearCurrent);
            tr.addView(linear30Days);
            tr.addView(linearmore30Days);
            // tr.addView(linearTotalYear);
            tableMainLayout.addView(tr, trParams);

            // add separator row
            final TableRow trSep = new TableRow(this);
            TableLayout.LayoutParams trParamsSep = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                    TableLayout.LayoutParams.WRAP_CONTENT);
            trParamsSep.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);

            trSep.setLayoutParams(trParamsSep);
            TextView tvSep = new TextView(this);
            TableRow.LayoutParams tvSepLay = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT);
            tvSepLay.span = 5;
            tvSep.setLayoutParams(tvSepLay);
            tvSep.setHeight(1);
            trSep.addView(tvSep);
            tableMainLayout.addView(trSep, trParamsSep);
        }

        tableLayoutfooter.removeAllViews();
        final TextView tvTotal = new TextView(this);
        tvTotal.setPadding(15, 15, 0, 15);
        tvTotal.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));

        tvTotal.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumTextSize);
        tvTotal.setGravity(Gravity.LEFT);
        tvTotal.setText("Group");
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            tvTotal.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            tvTotal.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            tvTotal.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            tvTotal.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        tvTotal.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        tvTotal.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        //layTotalTitle.addView(tvTotal);

        final TextView tvTotalARSum = new TextView(this);
        tvTotalARSum.setPadding(15, 15, 0, 15);
        tvTotalARSum.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        tvTotalARSum.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumTextSize);
        tvTotalARSum.setGravity(Gravity.RIGHT);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            tvTotalARSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            tvTotalARSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            tvTotalARSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            tvTotalARSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        tvTotalARSum.setText(formatARTotalSum);

        tvTotalARSum.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        tvTotalARSum.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        //layTotalAR.addView(tvTotalARSum);

        final TextView tvTotalCurrentSum = new TextView(this);
        tvTotalCurrentSum.setPadding(15, 15, 0, 15);
        tvTotalCurrentSum.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        tvTotalCurrentSum.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumTextSize);
        tvTotalCurrentSum.setGravity(Gravity.RIGHT);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            tvTotalCurrentSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            tvTotalCurrentSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            tvTotalCurrentSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            tvTotalCurrentSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        tvTotalCurrentSum.setText(formatS110Sum);
        tvTotalCurrentSum.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        tvTotalCurrentSum.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        //layCurrentSum.addView(tvTotalCurrentSum);


        final TextView tv30daysSum = new TextView(this);
        tv30daysSum.setPadding(15, 15, 0, 15);
        tv30daysSum.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        tv30daysSum.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumTextSize);
        tv30daysSum.setGravity(Gravity.RIGHT);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            tv30daysSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            tv30daysSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            tv30daysSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            tv30daysSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        tv30daysSum.setText(formatS1120Sum);

        tv30daysSum.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        tv30daysSum.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        // layTotal30DaySum.addView(tv30daysSum);

        final TextView tvlbh30daysSum = new TextView(this);
        tvlbh30daysSum.setPadding(0, 15, 15, 15);
        tvlbh30daysSum.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        tvlbh30daysSum.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumTextSize);
        tvlbh30daysSum.setGravity(Gravity.RIGHT);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            tvlbh30daysSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            tvlbh30daysSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            tvlbh30daysSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            tvlbh30daysSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        tvlbh30daysSum.setText(formatS2130Sum);
        tvlbh30daysSum.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        tvlbh30daysSum.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));

        GrandTotal30Day=totalARSum;
        GrandShip130Day=totalS110;
        GrandShip230Day=totalS1120;
        GrandShip330Day=totalS2130;

        final TextView tvPriceFooter = new TextView(this);
        tvPriceFooter.setPadding(15, 15, 0, 15);
        tvPriceFooter.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        tvPriceFooter.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumTextSize);
        tvPriceFooter.setGravity(Gravity.RIGHT);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            tvPriceFooter.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            tvPriceFooter.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            tvPriceFooter.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            tvPriceFooter.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        tvPriceFooter.setText(formatYearTotalSum);
        tvPriceFooter.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        tvPriceFooter.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        //   layPricesFooter.addView(tvPriceFooter);

        // add table row
        final TableRow trFooter = new TableRow(this);
        TableLayout.LayoutParams trParamsFooter = new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT,
                TableLayout.LayoutParams.WRAP_CONTENT);
        trParamsFooter.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);
        trFooter.setPadding(0,0,0,0);
        trFooter.setLayoutParams(trParamsFooter);

        //tr.addView(tv);
        trFooter.addView(tvTotal);
        trFooter.addView(tvTotalARSum);
        trFooter.addView(tvTotalCurrentSum);
        trFooter.addView(tv30daysSum);
        trFooter.addView(tvlbh30daysSum);
        //   trFooter.addView(tvPriceFooter);
        tableLayoutfooter.addView(trFooter, trParamsFooter);
        final TableRow trSep = new TableRow(this);
        TableLayout.LayoutParams trParamsSep = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                TableLayout.LayoutParams.WRAP_CONTENT);
        trParamsSep.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);

        trSep.setLayoutParams(trParamsSep);
        TextView tvSep = new TextView(this);
        TableRow.LayoutParams tvSepLay = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT);
        tvSepLay.span = 5;
        tvSep.setLayoutParams(tvSepLay);
        tvSep.setHeight(1);
        trSep.addView(tvSep);
        tableLayoutfooter.addView(trSep, trParamsSep);

    }

    public void loadData90DaysLimit(List<Object> objectList, TreasuryArModel data) {
        TotalRow = DBquery.COUNT(TreasuryArModel.TABLE_NAME,"Select * From TREASURY_AR where KVGR5 = '"+dataTipe+"'");
        refreshPage();
        int leftRowMargin = 0;
        int topRowMargin = 0;
        int rightRowMargin = 0;
        int bottomRowMargin = 0;
        int textSize = 0, smallTextSize = 0, mediumTextSize = 0;
        TextView textSpacer = null;
        textSize = (int) getResources().getDimension(R.dimen.font_size_large);
        smallTextSize = (int) getResources().getDimension(R.dimen.font_size_small);
        mediumTextSize = (int) getResources().getDimension(R.dimen.font_size_large);

        int rows;
        int totaldata = objectList.size();
        String TypeOfCustomer = null;
        String TotalAR = null;
        double dTotalAR = 0;
        String S3160 = null;
        double d3160= 0;
        String S6190 = null;
        double d6190 = 0;
        String S90 = null;
        double d90 = 0;
        String TotalYear = null;
        double dTotalYear = 0;

        double  totalARSum = 0 ;
        String formatARTotalSum = null;
        double  total3160 = 0 ;
        String format3160Sum = null;
        double  total6190 = 0 ;
        String format6190Sum= null;
        double  total90 = 0 ;
        String format90Sum= null;
        double  totalsumYear = 0 ;
        String formatYearTotalSum= null;

        // linear //

        LinearLayout linearCustomerType;
        LinearLayout linearTotalAR;
        LinearLayout linearTotalYear;
        LinearLayout linearCurrent;
        LinearLayout linear30Days;
        LinearLayout linearmore30Days;
        tableMainLayout.removeAllViews();
        TextView tvTypeOfCustomer;
        TextView tvTotalAR;
        TextView tvCurrent;
        LinearLayout layarCurrent;
        TextView tv1s30;
        LinearLayout layar130;
        TextView tvlbh30;
        LinearLayout layarlbh130;
        TextView tvTotalYear;
        LinearLayout layarTotalYear;
        NumberFormat formatterValue = new DecimalFormat("#,###,###,###,###,###");
        for (rows= 0; rows< totaldata; rows++) {
            data = (TreasuryArModel) objectList.get(rows);
            TypeOfCustomer = data.getBezei();
//            dTotalAR= Double.parseDouble(data.getTotaldebt())/1000000;
//            TotalAR = formatterValue.format(dTotalAR);
            d3160= Double.parseDouble(data.getDeB60())/1000000;
            S3160 = formatterValue.format(d3160);
            d6190= Double.parseDouble(data.getDeB90())/1000000;
            S6190 = formatterValue.format(d6190);
            d90= Double.parseDouble(data.getDeB99())/1000000;
            S90 = formatterValue.format(d90);
            dTotalAR= (d3160+d6190+d90);
            TotalAR = formatterValue.format(dTotalAR);
            TotalYear = formatterValue.format(dTotalAR);
//          totalARSum += dTotalAR;
//          formatARTotalSum = formatterValue.format(totalARSum);
            total3160+= d3160;
            format3160Sum= formatterValue.format(total3160);
            total6190 += d6190;
            format6190Sum= formatterValue.format(total6190);
            total90 += d90;
            format90Sum= formatterValue.format(total90);
            totalsumYear += dTotalAR;
            formatYearTotalSum = formatterValue.format(totalsumYear);
            totalARSum = total3160+total6190+total90;
            formatARTotalSum = formatterValue.format(totalARSum);
            /* Type Of Customer */
            linearCustomerType= new LinearLayout(this);
            linearCustomerType.setOrientation(LinearLayout.VERTICAL);
            linearCustomerType.setGravity(Gravity.LEFT);
            linearCustomerType.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT));
            tvTypeOfCustomer = new TextView(this);
            tvTypeOfCustomer.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT));
            tvTypeOfCustomer.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            tvTypeOfCustomer.setGravity(Gravity.LEFT);
            tvTypeOfCustomer.setPadding(30, 15, 0, 15);
            tvTypeOfCustomer.setLines(2);
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                tvTypeOfCustomer.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
                tvTypeOfCustomer.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            }else{
                tvTypeOfCustomer.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
                tvTypeOfCustomer.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            }
            tvTypeOfCustomer.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            tvTypeOfCustomer.setTextColor(getResources().getColor(R.color.colorBlackGray));
            tvTypeOfCustomer.setText(TypeOfCustomer);
            linearCustomerType.addView(tvTypeOfCustomer);
            /* Type Of Customer */

            /* Total AR */
            linearTotalAR= new LinearLayout(this);
            linearTotalAR.setOrientation(LinearLayout.VERTICAL);
            linearTotalAR.setGravity(Gravity.LEFT);
            linearTotalAR.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT));
            tvTotalAR = new TextView(this);
            tvTotalAR.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT));
            tvTotalAR.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            tvTotalAR.setGravity(Gravity.RIGHT);
            tvTotalAR.setLines(2);
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                tvTotalAR.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
                tvTotalAR.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            }else{
                tvTotalAR.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
                tvTotalAR.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            }
            tvTotalAR.setPadding(15, 15, 0, 15);
            tvTotalAR.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            tvTotalAR.setTextColor(getResources().getColor(R.color.colorBlackGray));
            tvTotalAR.setText(TotalAR);
            linearTotalAR.addView(tvTotalAR);
            /* Total AR */

            /* Total CURRENT */
            linearCurrent= new LinearLayout(this);
            linearCurrent.setOrientation(LinearLayout.VERTICAL);
            linearCurrent.setGravity(Gravity.LEFT);
            linearCurrent.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT));
            tvCurrent = new TextView(this);
            tvCurrent.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT));
            tvCurrent.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            tvCurrent.setGravity(Gravity.RIGHT);
            tvCurrent.setLines(2);
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                tvCurrent.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
                tvCurrent.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            }else{
                tvCurrent.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
                tvCurrent.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            }
            tvCurrent.setPadding(15, 15, 0, 15);
            tvCurrent.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            tvCurrent.setTextColor(getResources().getColor(R.color.colorBlackGray));
            tvCurrent.setText(S3160);
            linearCurrent.addView(tvCurrent);
            /* Total CURRENT */

            /* Total 1-30 */
            linear30Days= new LinearLayout(this);
            linear30Days.setOrientation(LinearLayout.VERTICAL);
            linear30Days.setGravity(Gravity.LEFT);
            linear30Days.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT));
            tv1s30 = new TextView(this);
            tv1s30.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT));
            tv1s30.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            tv1s30.setGravity(Gravity.RIGHT);
            tv1s30.setLines(2);
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                tv1s30.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
                tv1s30.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            }else{
                tv1s30.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
                tv1s30.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            }
            tv1s30.setPadding(15, 15, 0, 15);
            tv1s30.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            tv1s30.setTextColor(getResources().getColor(R.color.colorBlackGray));
            tv1s30.setText(S6190);
            linear30Days.addView(tv1s30);
            /* Total 1-30 */

            /* Total >30 */
            linearmore30Days= new LinearLayout(this);
            linearmore30Days.setOrientation(LinearLayout.VERTICAL);
            linearmore30Days.setGravity(Gravity.LEFT);
            linearmore30Days.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT));
            tvlbh30 = new TextView(this);
            tvlbh30.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT));
            tvlbh30.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            tvlbh30.setGravity(Gravity.RIGHT);
            tvlbh30.setLines(2);
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                tvlbh30.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
                tvlbh30.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            }else{
                tvlbh30.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
                tvlbh30.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            }
            tvlbh30.setPadding(0, 15, 15, 15);
            tvlbh30.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            tvlbh30.setTextColor(getResources().getColor(R.color.colorBlackGray));
            tvlbh30.setText(S90);
            linearmore30Days.addView(tvlbh30);
            /* Total 1-30 */

            /* Total  */
            linearTotalYear= new LinearLayout(this);
            linearTotalYear.setOrientation(LinearLayout.VERTICAL);
            linearTotalYear.setGravity(Gravity.LEFT);
            linearTotalYear.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT));
            tvTotalYear = new TextView(this);
            tvTotalYear.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT));
            tvTotalYear.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            tvTotalYear.setGravity(Gravity.RIGHT);
            tvTotalYear.setLines(2);
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                tvTotalYear.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
                tvTotalYear.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            }else{
                tvTotalYear.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
                tvTotalYear.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            }
            tvTotalYear.setPadding(15, 15, 0, 15);
            tvTotalYear.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            tvTotalYear.setTextColor(getResources().getColor(R.color.colorBlackGray));
            tvTotalYear.setText("n/a");
            linearTotalYear.addView(tvTotalYear);
            /* Total  */


// add table row
            final TableRow tr = new TableRow(this);
            tr.setId(rows + 1);
            TableLayout.LayoutParams trParams = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                    TableLayout.LayoutParams.WRAP_CONTENT);
            trParams.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);
            tr.setPadding(0,0,0,0);
            tr.setLayoutParams(trParams);

            //tr.addView(tv);
            tr.addView(linearCustomerType);
            tr.addView(linearTotalAR);
            tr.addView(linearCurrent);
            tr.addView(linear30Days);
            tr.addView(linearmore30Days);
            //   tr.addView(linearTotalYear);
            tableMainLayout.addView(tr, trParams);

            // add separator row
            final TableRow trSep = new TableRow(this);
            TableLayout.LayoutParams trParamsSep = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                    TableLayout.LayoutParams.WRAP_CONTENT);
            trParamsSep.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);

            trSep.setLayoutParams(trParamsSep);
            TextView tvSep = new TextView(this);
            TableRow.LayoutParams tvSepLay = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT);
            tvSepLay.span = 5;
            tvSep.setLayoutParams(tvSepLay);
            tvSep.setHeight(1);
            trSep.addView(tvSep);
            tableMainLayout.addView(trSep, trParamsSep);
        }


        tableLayoutfooter.removeAllViews();
        final TextView tvTotal = new TextView(this);
        tvTotal.setPadding(15, 15, 0, 15);
        tvTotal.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));

        tvTotal.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumTextSize);
        tvTotal.setGravity(Gravity.LEFT);
        tvTotal.setText("Group");
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            tvTotal.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            tvTotal.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            tvTotal.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            tvTotal.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        tvTotal.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        tvTotal.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        //layTotalTitle.addView(tvTotal);

        final TextView tvTotalARSum = new TextView(this);
        tvTotalARSum.setPadding(15, 15, 0, 15);
        tvTotalARSum.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        tvTotalARSum.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumTextSize);
        tvTotalARSum.setGravity(Gravity.RIGHT);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            tvTotalARSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            tvTotalARSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            tvTotalARSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            tvTotalARSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        tvTotalARSum.setText(formatARTotalSum);
        tvTotalARSum.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        tvTotalARSum.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        //layTotalAR.addView(tvTotalARSum);

        final TextView tvTotalCurrentSum = new TextView(this);
        tvTotalCurrentSum.setPadding(15, 15, 0, 15);
        tvTotalCurrentSum.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        tvTotalCurrentSum.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumTextSize);
        tvTotalCurrentSum.setGravity(Gravity.RIGHT);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            tvTotalCurrentSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            tvTotalCurrentSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            tvTotalCurrentSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            tvTotalCurrentSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        tvTotalCurrentSum.setText(format3160Sum);
        tvTotalCurrentSum.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        tvTotalCurrentSum.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        //layCurrentSum.addView(tvTotalCurrentSum);


        final TextView tv30daysSum = new TextView(this);
        tv30daysSum.setPadding(15, 15, 0, 15);
        tv30daysSum.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        tv30daysSum.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumTextSize);
        tv30daysSum.setGravity(Gravity.RIGHT);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            tv30daysSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            tv30daysSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            tv30daysSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            tv30daysSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        tv30daysSum.setText(format6190Sum);
        tv30daysSum.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        tv30daysSum.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        // layTotal30DaySum.addView(tv30daysSum);

        final TextView tvlbh30daysSum = new TextView(this);
        tvlbh30daysSum.setPadding(0, 15, 15, 15);
        tvlbh30daysSum.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        tvlbh30daysSum.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumTextSize);
        tvlbh30daysSum.setGravity(Gravity.RIGHT);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            tvlbh30daysSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            tvlbh30daysSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            tvlbh30daysSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            tvlbh30daysSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        tvlbh30daysSum.setText(format90Sum);
        tvlbh30daysSum.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        tvlbh30daysSum.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        // layTotallbh30DaySum.addView(tvlbh30daysSum);

        GrandTotalMore30Day=totalARSum;
        GrandShip1More30Day=total3160;
        GrandShip2More30Day=total6190;
        GrandShip3More30Day=total90;
        final TextView tvPriceFooter = new TextView(this);
        tvPriceFooter.setPadding(15, 15, 0, 15);
        tvPriceFooter.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        tvPriceFooter.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumTextSize);
        tvPriceFooter.setGravity(Gravity.RIGHT);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            tvPriceFooter.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            tvPriceFooter.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            tvPriceFooter.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            tvPriceFooter.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        tvPriceFooter.setText(formatYearTotalSum);
        tvPriceFooter.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        tvPriceFooter.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        //   layPricesFooter.addView(tvPriceFooter);

        // add table row
        final TableRow trFooter = new TableRow(this);
        TableLayout.LayoutParams trParamsFooter = new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT,
                TableLayout.LayoutParams.WRAP_CONTENT);
        trParamsFooter.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);
        trFooter.setPadding(0,0,0,0);
        trFooter.setLayoutParams(trParamsFooter);

        //tr.addView(tv);
        trFooter.addView(tvTotal);
        trFooter.addView(tvTotalARSum);
        trFooter.addView(tvTotalCurrentSum);
        trFooter.addView(tv30daysSum);
        trFooter.addView(tvlbh30daysSum);
        //   trFooter.addView(tvPriceFooter);
        tableLayoutfooter.addView(trFooter, trParamsFooter);
        final TableRow trSep = new TableRow(this);
        TableLayout.LayoutParams trParamsSep = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                TableLayout.LayoutParams.WRAP_CONTENT);
        trParamsSep.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);

        trSep.setLayoutParams(trParamsSep);
        TextView tvSep = new TextView(this);
        TableRow.LayoutParams tvSepLay = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT);
        tvSepLay.span = 5;
        tvSep.setLayoutParams(tvSepLay);
        tvSep.setHeight(1);
        trSep.addView(tvSep);
        tableLayoutfooter.addView(trSep, trParamsSep);

    }

    public void loadDataSummaryNonGroup(List<Object> objectList, TreasuryArModel data) {
        TotalRow = DBquery.COUNT(TreasuryArModel.TABLE_NAME,"Select * From TREASURY_AR where KVGR5 = '"+dataTipe+"'");
        refreshPage();
        int leftRowMargin = 0;
        int topRowMargin = 0;
        int rightRowMargin = 0;
        int bottomRowMargin = 0;
        int textSize = 0, smallTextSize = 0, mediumTextSize = 0;
        TextView textSpacer = null;
        textSize = (int) getResources().getDimension(R.dimen.font_size_large);
        smallTextSize = (int) getResources().getDimension(R.dimen.font_size_small);
        mediumTextSize = (int) getResources().getDimension(R.dimen.font_size_large);

        int rows;
        int totaldata = objectList.size();
        String TypeOfCustomer = null;
        String TotalAR = null;
        double dTotalAR = 0;
        String Current = null;
        double dCurrent = 0;
        String Days1s30 = null;
        double dDays1s30 = 0;
        String Dayslbh30 = null;
        double dDayslbh30 = 0;
        String TotalYear = null;
        double dTotalYear = 0;

        double  totalARSum = 0 ;
        String formatARTotalSum = null;
        double  totalCurrentSum = 0 ;
        String formatCurrentTotalSum = null;
        double  total130day = 0 ;
        String format130dayTotalSum= null;
        double  totallbh30day = 0 ;
        String formatlbh30dayTotalSum= null;
        double  totalsumYear = 0 ;
        String formatYearTotalSum= null;


        // linear //

        LinearLayout linearCustomerType;
        LinearLayout linearTotalAR;
        LinearLayout linearTotalYear;
        LinearLayout linearCurrent;
        LinearLayout linear30Days;
        LinearLayout linearmore30Days;
        tableMainLayoutNonGroup.removeAllViews();
        TextView tvTypeOfCustomer;
        TextView tvTotalAR;
        TextView tvCurrent;
        LinearLayout layarCurrent;
        TextView tv1s30;
        LinearLayout layar130;
        TextView tvlbh30;
        LinearLayout layarlbh130;
        TextView tvTotalYear;
        LinearLayout layarTotalYear;
        NumberFormat formatterValue = new DecimalFormat("#,###,###,###,###,###");
        for (rows= 0; rows< totaldata; rows++) {
            data = (TreasuryArModel) objectList.get(rows);
            TypeOfCustomer = data.getBezei();
//            dTotalAR= Double.parseDouble(data.getTotaldebt())/1000000;
//            TotalAR = formatterValue.format(dTotalAR);
            dCurrent= Double.parseDouble(data.getDeB00())/1000000;
            Current = formatterValue.format(dCurrent);
            dDays1s30= Double.parseDouble(data.getDeB10())/1000000+
                    Double.parseDouble(data.getDeB20())/1000000+Double.parseDouble(data.getDeB30())/1000000;
            Days1s30 = formatterValue.format(dDays1s30);
            dDayslbh30= Double.parseDouble(data.getDeB60())/1000000+
                    Double.parseDouble(data.getDeB90())/1000000+Double.parseDouble(data.getDeB99())/1000000;
            Dayslbh30 = formatterValue.format(dDayslbh30);
            dTotalAR= (dCurrent+dDays1s30+dDayslbh30);
            TotalAR = formatterValue.format(dTotalAR);
            TotalYear = formatterValue.format(dTotalAR);
//            totalARSum += dTotalAR;
//            formatARTotalSum = formatterValue.format(totalARSum);
            totalCurrentSum+= dCurrent;
            formatCurrentTotalSum= formatterValue.format(totalCurrentSum);
            total130day += dDays1s30;
            format130dayTotalSum= formatterValue.format(total130day);
            totallbh30day += dDayslbh30;
            formatlbh30dayTotalSum= formatterValue.format(totallbh30day);
            totalsumYear += dTotalAR;
            formatYearTotalSum = formatterValue.format(totalsumYear);
            totalARSum = totalCurrentSum+total130day+totallbh30day;
            formatARTotalSum = formatterValue.format(totalARSum);
            /* Type Of Customer */
            linearCustomerType= new LinearLayout(this);
            linearCustomerType.setOrientation(LinearLayout.VERTICAL);
            linearCustomerType.setGravity(Gravity.LEFT);
            linearCustomerType.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT));
            tvTypeOfCustomer = new TextView(this);
            tvTypeOfCustomer.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT));
            tvTypeOfCustomer.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            tvTypeOfCustomer.setGravity(Gravity.LEFT);
            tvTypeOfCustomer.setPadding(30, 15, 0, 15);
            tvTypeOfCustomer.setLines(2);
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                tvTypeOfCustomer.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
                tvTypeOfCustomer.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            }else{
                tvTypeOfCustomer.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
                tvTypeOfCustomer.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            }
            tvTypeOfCustomer.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            tvTypeOfCustomer.setTextColor(getResources().getColor(R.color.colorBlackGray));
            tvTypeOfCustomer.setText(TypeOfCustomer);
            linearCustomerType.addView(tvTypeOfCustomer);
            /* Type Of Customer */

            /* Total AR */
            linearTotalAR= new LinearLayout(this);
            linearTotalAR.setOrientation(LinearLayout.VERTICAL);
            linearTotalAR.setGravity(Gravity.LEFT);
            linearTotalAR.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT));
            tvTotalAR = new TextView(this);
            tvTotalAR.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT));
            tvTotalAR.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            tvTotalAR.setGravity(Gravity.RIGHT);
            tvTotalAR.setLines(2);
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                tvTotalAR.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
                tvTotalAR.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            }else{
                tvTotalAR.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
                tvTotalAR.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            }
            tvTotalAR.setPadding(15, 15, 0, 15);
            tvTotalAR.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            tvTotalAR.setTextColor(getResources().getColor(R.color.colorBlackGray));
            tvTotalAR.setText(TotalAR);
            linearTotalAR.addView(tvTotalAR);
            /* Total AR */

            /* Total CURRENT */
            linearCurrent= new LinearLayout(this);
            linearCurrent.setOrientation(LinearLayout.VERTICAL);
            linearCurrent.setGravity(Gravity.LEFT);
            linearCurrent.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT));
            tvCurrent = new TextView(this);
            tvCurrent.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT));
            tvCurrent.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            tvCurrent.setGravity(Gravity.RIGHT);
            tvCurrent.setLines(2);
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                tvCurrent.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
                tvCurrent.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            }else{
                tvCurrent.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
                tvCurrent.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            }
            tvCurrent.setPadding(15, 15, 0, 15);
            tvCurrent.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            tvCurrent.setTextColor(getResources().getColor(R.color.colorBlackGray));
            tvCurrent.setText(Current);
            linearCurrent.addView(tvCurrent);
            /* Total CURRENT */

            /* Total 1-30 */
            linear30Days= new LinearLayout(this);
            linear30Days.setOrientation(LinearLayout.VERTICAL);
            linear30Days.setGravity(Gravity.LEFT);
            linear30Days.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT));
            tv1s30 = new TextView(this);
            tv1s30.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT));
            tv1s30.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            tv1s30.setGravity(Gravity.RIGHT);
            tv1s30.setLines(2);
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                tv1s30.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
                tv1s30.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            }else{
                tv1s30.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
                tv1s30.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            }
            tv1s30.setPadding(15, 15, 0, 15);
            tv1s30.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            tv1s30.setTextColor(getResources().getColor(R.color.colorBlackGray));
            tv1s30.setText(Days1s30);
            linear30Days.addView(tv1s30);
            /* Total 1-30 */

            /* Total >30 */
            linearmore30Days= new LinearLayout(this);
            linearmore30Days.setOrientation(LinearLayout.VERTICAL);
            linearmore30Days.setGravity(Gravity.LEFT);
            linearmore30Days.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT));
            tvlbh30 = new TextView(this);
            tvlbh30.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT));
            tvlbh30.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            tvlbh30.setGravity(Gravity.RIGHT);
            tvlbh30.setLines(2);
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                tvlbh30.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
                tvlbh30.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            }else{
                tvlbh30.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
                tvlbh30.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            }
            tvlbh30.setPadding(15, 15, 15, 15);
            tvlbh30.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            tvlbh30.setTextColor(getResources().getColor(R.color.colorBlackGray));
            tvlbh30.setText(Dayslbh30);
            linearmore30Days.addView(tvlbh30);
            /* Total 1-30 */

            /* Total  */
            linearTotalYear= new LinearLayout(this);
            linearTotalYear.setOrientation(LinearLayout.VERTICAL);
            linearTotalYear.setGravity(Gravity.LEFT);
            linearTotalYear.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT));
            tvTotalYear = new TextView(this);
            tvTotalYear.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT));
            tvTotalYear.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            tvTotalYear.setGravity(Gravity.RIGHT);
            tvTotalYear.setLines(2);
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                tvTotalYear.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
                tvTotalYear.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            }else{
                tvTotalYear.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
                tvTotalYear.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            }
            tvTotalYear.setPadding(15, 15, 0, 15);
            tvTotalYear.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            tvTotalYear.setTextColor(getResources().getColor(R.color.colorBlackGray));
            tvTotalYear.setText("n/a");
            linearTotalYear.addView(tvTotalYear);
            /* Total  */


// add table row
            final TableRow tr = new TableRow(this);
            tr.setId(rows + 1);
            TableLayout.LayoutParams trParams = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                    TableLayout.LayoutParams.WRAP_CONTENT);
            trParams.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);
            tr.setPadding(0,0,0,0);
            tr.setLayoutParams(trParams);

            //tr.addView(tv);
            tr.addView(linearCustomerType);
            tr.addView(linearTotalAR);
            tr.addView(linearCurrent);
            tr.addView(linear30Days);
            tr.addView(linearmore30Days);
            //   tr.addView(linearTotalYear);
            tableMainLayoutNonGroup.addView(tr, trParams);

            // add separator row
            final TableRow trSep = new TableRow(this);
            TableLayout.LayoutParams trParamsSep = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                    TableLayout.LayoutParams.WRAP_CONTENT);
            trParamsSep.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);

            trSep.setLayoutParams(trParamsSep);
            TextView tvSep = new TextView(this);
            TableRow.LayoutParams tvSepLay = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT);
            tvSepLay.span = 5;
            tvSep.setLayoutParams(tvSepLay);
            tvSep.setHeight(1);
            trSep.addView(tvSep);
            tableMainLayoutNonGroup.addView(trSep, trParamsSep);
        }


        tableLayoutfooterNonGroup.removeAllViews();
        final TextView tvTotal = new TextView(this);
        tvTotal.setPadding(15, 15, 0, 15);
        tvTotal.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));

        tvTotal.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumTextSize);
        tvTotal.setGravity(Gravity.LEFT);
        tvTotal.setText("Non Group");
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            tvTotal.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            tvTotal.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            tvTotal.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            tvTotal.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        tvTotal.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        tvTotal.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        //layTotalTitle.addView(tvTotal);

        final TextView tvTotalARSum = new TextView(this);
        tvTotalARSum.setPadding(15, 15, 0, 15);
        tvTotalARSum.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        tvTotalARSum.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumTextSize);
        tvTotalARSum.setGravity(Gravity.RIGHT);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            tvTotalARSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            tvTotalARSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            tvTotalARSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            tvTotalARSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        tvTotalARSum.setText(formatARTotalSum);

        tvTotalARSum.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        tvTotalARSum.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        //layTotalAR.addView(tvTotalARSum);

        final TextView tvTotalCurrentSum = new TextView(this);
        tvTotalCurrentSum.setPadding(15, 15, 0, 15);
        tvTotalCurrentSum.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        tvTotalCurrentSum.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumTextSize);
        tvTotalCurrentSum.setGravity(Gravity.RIGHT);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            tvTotalCurrentSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            tvTotalCurrentSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            tvTotalCurrentSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            tvTotalCurrentSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        tvTotalCurrentSum.setText(formatCurrentTotalSum);
        tvTotalCurrentSum.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        tvTotalCurrentSum.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        //layCurrentSum.addView(tvTotalCurrentSum);


        final TextView tv30daysSum = new TextView(this);
        tv30daysSum.setPadding(15, 15, 0, 15);
        tv30daysSum.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        tv30daysSum.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumTextSize);
        tv30daysSum.setGravity(Gravity.RIGHT);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            tv30daysSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            tv30daysSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            tv30daysSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            tv30daysSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        tv30daysSum.setText(format130dayTotalSum);
        tv30daysSum.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        tv30daysSum.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));


        final TextView tvlbh30daysSum = new TextView(this);
        tvlbh30daysSum.setPadding(15, 15, 15, 15);
        tvlbh30daysSum.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        tvlbh30daysSum.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumTextSize);
        tvlbh30daysSum.setGravity(Gravity.RIGHT);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            tvlbh30daysSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            tvlbh30daysSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            tvlbh30daysSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            tvlbh30daysSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        tvlbh30daysSum.setText(formatlbh30dayTotalSum);
        tvlbh30daysSum.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        tvlbh30daysSum.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));

        GrandTotalSummaryNonGroup=totalARSum;
        new SharePreference(ActivityAR.this).setTotalNonGroupAR(String.valueOf(totalARSum));
        GrandShip1SummaryNonGroup=totalCurrentSum;
        GrandShip2SummaryNonGroup=total130day;
        GrandShip3SummaryNonGroup=totallbh30day;

        final TextView tvPriceFooter = new TextView(this);
        tvPriceFooter.setPadding(15, 15, 0, 15);
        tvPriceFooter.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        tvPriceFooter.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumTextSize);
        tvPriceFooter.setGravity(Gravity.RIGHT);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            tvPriceFooter.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            tvPriceFooter.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            tvPriceFooter.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            tvPriceFooter.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        tvPriceFooter.setText(formatYearTotalSum);
        tvPriceFooter.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        tvPriceFooter.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        //   layPricesFooter.addView(tvPriceFooter);

        // add table row
        final TableRow trFooter = new TableRow(this);
        TableLayout.LayoutParams trParamsFooter = new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT,
                TableLayout.LayoutParams.WRAP_CONTENT);
        trParamsFooter.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);
        trFooter.setPadding(0,0,0,0);
        trFooter.setLayoutParams(trParamsFooter);

        //tr.addView(tv);
        trFooter.addView(tvTotal);
        trFooter.addView(tvTotalARSum);
        trFooter.addView(tvTotalCurrentSum);
        trFooter.addView(tv30daysSum);
        trFooter.addView(tvlbh30daysSum);
        //   trFooter.addView(tvPriceFooter);
        tableLayoutfooterNonGroup.addView(trFooter, trParamsFooter);
        final TableRow trSep = new TableRow(this);
        TableLayout.LayoutParams trParamsSep = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                TableLayout.LayoutParams.WRAP_CONTENT);
        trParamsSep.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);

        trSep.setLayoutParams(trParamsSep);
        TextView tvSep = new TextView(this);
        TableRow.LayoutParams tvSepLay = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT);
        tvSepLay.span = 5;
        tvSep.setLayoutParams(tvSepLay);
        tvSep.setHeight(1);
        trSep.addView(tvSep);
        tableLayoutfooterNonGroup.addView(trSep, trParamsSep);
    }

    public void loadData30DaysLimitNonGroup(List<Object> objectList, TreasuryArModel data) {
        TotalRow = DBquery.COUNT(TreasuryArModel.TABLE_NAME,"Select * From TREASURY_AR where KVGR5 = '"+dataTipe+"'");
        refreshPage();
        int leftRowMargin = 0;
        int topRowMargin = 0;
        int rightRowMargin = 0;
        int bottomRowMargin = 0;
        int textSize = 0, smallTextSize = 0, mediumTextSize = 0;
        TextView textSpacer = null;
        textSize = (int) getResources().getDimension(R.dimen.font_size_large);
        smallTextSize = (int) getResources().getDimension(R.dimen.font_size_small);
        mediumTextSize = (int) getResources().getDimension(R.dimen.font_size_large);

        int rows;
        int totaldata = objectList.size();
        String TypeOfCustomer = null;
        String TotalAR= null;
        double dTotalAR = 0;
        String S110 = null;
        double d110 = 0;
        String S1120 = null;
        double d1120 = 0;
        String S2130 = null;
        double d2130 = 0;
        String TotalYear = null;
        double dTotalYear = 0;

        double  totalARSum = 0 ;
        String formatARTotalSum = null;
        double  totalS110 = 0 ;
        String formatS110Sum = null;
        double  totalS1120 = 0 ;
        String formatS1120Sum= null;
        double  totalS2130 = 0 ;
        String formatS2130Sum= null;
        double  totalsumYear = 0 ;
        String formatYearTotalSum= null;

        // linear //

        LinearLayout linearCustomerType;
        LinearLayout linearTotalAR;
        LinearLayout linearTotalYear;
        LinearLayout linearCurrent;
        LinearLayout linear30Days;
        LinearLayout linearmore30Days;
        tableMainLayoutNonGroup.removeAllViews();
        TextView tvTypeOfCustomer;
        TextView tvTotalAR;
        TextView tvCurrent;
        LinearLayout layarCurrent;
        TextView tv1s30;
        LinearLayout layar130;
        TextView tvlbh30;
        LinearLayout layarlbh130;
        TextView tvTotalYear;
        LinearLayout layarTotalYear;
        NumberFormat formatterValue = new DecimalFormat("#,###,###,###,###,###");
        for (rows= 0; rows< totaldata; rows++) {
            data = (TreasuryArModel) objectList.get(rows);
            TypeOfCustomer = data.getBezei();
          /*  dTotalAR= Double.parseDouble(data.getTotaldebt())/1000000;
            TotalAR = formatterValue.format(dTotalAR);*/
            d110= Double.parseDouble(data.getDeB10())/1000000;
            S110 = formatterValue.format(d110);
            d1120= Double.parseDouble(data.getDeB20())/1000000;
            S1120 = formatterValue.format(d110);
            d2130= Double.parseDouble(data.getDeB30())/1000000;
            S2130 = formatterValue.format(d2130);
            TotalYear = formatterValue.format(dTotalAR);
            dTotalAR= (d110+d1120+d2130);
            TotalAR = formatterValue.format(dTotalAR);
//            totalARSum += dTotalAR;
//            formatARTotalSum = formatterValue.format(totalARSum);
            totalS110+= d110;
            formatS110Sum= formatterValue.format(totalS110);
            totalS1120 += d1120;
            formatS1120Sum= formatterValue.format(totalS1120);
            totalS2130 += d2130;
            formatS2130Sum= formatterValue.format(totalS2130);
            totalsumYear += dTotalAR;
            formatYearTotalSum = formatterValue.format(totalsumYear);
            totalARSum = totalS110+totalS1120+totalS2130;
            formatARTotalSum = formatterValue.format(totalARSum);
            /* Type Of Customer */
            linearCustomerType= new LinearLayout(this);
            linearCustomerType.setOrientation(LinearLayout.VERTICAL);
            linearCustomerType.setGravity(Gravity.LEFT);
            linearCustomerType.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT));
            tvTypeOfCustomer = new TextView(this);
            tvTypeOfCustomer.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT));
            tvTypeOfCustomer.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            tvTypeOfCustomer.setGravity(Gravity.LEFT);
            tvTypeOfCustomer.setPadding(30, 15, 0, 15);
            tvTypeOfCustomer.setLines(2);
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                tvTypeOfCustomer.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
                tvTypeOfCustomer.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            }else{
                tvTypeOfCustomer.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
                tvTypeOfCustomer.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            }
            tvTypeOfCustomer.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            tvTypeOfCustomer.setTextColor(getResources().getColor(R.color.colorBlackGray));
            tvTypeOfCustomer.setText(TypeOfCustomer);
            linearCustomerType.addView(tvTypeOfCustomer);
            /* Type Of Customer */

            /* Total AR */
            linearTotalAR= new LinearLayout(this);
            linearTotalAR.setOrientation(LinearLayout.VERTICAL);
            linearTotalAR.setGravity(Gravity.LEFT);
            linearTotalAR.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT));
            tvTotalAR = new TextView(this);
            tvTotalAR.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT));
            tvTotalAR.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            tvTotalAR.setGravity(Gravity.RIGHT);
            tvTotalAR.setLines(2);
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                tvTotalAR.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
                tvTotalAR.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            }else{
                tvTotalAR.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
                tvTotalAR.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            }
            tvTotalAR.setPadding(15, 15, 0, 15);
            tvTotalAR.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            tvTotalAR.setTextColor(getResources().getColor(R.color.colorBlackGray));
            tvTotalAR.setText(TotalAR);
            linearTotalAR.addView(tvTotalAR);
            /* Total AR */

            /* Total CURRENT */
            linearCurrent= new LinearLayout(this);
            linearCurrent.setOrientation(LinearLayout.VERTICAL);
            linearCurrent.setGravity(Gravity.LEFT);
            linearCurrent.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT));
            tvCurrent = new TextView(this);
            tvCurrent.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT));
            tvCurrent.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            tvCurrent.setGravity(Gravity.RIGHT);
            tvCurrent.setLines(2);
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                tvCurrent.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
                tvCurrent.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            }else{
                tvCurrent.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
                tvCurrent.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            }
            tvCurrent.setPadding(15, 15, 0, 15);
            tvCurrent.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            tvCurrent.setTextColor(getResources().getColor(R.color.colorBlackGray));
            tvCurrent.setText(S110);
            linearCurrent.addView(tvCurrent);
            /* Total CURRENT */

            /* Total 1-30 */
            linear30Days= new LinearLayout(this);
            linear30Days.setOrientation(LinearLayout.VERTICAL);
            linear30Days.setGravity(Gravity.LEFT);
            linear30Days.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT));
            tv1s30 = new TextView(this);
            tv1s30.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT));
            tv1s30.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            tv1s30.setGravity(Gravity.RIGHT);
            tv1s30.setLines(2);
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                tv1s30.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
                tv1s30.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            }else{
                tv1s30.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
                tv1s30.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            }
            tv1s30.setPadding(15, 15, 0, 15);
            tv1s30.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            tv1s30.setTextColor(getResources().getColor(R.color.colorBlackGray));
            tv1s30.setText(S1120);
            linear30Days.addView(tv1s30);
            /* Total 1-30 */

            /* Total >30 */
            linearmore30Days= new LinearLayout(this);
            linearmore30Days.setOrientation(LinearLayout.VERTICAL);
            linearmore30Days.setGravity(Gravity.LEFT);
            linearmore30Days.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT));
            tvlbh30 = new TextView(this);
            tvlbh30.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT));
            tvlbh30.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            tvlbh30.setGravity(Gravity.RIGHT);
            tvlbh30.setLines(2);
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                tvlbh30.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
                tvlbh30.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            }else{
                tvlbh30.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
                tvlbh30.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            }
            tvlbh30.setPadding(15, 15, 15, 15);
            tvlbh30.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            tvlbh30.setTextColor(getResources().getColor(R.color.colorBlackGray));
            tvlbh30.setText(S2130);
            linearmore30Days.addView(tvlbh30);
            /* Total 1-30 */

            /* Total  */
            linearTotalYear= new LinearLayout(this);
            linearTotalYear.setOrientation(LinearLayout.VERTICAL);
            linearTotalYear.setGravity(Gravity.LEFT);
            linearTotalYear.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT));
            tvTotalYear = new TextView(this);
            tvTotalYear.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT));
            tvTotalYear.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            tvTotalYear.setGravity(Gravity.RIGHT);
            tvTotalYear.setLines(2);
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                tvTotalYear.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
                tvTotalYear.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            }else{
                tvTotalYear.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
                tvTotalYear.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            }
            tvTotalYear.setPadding(15, 15, 0, 15);
            tvTotalYear.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            tvTotalYear.setTextColor(getResources().getColor(R.color.colorBlackGray));
            tvTotalYear.setText("n/a");
            linearTotalYear.addView(tvTotalYear);
            /* Total  */


// add table row
            final TableRow tr = new TableRow(this);
            tr.setId(rows + 1);
            TableLayout.LayoutParams trParams = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                    TableLayout.LayoutParams.WRAP_CONTENT);
            trParams.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);
            tr.setPadding(0,0,0,0);
            tr.setLayoutParams(trParams);

            //tr.addView(tv);
            tr.addView(linearCustomerType);
            tr.addView(linearTotalAR);
            tr.addView(linearCurrent);
            tr.addView(linear30Days);
            tr.addView(linearmore30Days);
            // tr.addView(linearTotalYear);
            tableMainLayoutNonGroup.addView(tr, trParams);

            // add separator row
            final TableRow trSep = new TableRow(this);
            TableLayout.LayoutParams trParamsSep = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                    TableLayout.LayoutParams.WRAP_CONTENT);
            trParamsSep.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);

            trSep.setLayoutParams(trParamsSep);
            TextView tvSep = new TextView(this);
            TableRow.LayoutParams tvSepLay = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT);
            tvSepLay.span = 5;
            tvSep.setLayoutParams(tvSepLay);
            tvSep.setHeight(1);
            trSep.addView(tvSep);
            tableMainLayoutNonGroup.addView(trSep, trParamsSep);
        }

        tableLayoutfooterNonGroup.removeAllViews();
        final TextView tvTotal = new TextView(this);
        tvTotal.setPadding(15, 15, 0, 15);
        tvTotal.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));

        tvTotal.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumTextSize);
        tvTotal.setGravity(Gravity.LEFT);
        tvTotal.setText("Non Group");
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            tvTotal.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            tvTotal.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            tvTotal.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            tvTotal.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        tvTotal.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        tvTotal.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        //layTotalTitle.addView(tvTotal);

        final TextView tvTotalARSum = new TextView(this);
        tvTotalARSum.setPadding(15, 15, 0, 15);
        tvTotalARSum.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        tvTotalARSum.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumTextSize);
        tvTotalARSum.setGravity(Gravity.RIGHT);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            tvTotalARSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            tvTotalARSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            tvTotalARSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            tvTotalARSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        tvTotalARSum.setText(formatARTotalSum);

        tvTotalARSum.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        tvTotalARSum.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        //layTotalAR.addView(tvTotalARSum);

        final TextView tvTotalCurrentSum = new TextView(this);
        tvTotalCurrentSum.setPadding(15, 15, 0, 15);
        tvTotalCurrentSum.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        tvTotalCurrentSum.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumTextSize);
        tvTotalCurrentSum.setGravity(Gravity.RIGHT);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            tvTotalCurrentSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            tvTotalCurrentSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            tvTotalCurrentSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            tvTotalCurrentSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        tvTotalCurrentSum.setText(formatS110Sum);
        tvTotalCurrentSum.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        tvTotalCurrentSum.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        //layCurrentSum.addView(tvTotalCurrentSum);


        final TextView tv30daysSum = new TextView(this);
        tv30daysSum.setPadding(15, 15, 0, 15);
        tv30daysSum.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        tv30daysSum.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumTextSize);
        tv30daysSum.setGravity(Gravity.RIGHT);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            tv30daysSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            tv30daysSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            tv30daysSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            tv30daysSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        tv30daysSum.setText(formatS1120Sum);
        tv30daysSum.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        tv30daysSum.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));


        final TextView tvlbh30daysSum = new TextView(this);
        tvlbh30daysSum.setPadding(0, 15, 15, 15);
        tvlbh30daysSum.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        tvlbh30daysSum.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumTextSize);
        tvlbh30daysSum.setGravity(Gravity.RIGHT);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            tvlbh30daysSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            tvlbh30daysSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            tvlbh30daysSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            tvlbh30daysSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        tvlbh30daysSum.setText(formatS2130Sum);
        tvlbh30daysSum.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        tvlbh30daysSum.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));


        GrandTotal30DayNonGroup=totalARSum;
        GrandShip130DayNonGroup=totalS110;
        GrandShip230DayNonGroup=totalS1120;
        GrandShip330DayNonGroup=totalS2130;

        final TextView tvPriceFooter = new TextView(this);
        tvPriceFooter.setPadding(15, 15, 0, 15);
        tvPriceFooter.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        tvPriceFooter.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
        tvPriceFooter.setGravity(Gravity.RIGHT);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            tvPriceFooter.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            tvPriceFooter.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            tvPriceFooter.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            tvPriceFooter.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        tvPriceFooter.setText(formatYearTotalSum);
        tvPriceFooter.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        tvPriceFooter.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        //   layPricesFooter.addView(tvPriceFooter);

        // add table row
        final TableRow trFooter = new TableRow(this);
        TableLayout.LayoutParams trParamsFooter = new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT,
                TableLayout.LayoutParams.WRAP_CONTENT);
        trParamsFooter.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);
        trFooter.setPadding(0,0,0,0);
        trFooter.setLayoutParams(trParamsFooter);

        //tr.addView(tv);
        trFooter.addView(tvTotal);
        trFooter.addView(tvTotalARSum);
        trFooter.addView(tvTotalCurrentSum);
        trFooter.addView(tv30daysSum);
        trFooter.addView(tvlbh30daysSum);
        //   trFooter.addView(tvPriceFooter);
        tableLayoutfooterNonGroup.addView(trFooter, trParamsFooter);
        final TableRow trSep = new TableRow(this);
        TableLayout.LayoutParams trParamsSep = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                TableLayout.LayoutParams.WRAP_CONTENT);
        trParamsSep.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);

        trSep.setLayoutParams(trParamsSep);
        TextView tvSep = new TextView(this);
        TableRow.LayoutParams tvSepLay = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT);
        tvSepLay.span = 5;
        tvSep.setLayoutParams(tvSepLay);
        tvSep.setHeight(1);
        trSep.addView(tvSep);
        tableLayoutfooterNonGroup.addView(trSep, trParamsSep);

    }

    public void loadData90DaysLimitNonGroup(List<Object> objectList, TreasuryArModel data) {
        TotalRow = DBquery.COUNT(TreasuryArModel.TABLE_NAME,"Select * From TREASURY_AR where KVGR5 = '"+dataTipe+"'");
        refreshPage();
        int leftRowMargin = 0;
        int topRowMargin = 0;
        int rightRowMargin = 0;
        int bottomRowMargin = 0;
        int textSize = 0, smallTextSize = 0, mediumTextSize = 0;
        TextView textSpacer = null;
        textSize = (int) getResources().getDimension(R.dimen.font_size_large);
        smallTextSize = (int) getResources().getDimension(R.dimen.font_size_small);
        mediumTextSize = (int) getResources().getDimension(R.dimen.font_size_large);

        int rows;
        int totaldata = objectList.size();
        String TypeOfCustomer = null;
        String TotalAR = null;
        double dTotalAR = 0;
        String S3160 = null;
        double d3160= 0;
        String S6190 = null;
        double d6190 = 0;
        String S90 = null;
        double d90 = 0;
        String TotalYear = null;
        double dTotalYear = 0;

        double  totalARSum = 0 ;
        String formatARTotalSum = null;
        double  total3160 = 0 ;
        String format3160Sum = null;
        double  total6190 = 0 ;
        String format6190Sum= null;
        double  total90 = 0 ;
        String format90Sum= null;
        double  totalsumYear = 0 ;
        String formatYearTotalSum= null;

        // linear //

        LinearLayout linearCustomerType;
        LinearLayout linearTotalAR;
        LinearLayout linearTotalYear;
        LinearLayout linearCurrent;
        LinearLayout linear30Days;
        LinearLayout linearmore30Days;
        tableMainLayoutNonGroup.removeAllViews();
        TextView tvTypeOfCustomer;
        TextView tvTotalAR;
        TextView tvCurrent;
        LinearLayout layarCurrent;
        TextView tv1s30;
        LinearLayout layar130;
        TextView tvlbh30;
        LinearLayout layarlbh130;
        TextView tvTotalYear;
        LinearLayout layarTotalYear;
        NumberFormat formatterValue = new DecimalFormat("#,###,###,###,###,###");
        for (rows= 0; rows< totaldata; rows++) {
            data = (TreasuryArModel) objectList.get(rows);
            TypeOfCustomer = data.getBezei();
           /* dTotalAR= Double.parseDouble(data.getTotaldebt())/1000000;
            TotalAR = formatterValue.format(dTotalAR);*/
            d3160= Double.parseDouble(data.getDeB60())/1000000;
            S3160 = formatterValue.format(d3160);
            d6190= Double.parseDouble(data.getDeB90())/1000000;
            S6190 = formatterValue.format(d6190);
            d90= Double.parseDouble(data.getDeB99())/1000000;
            S90 = formatterValue.format(d90);
            TotalYear = formatterValue.format(dTotalAR);
            dTotalAR= (d3160+d6190+d90);
            TotalAR = formatterValue.format(dTotalAR);
//            totalARSum += dTotalAR;
//            formatARTotalSum = formatterValue.format(totalARSum);
            total3160+= d3160;
            format3160Sum= formatterValue.format(total3160);
            total6190 += d6190;
            format6190Sum= formatterValue.format(total6190);
            total90 += d90;
            format90Sum= formatterValue.format(total90);
            totalsumYear += dTotalAR;
            formatYearTotalSum = formatterValue.format(totalsumYear);
            totalARSum = total3160+total6190+total90;
            formatARTotalSum = formatterValue.format(totalARSum);
            /* Type Of Customer */
            linearCustomerType= new LinearLayout(this);
            linearCustomerType.setOrientation(LinearLayout.VERTICAL);
            linearCustomerType.setGravity(Gravity.LEFT);
            linearCustomerType.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT));
            tvTypeOfCustomer = new TextView(this);
            tvTypeOfCustomer.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT));
            tvTypeOfCustomer.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            tvTypeOfCustomer.setGravity(Gravity.LEFT);
            tvTypeOfCustomer.setPadding(30, 15, 0, 15);
            tvTypeOfCustomer.setLines(2);
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                tvTypeOfCustomer.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
                tvTypeOfCustomer.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            }else{
                tvTypeOfCustomer.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
                tvTypeOfCustomer.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            }
            tvTypeOfCustomer.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            tvTypeOfCustomer.setTextColor(getResources().getColor(R.color.colorBlackGray));
            tvTypeOfCustomer.setText(TypeOfCustomer);
            linearCustomerType.addView(tvTypeOfCustomer);
            /* Type Of Customer */

            /* Total AR */
            linearTotalAR= new LinearLayout(this);
            linearTotalAR.setOrientation(LinearLayout.VERTICAL);
            linearTotalAR.setGravity(Gravity.LEFT);
            linearTotalAR.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT));
            tvTotalAR = new TextView(this);
            tvTotalAR.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT));
            tvTotalAR.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            tvTotalAR.setGravity(Gravity.RIGHT);
            tvTotalAR.setLines(2);
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                tvTotalAR.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
                tvTotalAR.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            }else{
                tvTotalAR.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
                tvTotalAR.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            }
            tvTotalAR.setPadding(15, 15, 0, 15);
            tvTotalAR.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            tvTotalAR.setTextColor(getResources().getColor(R.color.colorBlackGray));
            tvTotalAR.setText(TotalAR);
            linearTotalAR.addView(tvTotalAR);
            /* Total AR */

            /* Total CURRENT */
            linearCurrent= new LinearLayout(this);
            linearCurrent.setOrientation(LinearLayout.VERTICAL);
            linearCurrent.setGravity(Gravity.LEFT);
            linearCurrent.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT));
            tvCurrent = new TextView(this);
            tvCurrent.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT));
            tvCurrent.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            tvCurrent.setGravity(Gravity.RIGHT);
            tvCurrent.setLines(2);
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                tvCurrent.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
                tvCurrent.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            }else{
                tvCurrent.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
                tvCurrent.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            }
            tvCurrent.setPadding(15, 15, 0, 15);
            tvCurrent.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            tvCurrent.setTextColor(getResources().getColor(R.color.colorBlackGray));
            tvCurrent.setText(S3160);
            linearCurrent.addView(tvCurrent);
            /* Total CURRENT */

            /* Total 1-30 */
            linear30Days= new LinearLayout(this);
            linear30Days.setOrientation(LinearLayout.VERTICAL);
            linear30Days.setGravity(Gravity.LEFT);
            linear30Days.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT));
            tv1s30 = new TextView(this);
            tv1s30.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT));
            tv1s30.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            tv1s30.setGravity(Gravity.RIGHT);
            tv1s30.setLines(2);
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                tv1s30.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
                tv1s30.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            }else{
                tv1s30.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
                tv1s30.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            }
            tv1s30.setPadding(15, 15, 0, 15);
            tv1s30.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            tv1s30.setTextColor(getResources().getColor(R.color.colorBlackGray));
            tv1s30.setText(S6190);
            linear30Days.addView(tv1s30);
            /* Total 1-30 */

            /* Total >30 */
            linearmore30Days= new LinearLayout(this);
            linearmore30Days.setOrientation(LinearLayout.VERTICAL);
            linearmore30Days.setGravity(Gravity.LEFT);
            linearmore30Days.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT));
            tvlbh30 = new TextView(this);
            tvlbh30.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT));
            tvlbh30.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            tvlbh30.setGravity(Gravity.RIGHT);
            tvlbh30.setLines(2);
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                tvlbh30.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
                tvlbh30.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            }else{
                tvlbh30.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
                tvlbh30.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            }
            tvlbh30.setPadding(0, 15, 15, 15);
            tvlbh30.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            tvlbh30.setTextColor(getResources().getColor(R.color.colorBlackGray));
            tvlbh30.setText(S90);
            linearmore30Days.addView(tvlbh30);
            /* Total 1-30 */

            /* Total  */
            linearTotalYear= new LinearLayout(this);
            linearTotalYear.setOrientation(LinearLayout.VERTICAL);
            linearTotalYear.setGravity(Gravity.LEFT);
            linearTotalYear.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.MATCH_PARENT));
            tvTotalYear = new TextView(this);
            tvTotalYear.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT));
            tvTotalYear.setTextSize(TypedValue.COMPLEX_UNIT_PX, textSize);
            tvTotalYear.setGravity(Gravity.RIGHT);
            tvTotalYear.setLines(2);
            if (orientation == Configuration.ORIENTATION_PORTRAIT) {
                tvTotalYear.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
                tvTotalYear.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            }else{
                tvTotalYear.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
                tvTotalYear.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            }
            tvTotalYear.setPadding(15, 15, 0, 15);
            tvTotalYear.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            tvTotalYear.setTextColor(getResources().getColor(R.color.colorBlackGray));
            tvTotalYear.setText("n/a");
            linearTotalYear.addView(tvTotalYear);
            /* Total  */


// add table row
            final TableRow tr = new TableRow(this);
            tr.setId(rows + 1);
            TableLayout.LayoutParams trParams = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                    TableLayout.LayoutParams.WRAP_CONTENT);
            trParams.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);
            tr.setPadding(0,0,0,0);
            tr.setLayoutParams(trParams);

            //tr.addView(tv);
            tr.addView(linearCustomerType);
            tr.addView(linearTotalAR);
            tr.addView(linearCurrent);
            tr.addView(linear30Days);
            tr.addView(linearmore30Days);
            //   tr.addView(linearTotalYear);
            tableMainLayoutNonGroup.addView(tr, trParams);

            // add separator row
            final TableRow trSep = new TableRow(this);
            TableLayout.LayoutParams trParamsSep = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                    TableLayout.LayoutParams.WRAP_CONTENT);
            trParamsSep.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);

            trSep.setLayoutParams(trParamsSep);
            TextView tvSep = new TextView(this);
            TableRow.LayoutParams tvSepLay = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                    TableRow.LayoutParams.WRAP_CONTENT);
            tvSepLay.span = 5;
            tvSep.setLayoutParams(tvSepLay);
            tvSep.setHeight(1);
            trSep.addView(tvSep);
            tableMainLayoutNonGroup.addView(trSep, trParamsSep);
        }


        tableLayoutfooterNonGroup.removeAllViews();
        final TextView tvTotal = new TextView(this);
        tvTotal.setPadding(15, 15, 0, 15);
        tvTotal.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));

        tvTotal.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumTextSize);
        tvTotal.setGravity(Gravity.LEFT);
        tvTotal.setText("Non Group");
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            tvTotal.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            tvTotal.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            tvTotal.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            tvTotal.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        tvTotal.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        tvTotal.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        //layTotalTitle.addView(tvTotal);

        final TextView tvTotalARSum = new TextView(this);
        tvTotalARSum.setPadding(15, 15, 0, 15);
        tvTotalARSum.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        tvTotalARSum.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumTextSize);
        tvTotalARSum.setGravity(Gravity.RIGHT);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            tvTotalARSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            tvTotalARSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            tvTotalARSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            tvTotalARSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        tvTotalARSum.setText(formatARTotalSum);

        tvTotalARSum.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        tvTotalARSum.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        //layTotalAR.addView(tvTotalARSum);

        final TextView tvTotalCurrentSum = new TextView(this);
        tvTotalCurrentSum.setPadding(15, 15, 0, 15);
        tvTotalCurrentSum.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        tvTotalCurrentSum.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumTextSize);
        tvTotalCurrentSum.setGravity(Gravity.RIGHT);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            tvTotalCurrentSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            tvTotalCurrentSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            tvTotalCurrentSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            tvTotalCurrentSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        tvTotalCurrentSum.setText(format3160Sum);
        tvTotalCurrentSum.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        tvTotalCurrentSum.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        //layCurrentSum.addView(tvTotalCurrentSum);


        final TextView tv30daysSum = new TextView(this);
        tv30daysSum.setPadding(15, 15, 0, 15);
        tv30daysSum.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        tv30daysSum.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumTextSize);
        tv30daysSum.setGravity(Gravity.RIGHT);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            tv30daysSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            tv30daysSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            tv30daysSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            tv30daysSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        tv30daysSum.setText(format6190Sum);

        tv30daysSum.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        tv30daysSum.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        // layTotal30DaySum.addView(tv30daysSum);

        final TextView tvlbh30daysSum = new TextView(this);
        tvlbh30daysSum.setPadding(0, 15, 15, 15);
        tvlbh30daysSum.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        tvlbh30daysSum.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumTextSize);
        tvlbh30daysSum.setGravity(Gravity.RIGHT);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            tvlbh30daysSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            tvlbh30daysSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            tvlbh30daysSum.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            tvlbh30daysSum.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        tvlbh30daysSum.setText(format90Sum);
        tvlbh30daysSum.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        tvlbh30daysSum.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));

        GrandTotalMore30DayNonGroup=totalARSum;
        GrandShip1More30DayNonGroup=total3160;
        GrandShip2More30DayNonGroup=total6190;
        GrandShip3More30DayNonGroup=total90;


        final TextView tvPriceFooter = new TextView(this);
        tvPriceFooter.setPadding(15, 15, 0, 15);
        tvPriceFooter.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        tvPriceFooter.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumTextSize);
        tvPriceFooter.setGravity(Gravity.RIGHT);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            tvPriceFooter.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            tvPriceFooter.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            tvPriceFooter.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            tvPriceFooter.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        tvPriceFooter.setText(formatYearTotalSum);
        tvPriceFooter.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        tvPriceFooter.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        //   layPricesFooter.addView(tvPriceFooter);

        // add table row
        final TableRow trFooter = new TableRow(this);
        TableLayout.LayoutParams trParamsFooter = new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT,
                TableLayout.LayoutParams.WRAP_CONTENT);
        trParamsFooter.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);
        trFooter.setPadding(0,0,0,0);
        trFooter.setLayoutParams(trParamsFooter);

        //tr.addView(tv);
        trFooter.addView(tvTotal);
        trFooter.addView(tvTotalARSum);
        trFooter.addView(tvTotalCurrentSum);
        trFooter.addView(tv30daysSum);
        trFooter.addView(tvlbh30daysSum);
        //   trFooter.addView(tvPriceFooter);
        tableLayoutfooterNonGroup.addView(trFooter, trParamsFooter);
        final TableRow trSep = new TableRow(this);
        TableLayout.LayoutParams trParamsSep = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                TableLayout.LayoutParams.WRAP_CONTENT);
        trParamsSep.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);

        trSep.setLayoutParams(trParamsSep);
        TextView tvSep = new TextView(this);
        TableRow.LayoutParams tvSepLay = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT);
        tvSepLay.span = 5;
        tvSep.setLayoutParams(tvSepLay);
        tvSep.setHeight(1);
        trSep.addView(tvSep);
        tableLayoutfooterNonGroup.addView(trSep, trParamsSep);

    }

    public void loadGrandTotalSummary() {
        NumberFormat formatterValue = new DecimalFormat("#,###,###,###,###,###");
        double Total =GrandTotalSummary+GrandTotalSummaryNonGroup;
        String FormatTotal = formatterValue.format(Total);
        double TotalShip1 =GrandShip1Summary+GrandShip1SummaryNonGroup;
        String FormatShip1 = formatterValue.format(TotalShip1);
        double TotalShip2 =GrandShip2Summary+GrandShip2SummaryNonGroup;
        String FormatShip2= formatterValue.format(TotalShip2);
        double TotalShip3 =GrandShip3Summary+GrandShip3SummaryNonGroup;
        String FormatShip3= formatterValue.format(TotalShip3);
        int leftRowMargin = 0;
        int topRowMargin = 0;
        int rightRowMargin = 0;
        int bottomRowMargin = 0;
        tableGrandTotalLayout.removeAllViews();
        //    LinearLayout LayarHeaderCustomerType= new LinearLayout(this);
        TextView textSpacer = new TextView(this);
        textSpacer.setText("");
        int textSize = (int) getResources().getDimension(R.dimen.font_size_normal);
        int mediumtextSize = (int) getResources().getDimension(R.dimen.font_size_large);
        LinearLayout linearTitleCustomerType= new LinearLayout(this);
        linearTitleCustomerType.setOrientation(LinearLayout.VERTICAL);
        linearTitleCustomerType.setGravity(Gravity.LEFT);
        linearTitleCustomerType.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT));
        TextView txtCustomerType = new TextView(this);
        txtCustomerType.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        txtCustomerType.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumtextSize);
        txtCustomerType.setText("Total");
        txtCustomerType.setLines(1);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            txtCustomerType.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            txtCustomerType.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            txtCustomerType.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            txtCustomerType.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        txtCustomerType.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        txtCustomerType.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        txtCustomerType.setPadding(15, 15, 0, 15);
        linearTitleCustomerType.addView(txtCustomerType);
        //      LayarHeaderCustomerType.addView(txtCustomerType);

        LinearLayout linearTotalAR= new LinearLayout(this);
        linearTotalAR.setOrientation(LinearLayout.VERTICAL);
        linearTotalAR.setGravity(Gravity.CENTER);
        linearTotalAR.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT));
        TextView txtTotalAR = new TextView(this);
        txtTotalAR.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        txtTotalAR.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumtextSize);
        txtTotalAR.setGravity(Gravity.RIGHT);
        txtTotalAR.setText(FormatTotal);
//        new SharePreference(ActivityAR.this).setTotalAR(String.valueOf(Total));
        txtTotalAR.setLines(1);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            txtTotalAR.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            txtTotalAR.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            txtTotalAR.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            txtTotalAR.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        txtTotalAR.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        txtTotalAR.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        txtTotalAR.setPadding(15, 15, 0, 15);
        linearTotalAR.addView(txtTotalAR);


        LinearLayout linearTitleCurrent= new LinearLayout(this);
        linearTitleCurrent.setOrientation(LinearLayout.VERTICAL);
        linearTitleCurrent.setGravity(Gravity.LEFT);
        linearTitleCurrent.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT));
        TextView txtCurrent = new TextView(this);
        txtCurrent.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        txtCurrent.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumtextSize);
        txtCurrent.setGravity(Gravity.RIGHT);
        txtCurrent.setText(FormatShip1);

        txtCurrent.setLines(1);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            txtCurrent.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            txtCurrent.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            txtCurrent.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            txtCurrent.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        txtCurrent.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        txtCurrent.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        txtCurrent.setPadding(15, 15, 0, 15);
        linearTitleCurrent.addView(txtCurrent);


        LinearLayout linearTitle30Days= new LinearLayout(this);
        linearTitle30Days.setOrientation(LinearLayout.VERTICAL);
        linearTitle30Days.setGravity(Gravity.LEFT);
        linearTitle30Days.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT));
        TextView txt30Days = new TextView(this);
        txt30Days.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        txt30Days.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumtextSize);
        txt30Days.setGravity(Gravity.RIGHT);
        txt30Days.setText(FormatShip2);
        txt30Days.setLines(1);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            txt30Days.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            txt30Days.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            txt30Days.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            txt30Days.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        txt30Days.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        txt30Days.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        txt30Days.setPadding(15, 15, 0, 15);
        linearTitle30Days.addView(txt30Days);


        LinearLayout linearTitleMore30Days= new LinearLayout(this);
        linearTitleMore30Days.setOrientation(LinearLayout.VERTICAL);
        linearTitleMore30Days.setGravity(Gravity.LEFT);
        linearTitleMore30Days.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT));
        TextView txtmore30Days = new TextView(this);
        txtmore30Days.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        txtmore30Days.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumtextSize);
        txtmore30Days.setGravity(Gravity.RIGHT);
        txtmore30Days.setText(FormatShip3);
        txtmore30Days.setLines(1);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            txtmore30Days.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            txtmore30Days.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            txtmore30Days.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            txtmore30Days.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        txtmore30Days.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        txtmore30Days.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        txtmore30Days.setPadding(15, 15, 15, 15);
        linearTitleMore30Days.addView(txtmore30Days);


        LinearLayout linearTitleLastYear= new LinearLayout(this);
        linearTitleLastYear.setOrientation(LinearLayout.VERTICAL);
        linearTitleLastYear.setGravity(Gravity.RIGHT);
        linearTitleLastYear.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT));
        TextView txtTotalLastYear = new TextView(this);
        txtTotalLastYear.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT));
        txtTotalLastYear.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumtextSize);
        txtTotalLastYear.setGravity(Gravity.RIGHT);
        txtTotalLastYear.setText("Last Year");
        txtTotalLastYear.setLines(1);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            txtTotalLastYear.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            txtTotalLastYear.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            txtTotalLastYear.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            txtTotalLastYear.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        txtTotalLastYear.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        txtTotalLastYear.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        txtTotalLastYear.setPadding(15, 15, 0, 15);
        linearTitleLastYear.addView(txtTotalLastYear);


        final TableRow tr = new TableRow(this);
        TableLayout.LayoutParams trParams = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                TableLayout.LayoutParams.MATCH_PARENT);
        trParams.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);
        tr.setPadding(0,0,0,0);
        tr.setLayoutParams(trParams);
        //tr.addView(tv);
        tr.addView(linearTitleCustomerType);
        tr.addView(linearTotalAR);
        tr.addView(linearTitleCurrent);
        tr.addView(linearTitle30Days);
        tr.addView(linearTitleMore30Days);
        //  tr.addView(linearTitleLastYear);
        tableGrandTotalLayout.addView(tr, trParams);
        final TableRow trSep = new TableRow(this);
        TableLayout.LayoutParams trParamsSep = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                TableLayout.LayoutParams.WRAP_CONTENT);
        trParamsSep.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);

        trSep.setLayoutParams(trParamsSep);
        TextView tvSep = new TextView(this);
        TableRow.LayoutParams tvSepLay = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT);
        tvSepLay.span = 5;
        tvSep.setLayoutParams(tvSepLay);
        tvSep.setHeight(1);
        trSep.addView(tvSep);
        tableGrandTotalLayout.addView(trSep, trParamsSep);
    }
    public void loadGrandTotal30Days() {
        NumberFormat formatterValue = new DecimalFormat("#,###,###,###,###,###");
        double Total =GrandTotal30Day+GrandTotal30DayNonGroup;
        String FormatTotal = formatterValue.format(Total);
        double TotalShip1 =GrandShip130Day+GrandShip130DayNonGroup;
        String FormatShip1 = formatterValue.format(TotalShip1);
        double TotalShip2 =GrandShip230Day+GrandShip230DayNonGroup;
        String FormatShip2= formatterValue.format(TotalShip2);
        double TotalShip3 =GrandShip330Day+GrandShip330DayNonGroup;
        String FormatShip3= formatterValue.format(TotalShip3);

        int leftRowMargin = 0;
        int topRowMargin = 0;
        int rightRowMargin = 0;
        int bottomRowMargin = 0;
        tableGrandTotalLayout.removeAllViews();
        //    LinearLayout LayarHeaderCustomerType= new LinearLayout(this);
        TextView textSpacer = new TextView(this);
        textSpacer.setText("");
        int textSize = (int) getResources().getDimension(R.dimen.font_size_normal);
        int mediumtextSize = (int) getResources().getDimension(R.dimen.font_size_large);
        LinearLayout linearTitleCustomerType= new LinearLayout(this);
        linearTitleCustomerType.setOrientation(LinearLayout.VERTICAL);
        linearTitleCustomerType.setGravity(Gravity.LEFT);
        linearTitleCustomerType.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT));
        TextView txtCustomerType = new TextView(this);
        txtCustomerType.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        txtCustomerType.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumtextSize);
        txtCustomerType.setText("Total");
        txtCustomerType.setLines(1);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            txtCustomerType.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            txtCustomerType.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            txtCustomerType.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            txtCustomerType.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        txtCustomerType.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        txtCustomerType.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        txtCustomerType.setPadding(15, 15, 0, 15);
        linearTitleCustomerType.addView(txtCustomerType);
        //      LayarHeaderCustomerType.addView(txtCustomerType);

        LinearLayout linearTotalAR= new LinearLayout(this);
        linearTotalAR.setOrientation(LinearLayout.VERTICAL);
        linearTotalAR.setGravity(Gravity.LEFT);
        linearTotalAR.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT));
        TextView txtTotalAR = new TextView(this);
        txtTotalAR.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        txtTotalAR.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumtextSize);
        txtTotalAR.setGravity(Gravity.RIGHT);
        txtTotalAR.setText(FormatTotal);
        txtTotalAR.setLines(1);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            txtTotalAR.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            txtTotalAR.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            txtTotalAR.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            txtTotalAR.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        txtTotalAR.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        txtTotalAR.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        txtTotalAR.setPadding(15, 15, 0, 15);
        linearTotalAR.addView(txtTotalAR);


        LinearLayout linearTitleCurrent= new LinearLayout(this);
        linearTitleCurrent.setOrientation(LinearLayout.VERTICAL);
        linearTitleCurrent.setGravity(Gravity.LEFT);
        linearTitleCurrent.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT));
        TextView txtCurrent = new TextView(this);
        txtCurrent.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        txtCurrent.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumtextSize);
        txtCurrent.setGravity(Gravity.RIGHT);
        txtCurrent.setText(FormatShip1);
        txtCurrent.setLines(1);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            txtCurrent.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            txtCurrent.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            txtCurrent.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            txtCurrent.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        txtCurrent.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        txtCurrent.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        txtCurrent.setPadding(15, 15, 0, 15);
        linearTitleCurrent.addView(txtCurrent);


        LinearLayout linearTitle30Days= new LinearLayout(this);
        linearTitle30Days.setOrientation(LinearLayout.VERTICAL);
        linearTitle30Days.setGravity(Gravity.LEFT);
        linearTitle30Days.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT));
        TextView txt30Days = new TextView(this);
        txt30Days.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        txt30Days.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumtextSize);
        txt30Days.setGravity(Gravity.RIGHT);
        txt30Days.setText(FormatShip2);
        txt30Days.setLines(1);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            txt30Days.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            txt30Days.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            txt30Days.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            txt30Days.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        txt30Days.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        txt30Days.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        txt30Days.setPadding(15, 15, 0, 15);
        linearTitle30Days.addView(txt30Days);


        LinearLayout linearTitleMore30Days= new LinearLayout(this);
        linearTitleMore30Days.setOrientation(LinearLayout.VERTICAL);
        linearTitleMore30Days.setGravity(Gravity.LEFT);
        linearTitleMore30Days.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT));
        TextView txtmore30Days = new TextView(this);
        txtmore30Days.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        txtmore30Days.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumtextSize);
        txtmore30Days.setGravity(Gravity.RIGHT);
        txtmore30Days.setText(FormatShip3);
        txtmore30Days.setLines(1);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            txtmore30Days.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            txtmore30Days.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            txtmore30Days.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            txtmore30Days.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        txtmore30Days.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        txtmore30Days.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        txtmore30Days.setPadding(15, 15, 15, 15);
        linearTitleMore30Days.addView(txtmore30Days);


        LinearLayout linearTitleLastYear= new LinearLayout(this);
        linearTitleLastYear.setOrientation(LinearLayout.VERTICAL);
        linearTitleLastYear.setGravity(Gravity.RIGHT);
        linearTitleLastYear.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT));
        TextView txtTotalLastYear = new TextView(this);
        txtTotalLastYear.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT));
        txtTotalLastYear.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumtextSize);
        txtTotalLastYear.setGravity(Gravity.RIGHT);
        txtTotalLastYear.setText("Last Year");
        txtTotalLastYear.setLines(1);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            txtTotalLastYear.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            txtTotalLastYear.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            txtTotalLastYear.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            txtTotalLastYear.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        txtTotalLastYear.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        txtTotalLastYear.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        txtTotalLastYear.setPadding(15, 15, 0, 15);
        linearTitleLastYear.addView(txtTotalLastYear);


        final TableRow tr = new TableRow(this);
        TableLayout.LayoutParams trParams = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                TableLayout.LayoutParams.MATCH_PARENT);
        trParams.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);
        tr.setPadding(0,0,0,0);
        tr.setLayoutParams(trParams);
        //tr.addView(tv);
        tr.addView(linearTitleCustomerType);
        tr.addView(linearTotalAR);
        tr.addView(linearTitleCurrent);
        tr.addView(linearTitle30Days);
        tr.addView(linearTitleMore30Days);
        //  tr.addView(linearTitleLastYear);
        tableGrandTotalLayout.addView(tr, trParams);
        final TableRow trSep = new TableRow(this);
        TableLayout.LayoutParams trParamsSep = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                TableLayout.LayoutParams.WRAP_CONTENT);
        trParamsSep.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);

        trSep.setLayoutParams(trParamsSep);
        TextView tvSep = new TextView(this);
        TableRow.LayoutParams tvSepLay = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT);
        tvSepLay.span = 5;
        tvSep.setLayoutParams(tvSepLay);
        tvSep.setHeight(1);
        trSep.addView(tvSep);
        tableGrandTotalLayout.addView(trSep, trParamsSep);
    }
    public void loadGrandTotalMore30Days() {
        NumberFormat formatterValue = new DecimalFormat("#,###,###,###,###,###");
        double Total =GrandTotalMore30Day+GrandTotalMore30DayNonGroup;
        String FormatTotal = formatterValue.format(Total);
        double TotalShip1 =GrandShip1More30Day+GrandShip1More30DayNonGroup;
        String FormatShip1 = formatterValue.format(TotalShip1);
        double TotalShip2 =GrandShip2More30Day+GrandShip2More30DayNonGroup;
        String FormatShip2= formatterValue.format(TotalShip2);
        double TotalShip3 =GrandShip3More30Day+GrandShip3More30DayNonGroup;
        String FormatShip3= formatterValue.format(TotalShip3);
        int leftRowMargin = 0;
        int topRowMargin = 0;
        int rightRowMargin = 0;
        int bottomRowMargin = 0;
        tableGrandTotalLayout.removeAllViews();
        //    LinearLayout LayarHeaderCustomerType= new LinearLayout(this);
        TextView textSpacer = new TextView(this);
        textSpacer.setText("");
        int textSize = (int) getResources().getDimension(R.dimen.font_size_normal);
        int mediumtextSize = (int) getResources().getDimension(R.dimen.font_size_large);
        LinearLayout linearTitleCustomerType= new LinearLayout(this);
        linearTitleCustomerType.setOrientation(LinearLayout.VERTICAL);
        linearTitleCustomerType.setGravity(Gravity.LEFT);
        linearTitleCustomerType.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT));
        TextView txtCustomerType = new TextView(this);
        txtCustomerType.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        txtCustomerType.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumtextSize);
        txtCustomerType.setText("Total");
        txtCustomerType.setLines(1);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            txtCustomerType.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            txtCustomerType.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            txtCustomerType.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            txtCustomerType.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        txtCustomerType.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        txtCustomerType.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        txtCustomerType.setPadding(15, 15, 0, 15);
        linearTitleCustomerType.addView(txtCustomerType);
        //      LayarHeaderCustomerType.addView(txtCustomerType);

        LinearLayout linearTotalAR= new LinearLayout(this);
        linearTotalAR.setOrientation(LinearLayout.VERTICAL);
        linearTotalAR.setGravity(Gravity.LEFT);
        linearTotalAR.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT));
        TextView txtTotalAR = new TextView(this);
        txtTotalAR.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        txtTotalAR.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumtextSize);
        txtTotalAR.setGravity(Gravity.RIGHT);
        txtTotalAR.setText(FormatTotal);
        txtTotalAR.setLines(1);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            txtTotalAR.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            txtTotalAR.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            txtTotalAR.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            txtTotalAR.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        txtTotalAR.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        txtTotalAR.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        txtTotalAR.setPadding(15, 15, 0, 15);
        linearTotalAR.addView(txtTotalAR);


        LinearLayout linearTitleCurrent= new LinearLayout(this);
        linearTitleCurrent.setOrientation(LinearLayout.VERTICAL);
        linearTitleCurrent.setGravity(Gravity.LEFT);
        linearTitleCurrent.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT));
        TextView txtCurrent = new TextView(this);
        txtCurrent.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        txtCurrent.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumtextSize);
        txtCurrent.setGravity(Gravity.RIGHT);
        txtCurrent.setText(FormatShip1);
        txtCurrent.setLines(1);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            txtCurrent.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            txtCurrent.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            txtCurrent.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            txtCurrent.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        txtCurrent.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        txtCurrent.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        txtCurrent.setPadding(15, 15, 0, 15);
        linearTitleCurrent.addView(txtCurrent);


        LinearLayout linearTitle30Days= new LinearLayout(this);
        linearTitle30Days.setOrientation(LinearLayout.VERTICAL);
        linearTitle30Days.setGravity(Gravity.LEFT);
        linearTitle30Days.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT));
        TextView txt30Days = new TextView(this);
        txt30Days.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        txt30Days.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumtextSize);
        txt30Days.setGravity(Gravity.RIGHT);
        txt30Days.setText(FormatShip2);
        txt30Days.setLines(1);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            txt30Days.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            txt30Days.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            txt30Days.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            txt30Days.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        txt30Days.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        txt30Days.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        txt30Days.setPadding(15, 15, 0, 15);
        linearTitle30Days.addView(txt30Days);


        LinearLayout linearTitleMore30Days= new LinearLayout(this);
        linearTitleMore30Days.setOrientation(LinearLayout.VERTICAL);
        linearTitleMore30Days.setGravity(Gravity.LEFT);
        linearTitleMore30Days.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT));
        TextView txtmore30Days = new TextView(this);
        txtmore30Days.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT));
        txtmore30Days.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumtextSize);
        txtmore30Days.setGravity(Gravity.RIGHT);
        txtmore30Days.setText(FormatShip3);
        txtmore30Days.setLines(1);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            txtmore30Days.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            txtmore30Days.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            txtmore30Days.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            txtmore30Days.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        txtmore30Days.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        txtmore30Days.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        txtmore30Days.setPadding(15, 15, 15, 15);
        linearTitleMore30Days.addView(txtmore30Days);


        LinearLayout linearTitleLastYear= new LinearLayout(this);
        linearTitleLastYear.setOrientation(LinearLayout.VERTICAL);
        linearTitleLastYear.setGravity(Gravity.RIGHT);
        linearTitleLastYear.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT));
        TextView txtTotalLastYear = new TextView(this);
        txtTotalLastYear.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT));
        txtTotalLastYear.setTextSize(TypedValue.COMPLEX_UNIT_PX, mediumtextSize);
        txtTotalLastYear.setGravity(Gravity.RIGHT);
        txtTotalLastYear.setText("Last Year");
        txtTotalLastYear.setLines(1);
        if (orientation == Configuration.ORIENTATION_PORTRAIT) {
            txtTotalLastYear.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
            txtTotalLastYear.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_potrait));
        }else{
            txtTotalLastYear.setMinWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
            txtTotalLastYear.setMaxWidth(getResources().getDimensionPixelSize(R.dimen.setmax_width_land));
        }
        txtTotalLastYear.setTextColor(getResources().getColor(R.color.colorWhiteFont));
        txtTotalLastYear.setBackgroundColor(getResources().getColor(R.color.Blue_Ivy));
        txtTotalLastYear.setPadding(15, 15, 0, 15);
        linearTitleLastYear.addView(txtTotalLastYear);


        final TableRow tr = new TableRow(this);
        TableLayout.LayoutParams trParams = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                TableLayout.LayoutParams.MATCH_PARENT);
        trParams.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);
        tr.setPadding(0,0,0,0);
        tr.setLayoutParams(trParams);
        //tr.addView(tv);
        tr.addView(linearTitleCustomerType);
        tr.addView(linearTotalAR);
        tr.addView(linearTitleCurrent);
        tr.addView(linearTitle30Days);
        tr.addView(linearTitleMore30Days);
        //  tr.addView(linearTitleLastYear);
        tableGrandTotalLayout.addView(tr, trParams);
        final TableRow trSep = new TableRow(this);
        TableLayout.LayoutParams trParamsSep = new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT,
                TableLayout.LayoutParams.WRAP_CONTENT);
        trParamsSep.setMargins(leftRowMargin, topRowMargin, rightRowMargin, bottomRowMargin);

        trSep.setLayoutParams(trParamsSep);
        TextView tvSep = new TextView(this);
        TableRow.LayoutParams tvSepLay = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.WRAP_CONTENT);
        tvSepLay.span = 5;
        tvSep.setLayoutParams(tvSepLay);
        tvSep.setHeight(1);
        trSep.addView(tvSep);
        tableGrandTotalLayout.addView(trSep, trParamsSep);
    }
    void refreshPage(){
        int TotalDataHasil = 0;
        if(TotalRow<= firstPage){
            layarPage.setVisibility(View.GONE);
        }else{
            layarPage.setVisibility(View.VISIBLE);
            if(Page>1){
                imgBack.setVisibility(View.VISIBLE);
            }
            if(Page==1){
                imgBack.setVisibility(View.GONE);
            }
            if(TotalRowPage>TotalRow ||TotalRowPage==TotalRow){
                TotalDataHasil = TotalRowPage-(TotalRowPage-TotalRow);
                imgNext.setVisibility(View.GONE);


            }
            if(TotalRowPage<TotalRow){
                TotalDataHasil = TotalRowPage;
                imgNext.setVisibility(View.GONE);
            }
        }
        txtInitPage.setText("Page "+String.valueOf(Page)+" ( Data = "+String.valueOf(TotalDataHasil)+" "+" / "+String.valueOf(TotalRow)+" ) ");
    }
    long LongUpdatedDate(String dateString){
        long UpdatedDateTime = 0;
        try {
            Date date = formatUpdateDated.parse(dateString);

            UpdatedDateTime = date.getTime();

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return UpdatedDateTime;
    }

    private boolean InitLastCreatedData() {
        DBquery.openTransaction();
        Object objectData;
        String sqldb_query = "SELECT DISTINCT ("+TreasuryArModel.XML_CREATEDDATE+") from TREASURY_AR  ORDER BY "+ TreasuryArModel.XML_CREATEDDATE +" desc limit 0,1";
        objectData = DBquery.getDataFirstRaw(sqldb_query, TreasuryArModel.TABLE_NAME, null);
        DBquery.closeTransaction();
        TreasuryArModel treasuryArModel = (TreasuryArModel)objectData;
        new SharePreference(ActivityAR.this).setFormYear(treasuryArModel.getErdat().substring(0,10));
        if(!new SharePreference(ActivityAR.this).isFormYear().equalsIgnoreCase("")){
            String DateLast = new SharePreference(ActivityAR.this).isFormYear();
            et_year.setText(DateLast);
            return true;
        }else{
            return false;
        }
    }

    // MEMANGGIL DATA AR DARI FORM AR //
    private void CallAR(){
        String BA = new SharePreference(ActivityAR.this).isBA();
        if(!BA.equalsIgnoreCase("All")){
            DataSourceARBA();
            DataSourceARNonGroupBA();
        }else{
            DataSourceAR();
            DataSourceARNonGroup();
        }

        LayarForm.setVisibility(View.GONE);
        txtInfoDate.setVisibility(View.VISIBLE);
        txtInfoDate.setText(new SharePreference(ActivityAR.this).isFormYear());
        txtInfoDate.setOnClickListener(this);
    }


    private void InitDataNullGroup(){
        tableHeaderLayout.removeAllViews();
        tableMainLayout.removeAllViews();
        tableLayoutfooter.removeAllViews();
        txtInitPage.setText("Empty");
        imgNext.setVisibility(View.GONE);
        imgBack.setVisibility(View.GONE);
        layarHari.setVisibility(View.GONE);
        relativeLayoutOverdue.setVisibility(View.GONE);
        relativeInfoDate.setVisibility(View.GONE);
        LayarForm.setVisibility(View.VISIBLE);
        layoutEmpty.setVisibility(View.GONE);
    }
    private void InitDataNullNonGroup(){
        tableMainLayoutNonGroup.removeAllViews();
        tableLayoutfooterNonGroup.removeAllViews();
        tableGrandTotalLayout.removeAllViews();
        txtInitPage.setText("Empty");
        imgNext.setVisibility(View.GONE);
        imgBack.setVisibility(View.GONE);
        layarHari.setVisibility(View.GONE);
        relativeLayoutOverdue.setVisibility(View.GONE);
        relativeInfoDate.setVisibility(View.GONE);
        LayarForm.setVisibility(View.VISIBLE);
        layoutEmpty.setVisibility(View.GONE);
    }

    public static String formateDateFromstring(String inputFormat, String outputFormat, String inputDate){

        Date parsed = null;
        String outputDate = "";

        SimpleDateFormat df_input = new SimpleDateFormat(inputFormat, java.util.Locale.getDefault());
        SimpleDateFormat df_output = new SimpleDateFormat(outputFormat, java.util.Locale.getDefault());

        try {
            parsed = df_input.parse(inputDate);
            outputDate = df_output.format(parsed);

        } catch (ParseException e) {
            Log.i("AR", "ParseException - dateFormat");
        }

        return outputDate;

    }

    void ARDashboard(){
        if(new SharePreference(ActivityAR.this).isTOTALAR().equals("0")){
            double Total =GrandTotalSummary+GrandTotalSummaryNonGroup;
            new SharePreference(ActivityAR.this).setTotalAR(String.valueOf(Total));
        }else{

        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        // check if the request code is same as what is passed  here it is 2
        if(requestCode==99)
        {
            Bundle extras = data.getExtras();
            String dateString = extras.getString("tanggal");
            String message = extras.getString("message");
            if(dateString!=null){
                String intent = data.getStringExtra("tanggal");
                String DateIntent = intent.substring(0,10);
                et_year.setText(DateIntent);
                new SharePreference(ActivityAR.this).setFormYear(DateIntent);
                setSpinnerOverdue();
                CallAR();
                Offset=0;
                TotalRowPage=8;
                Page=1;
                initTypeAr="Default";
            }

        }
    }

    public void showYearDialog() {

        final Dialog d = new Dialog(ActivityAR.this);
        d.requestWindowFeature(Window.FEATURE_NO_TITLE);
        d.setContentView(R.layout.dialog_month_year);
        Button set = (Button) d.findViewById(R.id.btnOk);
        Button cancel = (Button) d.findViewById(R.id.btnCancel);
        TextView year_text = (TextView) d.findViewById(R.id.year_text);
        year_text.setText("Set Month and Year");
        final NumberPicker monthpicker = (NumberPicker) d.findViewById(R.id.picker_month);
        final NumberPicker yearpicker = (NumberPicker) d.findViewById(R.id.picker_year);

        monthpicker.setMaxValue(12);
        monthpicker.setMinValue(1);
        monthpicker.setWrapSelectorWheel(false);
        monthpicker.setValue(month);
        monthpicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);


        yearpicker.setMinValue(2016);
        yearpicker.setMaxValue(2025);
        yearpicker.setWrapSelectorWheel(false);
        yearpicker.setValue(year);

        set.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Months = String.valueOf(monthpicker.getValue() < 10 ? ("0" + monthpicker.getValue()) : (monthpicker.getValue()));
                String Years = String.valueOf(yearpicker.getValue());
                new SharePreference(ActivityAR.this).setRefreshMonth(Months);
                new SharePreference(ActivityAR.this).setRefreshYear(Years);
               // Toast.makeText(ActivityAR.this, Months+" "+Years, Toast.LENGTH_SHORT).show();
                GetDataARMonthYear();
                d.dismiss();
            }
        });
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                d.dismiss();
            }
        });
        d.show();
    }

}
