package com.indoagri.treasuryapps.widget.calendar.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.indoagri.treasuryapps.R;

public class ActivityCoba extends AppCompatActivity implements CustomCalendar.CalendarListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coba_calendar);
        CustomCalendar custom = findViewById(R.id.mycalendar);
        custom.setCalendarListener(this);
        custom.setEventDate("2019-09-16", "Meeting 1", "All Day");
        custom.refresh();


    }

    @Override
    public void onDayClick(String date) {
        Toast.makeText(getApplicationContext(), "Anda Memilih Tanggal " + date.substring(0,10), Toast.LENGTH_SHORT).show();
    }
}
