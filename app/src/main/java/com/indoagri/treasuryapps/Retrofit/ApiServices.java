package com.indoagri.treasuryapps.Retrofit;

import android.content.Context;


import com.indoagri.treasuryapps.Apps;
import com.indoagri.treasuryapps.Retrofit.Model.UserModel;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface  ApiServices {
    Context context = Apps.getAppContext();
    @FormUrlEncoded
    @POST("account/loginpermission")
    Call<UsersLoginResponse> LoginPermission(@Header("X-API-KEY") String xapikey,
                                             @Field("userName") String username,
                                             @Field("userPassword") String password,
                                             @Field("Domain") String domain,
                                             @Field("DeviceID") String deviceid);

    @FormUrlEncoded
    @POST("account/logoutpermission")
    Call<UsersLoginResponse> LogoutPermission(@Header("X-API-KEY") String xapikey,
                                              @Field("userName") String username,
                                              @Field("userPassword") String password,
                                              @Field("Domain") String domain,
                                              @Field("DeviceID") String deviceid);

    @GET("treasuryar/current")
    Call<TreasuryARResponse> getAllAR();
    @GET("treasuryar/currentmonth")
    Call<TreasuryARResponse> getThisMonthAR();
    @GET("permission/checkapp") //i.e https://api.test.com/Search?
    Call<AppDetailResponse> getAppDetail(@Query("userad") String userad,
                                         @Query("app") String app);
    @GET("treasuryar/getmonthyear") //i.e https://api.test.com/Search?
    Call<TreasuryARResponse> getMonthYear(@Query("month") String month,
                                         @Query("year") String year);
}
