package com.indoagri.treasuryapps.Retrofit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indoagri.treasuryapps.Retrofit.Model.AppDetailModel;

public class AppDetailResponse {
    @SerializedName("appDetail")
    @Expose
    AppDetailModel appDetailModel;

    public AppDetailModel getAPP() {
        return appDetailModel;
    }
}
