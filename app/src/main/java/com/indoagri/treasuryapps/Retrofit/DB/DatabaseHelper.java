package com.indoagri.treasuryapps.Retrofit.DB;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import com.indoagri.treasuryapps.Common.Constants;
import com.indoagri.treasuryapps.Retrofit.Model.AppDetailModel;
import com.indoagri.treasuryapps.Retrofit.Model.TreasuryArModel;
import com.indoagri.treasuryapps.Retrofit.Model.UserModel;

/**
 * Created by Dwi.Fajar on 4/2/2018.
 */


public class DatabaseHelper extends SQLiteOpenHelper {

    public final static int dbVersion = Constants.db_version;
    public final static String dbName = Constants.db_name;
    Context context;
    SQLiteDatabase db;
    public DatabaseHelper(Context context) {
        super(context, dbName, null, dbVersion);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query;
        query = "create table " + UserModel.TABLE_NAME+
                "(" +
                UserModel.XML_ID+" INTEGER PRIMARY KEY AUTOINCREMENT, " +
                UserModel.XML_USERID+ " text, "+
                UserModel.XML_USERNAME+ " text, "+
                UserModel.XML_SURNAME+ " text, "+
                UserModel.XML_EMAIL+ " text, "+
                UserModel.XML_LDAP+ " text, "+
                UserModel.XML_DOMAIN+ " text, "+
                UserModel.XML_USERDESCRIPTION+ " text, "+
                UserModel.XML_COMPANY+ " text, "+
                UserModel.XML_PROVINCE+ " text, "+
                UserModel.XML_DEPARTMENT+ " text, "+
                UserModel.XML_EMPLOYEEID+ " text, "+
                UserModel.XML_USERFULLNAME+ " text,"+
                UserModel.XML_USERPHONE+ " text, "+
                UserModel.XML_USERPASSWORD+ " text, "+
                UserModel.XML_USERKEY+ " text "+
                ")";
        db.execSQL(query);

        query = "create table " + TreasuryArModel.TABLE_NAME+
                "(" +
                TreasuryArModel.XML_IDX+" text PRIMARY KEY, " +
                TreasuryArModel.XML_GLACCOUNT+ " text, "+
                TreasuryArModel.XML_BA+ " text, "+
                TreasuryArModel.XML_CUSTOMERCODE+ " text, "+
                TreasuryArModel.XML_CUSTOMERNAME+ " text, "+
                TreasuryArModel.XML_ACCOUTNNAME+ " text, "+
                TreasuryArModel.XML_GROUPNONGROUP+ " text, "+
                TreasuryArModel.XML_COMPANYGROUP+ " text, "+
                TreasuryArModel.XML_CURRENCY+ " text, "+
                TreasuryArModel.XML_CUSTOMERGROUP+ " text, "+
                TreasuryArModel.XML_CURRENTDEBT+ " text, "+
                TreasuryArModel.XML_OVERDUE10+ " text, "+
                TreasuryArModel.XML_OVERDUE20+ " text,"+
                TreasuryArModel.XML_OVERDUE30+ " text, "+
                TreasuryArModel.XML_OVERDUE60+ " text, "+
                TreasuryArModel.XML_OVERDUE90+ " text, "+
                TreasuryArModel.XML_OVERDUE99+ " text, "+
                TreasuryArModel.XML_OVERDUETOTAL+ " text, "+
                TreasuryArModel.XML_CREATEDDATE+ " text, "+
                TreasuryArModel.XML_LASTYEAR+ " text "+
                ")";
        db.execSQL(query);
        query = "create table " + AppDetailModel.TABLE_NAME+
                "(" +
                AppDetailModel.XML_APPID+" text, " +
                AppDetailModel.XML_APPNAME+ " text, "+
                AppDetailModel.XML_APPDESC+ " text,"+
                AppDetailModel.XML_APPPACKAGE+ " text, "+
                AppDetailModel.XML_VERSIONCODE+ " text,"+
                AppDetailModel.XML_VERSIONNAME+ " text, "+
                AppDetailModel.XML_APKURL+ " text,"+
                AppDetailModel.XML_ASSETSICON+ " text, "+
                AppDetailModel.XML_ASSETSICONURL+ " text,"+
                AppDetailModel.XML_ACTIVE+ " text, "+
                AppDetailModel.XML_GROUPLEVEL+ " text,"+
                AppDetailModel.XML_USERAD+ " text "+
                ")";
        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int versionOld, int versionNew) {
        String query;
        try{
            switch(versionOld){


            }

        }catch(SQLiteException e){
            e.printStackTrace();
        }
    }


}