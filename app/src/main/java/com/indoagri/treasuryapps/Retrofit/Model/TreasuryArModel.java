package com.indoagri.treasuryapps.Retrofit.Model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class TreasuryArModel {

    public static String TABLE_NAME = "TREASURY_AR";
    public static String XML_IDX = "idx";
    public static String XML_GLACCOUNT= "HKONT";
    public static String XML_BA= "GSBER";
    public static String XML_CUSTOMERCODE= "KUNNR";
    public static String XML_CUSTOMERNAME= "NAME1";
    public static String XML_ACCOUTNNAME= "TXT50";
    public static String XML_GROUPNONGROUP= "KVGR5";
    public static String XML_COMPANYGROUP= "BUKRS";
    public static String XML_CURRENCY= "WAERS";
    public static String XML_CUSTOMERGROUP= "BEZEI";
    public static String XML_CURRENTDEBT= "DEB00";
    public static String XML_OVERDUE10= "DEB10";
    public static String XML_OVERDUE20= "DEB20";
    public static String XML_OVERDUE30= "DEB30";
    public static String XML_OVERDUE60= "DEB60";
    public static String XML_OVERDUE90= "DEB90";
    public static String XML_OVERDUE99= "DEB99";
    public static String XML_OVERDUETOTAL= "DEBTT";
    public static String XML_CREATEDDATE= "ERDAT";
    public static String XML_LASTYEAR= "LastYear";
    @SerializedName("idx")
    @Expose
    private int idx;
    @SerializedName("erdat")
    @Expose
    private String erdat;
    @SerializedName("waers")
    @Expose
    private String waers;
    @SerializedName("bukrs")
    @Expose
    private String bukrs;
    @SerializedName("gsber")
    @Expose
    private String gsber;
    @SerializedName("hkont")
    @Expose
    private String hkont;
    @SerializedName("kunnr")
    @Expose
    private String kunnr;
    @SerializedName("namE1")
    @Expose
    private String namE1;
    @SerializedName("txT50")
    @Expose
    private String txT50;
    @SerializedName("kvgR5")
    @Expose
    private String kvgR5;
    @SerializedName("bezei")
    @Expose
    private String bezei;
    @SerializedName("deB00")
    @Expose
    private String deB00;
    @SerializedName("deB10")
    @Expose
    private String deB10;
    @SerializedName("deB20")
    @Expose
    private String deB20;
    @SerializedName("deB30")
    @Expose
    private String deB30;
    @SerializedName("deB60")
    @Expose
    private String deB60;
    @SerializedName("deB90")
    @Expose
    private String deB90;
    @SerializedName("deB99")
    @Expose
    private String deB99;
    @SerializedName("debtt")
    @Expose
    private String debtt;

    private String lastyear;

    public TreasuryArModel() {
    }

    public int getIdx() {
        return idx;
    }

    public void setIdx(int idx) {
        this.idx = idx;
    }

    public String getErdat() {
        return erdat;
    }

    public void setErdat(String erdat) {
        this.erdat= erdat;
    }

    public String getWaers() {
        return waers;
    }

    public void setWaers(String waers) {
        this.waers = waers;
    }

    public String getBukrs() {
        return bukrs;
    }

    public void setBukrs(String bukrs) {
        this.bukrs = bukrs;
    }

    public String getGsber() {
        return gsber;
    }

    public void setGsber(String gsber) {
        this.gsber = gsber;
    }

    public String getHkont() {
        return hkont;
    }

    public void setHkont(String hkont) {
        this.hkont = hkont;
    }

    public String getKunnr() {
        return kunnr;
    }

    public void setKunnr(String kunnr) {
        this.kunnr = kunnr;
    }

    public String getNamE1() {
        return namE1;
    }

    public void setNamE1(String namE1) {
        this.namE1 = namE1;
    }

    public String getTxT50() {
        return txT50;
    }

    public void setTxT50(String txT50) {
        this.txT50 = txT50;
    }

    public String getKvgR5() {
        return kvgR5;
    }

    public void setKvgR5(String kvgR5) {
        this.kvgR5 = kvgR5;
    }

    public String getBezei() {
        return bezei;
    }

    public void setBezei(String bezei) {
        this.bezei = bezei;
    }

    public String getDeB00() {
        return deB00;
    }

    public void setDeB00(String deB00) {
        this.deB00 = deB00;
    }

    public String getDeB10() {
        return deB10;
    }

    public void setDeB10(String deB10) {
        this.deB10 = deB10;
    }

    public String getDeB20() {
        return deB20;
    }

    public void setDeB20(String deB20) {
        this.deB20 = deB20;
    }

    public String getDeB30() {
        return deB30;
    }

    public void setDeB30(String deB30) {
        this.deB30 = deB30;
    }

    public String getDeB60() {
        return deB60;
    }

    public void setDeB60(String deB60) {
        this.deB60 = deB60;
    }

    public String getDeB90() {
        return deB90;
    }

    public void setDeB90(String deB90) {
        this.deB90 = deB90;
    }

    public String getDeB99() {
        return deB99;
    }

    public void setDeB99(String deB99) {
        this.deB99 = deB99;
    }

    public String getDebtt() {
        return debtt;
    }

    public void setDebtt(String debtt) {
        this.debtt = debtt;
    }

    public String getLastyear() {
        return lastyear;
    }

    public void setLastyear(String lastyear) {
        this.lastyear = lastyear;
    }
}
