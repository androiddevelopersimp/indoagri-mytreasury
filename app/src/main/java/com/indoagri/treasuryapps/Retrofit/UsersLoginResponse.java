package com.indoagri.treasuryapps.Retrofit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indoagri.treasuryapps.Retrofit.Model.UserModel;

public class UsersLoginResponse {
        @SerializedName("userDetail")
        @Expose
        UserModel userModel;

        public UserModel getUserLogin() {
            return userModel;
        }
        public UserModel getUserLogout() {
            return userModel;
        }
}
