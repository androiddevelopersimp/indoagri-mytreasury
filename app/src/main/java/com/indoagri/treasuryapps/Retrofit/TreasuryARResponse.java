package com.indoagri.treasuryapps.Retrofit;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.indoagri.treasuryapps.Retrofit.Model.TreasuryArModel;

import java.util.List;

public class TreasuryARResponse {
    @SerializedName("treasuryAr")
    @Expose
    List<TreasuryArModel> treasuryArModels;

    public List getTreasury() {
        return treasuryArModels;
    }
}
