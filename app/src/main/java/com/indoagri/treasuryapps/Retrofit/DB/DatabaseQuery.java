package com.indoagri.treasuryapps.Retrofit.DB;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.widget.Toast;


import com.indoagri.treasuryapps.Retrofit.Model.AppDetailModel;
import com.indoagri.treasuryapps.Retrofit.Model.TreasuryArModel;
import com.indoagri.treasuryapps.Retrofit.Model.UserModel;

import java.util.ArrayList;
import java.util.List;

public class DatabaseQuery {
    private Context context;
    private SQLiteDatabase db;
    private DatabaseHelper helper;

    public DatabaseQuery(Context context) {
        this.context = context;
        this.helper = new DatabaseHelper(context);
    }

    public void openTransaction() throws SQLException {
        try {
            db = helper.getWritableDatabase();
            db.beginTransaction();
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
    }

    public void closeTransaction() {
        try {
            if(db.isOpen()){
                db.endTransaction();
                helper.close();
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
    }

    public void commitTransaction() throws SQLException {
        try {
            db.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
    }

    public int COUNT(String TABLE, String statement) {
        int count = 0;
        db = helper.getReadableDatabase();
        Cursor cursor = null;
        String query = statement;

        cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        count = cursor.getCount();
        cursor.close();

        return count;
    }
    public void UPDATEQUERY(String TABLE, String statement) {
        db = helper.getWritableDatabase();
        db.beginTransaction();
        String query = statement;
        try{
            db.execSQL(query);
            db.setTransactionSuccessful();
        } catch(Exception e) {
            Toast.makeText(context,e.getMessage(), Toast.LENGTH_SHORT).show();
            //Error in between database transaction
        } finally {
            db.endTransaction();
        }

    }
    public void INSERTQUERY(String TABLE, String statement) {
        db = helper.getWritableDatabase();
        db.beginTransaction();
        String query = statement;
        try{
            db.execSQL(query);
            db.setTransactionSuccessful();
        } catch(Exception e) {
            e.getMessage();
            Toast.makeText(context,e.getMessage(), Toast.LENGTH_SHORT).show();
            //Error in between database transaction
        } finally {
            db.endTransaction();
        }
    }

    public int insertDataSQL(String tableName, ContentValues values) {

        int result = 0;
        try{
            /*sqliteDatabase.rawQuery(query,bindArgs);*/
            db.insertOrThrow(tableName, null, values);
            result = 1;
        }
        catch(Exception e)
        {
            result = 0;
            e.printStackTrace();
        }

        return result;
    }
    public boolean DELETEEXISTING(String TABLE, String statement) {
        //openTransaction();
        db = helper.getWritableDatabase();
        db.beginTransaction();
        boolean deleteStatus = false;
        String query = statement;
        try{
            db.execSQL(query);
            deleteStatus=true;
        }
        catch (Exception e){
            deleteStatus=false;
            e.getMessage();
        }
        try {
            db.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
        try {
            if(db.isOpen()){
                db.endTransaction();
                helper.close();
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
        return deleteStatus;
    }
    public boolean DELETETABLE(String TABLE, String statement) {
        db = helper.getWritableDatabase();
        db.beginTransaction();
        boolean in = false;
        String query = statement;
        try {
            db.execSQL(query);
            in =true;
            db.setTransactionSuccessful();
        } catch (SQLiteException e) {
            e.printStackTrace();
            in = false;
        }
        try {
            if(db.isOpen()){
                db.endTransaction();
                helper.close();
            }
        } catch (SQLiteException e) {
            e.printStackTrace();
        }
        return in;
    }

    public int deleteDataTemporary(String tableName, String whereClause, String[] whereArgs) {

        try {
            int rowId = db.delete(tableName, whereClause, whereArgs);
            return rowId;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return 0;
    }
    public void deleteDataTable(String tableName, String whereClause, String[] whereArgs) {

        try {
            db.execSQL("DELETE FROM sqlite_sequence WHERE name= '"+tableName+"'");
          //  db.execSQL("VACUUM");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public Object getDataFirstRaw(String sqldb_query, String tableName, String[]a) {

        List<Object> listObject = getListDataRawQuery(sqldb_query,tableName,a);

        if (listObject.size() > 0) {
            return listObject.get(0);
        } else {
            return null;
        }
    }
    public List<Object> getListDataRawQuery(String sqldb_query, String tableName, String[]a) {

        Cursor cursor = null;
        List<Object> listObject = new ArrayList<Object>();

        try {
            cursor = db.rawQuery(sqldb_query,a);
            if (tableName.equals(TreasuryArModel.TABLE_NAME)) {
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        TreasuryArModel sb = new TreasuryArModel();
                        for (int i = 0; i < cursor.getColumnCount(); i++) {
                            if (cursor.getColumnName(i).equalsIgnoreCase(TreasuryArModel.XML_IDX)) {
                                sb.setIdx(cursor.getInt(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(TreasuryArModel.XML_GLACCOUNT)) {
                                sb.setHkont(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(TreasuryArModel.XML_BA)) {
                                sb.setGsber(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(TreasuryArModel.XML_CUSTOMERCODE)) {
                                sb.setKunnr(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(TreasuryArModel.XML_CUSTOMERNAME)) {
                                sb.setNamE1(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TreasuryArModel.XML_ACCOUTNNAME)) {
                                sb.setTxT50(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TreasuryArModel.XML_GROUPNONGROUP)) {
                                sb.setKvgR5(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TreasuryArModel.XML_COMPANYGROUP)) {
                                sb.setBukrs(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TreasuryArModel.XML_CURRENCY)) {
                                sb.setWaers(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TreasuryArModel.XML_CUSTOMERGROUP)) {
                                sb.setBezei(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TreasuryArModel.XML_CURRENTDEBT)) {
                                sb.setDeB00(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TreasuryArModel.XML_OVERDUE10)) {
                                sb.setDeB10(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TreasuryArModel.XML_OVERDUE20)) {
                                sb.setDeB20(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TreasuryArModel.XML_OVERDUE30)) {
                                sb.setDeB30(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TreasuryArModel.XML_OVERDUE60)) {
                                sb.setDeB60(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TreasuryArModel.XML_OVERDUE90)) {
                                sb.setDeB90(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TreasuryArModel.XML_OVERDUE99)) {
                                sb.setDeB99(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TreasuryArModel.XML_OVERDUETOTAL)) {
                                sb.setDebtt(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TreasuryArModel.XML_CREATEDDATE)) {
                                sb.setErdat(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(TreasuryArModel.XML_LASTYEAR)) {
                                sb.setLastyear(cursor.getString(i));
                            }
                        }

                        listObject.add(sb);
                    } while (cursor.moveToNext());
                }
            }else if (tableName.equals(UserModel.TABLE_NAME)) {
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        UserModel sb = new UserModel();
                        for (int i = 0; i < cursor.getColumnCount(); i++) {
                            if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_USERID)) {
                                sb.setUSERID(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_USERNAME)) {
                                sb.setUSERNAME(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_SURNAME)) {
                                sb.setSURNAME(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_EMAIL)) {
                                sb.setEMAIL(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_LDAP)) {
                                sb.setLDAP(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_DOMAIN)) {
                                sb.setDOMAIN(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_USERDESCRIPTION)) {
                                sb.setDESCRIPTION(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_COMPANY)) {
                                sb.setCOMPANY(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_PROVINCE)) {
                                sb.setPROVINCE(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_DEPARTMENT)) {
                                sb.setDEPARTMENT(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_EMPLOYEEID)) {
                                sb.setEMPLOYEEID(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_USERPHONE)) {
                                sb.setPHONE(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_USERPASSWORD)) {
                                sb.setPASSWORD(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_USERKEY)) {
                                sb.setKEY(cursor.getString(i));
                            }
                        }

                        listObject.add(sb);
                    } while (cursor.moveToNext());
                }
            }else if (tableName.equals(AppDetailModel.TABLE_NAME)) {
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        AppDetailModel sb = new AppDetailModel();
                        for (int i = 0; i < cursor.getColumnCount(); i++) {
                            if (cursor.getColumnName(i).equalsIgnoreCase(AppDetailModel.XML_APPID)) {
                                sb.setID(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(AppDetailModel.XML_APPNAME)) {
                                sb.setAPPNAME(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(AppDetailModel.XML_APPDESC)) {
                                sb.setAPPDESCRIPTION(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(AppDetailModel.XML_APPPACKAGE)) {
                                sb.setAPPPACKAGE(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(AppDetailModel.XML_VERSIONCODE)) {
                                sb.setVERSIONCODE(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(AppDetailModel.XML_VERSIONNAME)) {
                                sb.setVERSIONNAME(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(AppDetailModel.XML_APKURL)) {
                                sb.setAPKURL(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(AppDetailModel.XML_ASSETSICON)) {
                                sb.setASSETSICON(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(AppDetailModel.XML_ASSETSICONURL)) {
                                sb.setASSETSICONURL(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(AppDetailModel.XML_ACTIVE)) {
                                sb.setACTIVE(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(AppDetailModel.XML_GROUPLEVEL)) {
                                sb.setGROUPLEVEL(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(AppDetailModel.XML_USERAD)) {
                                sb.setUSERAD(cursor.getString(i));
                            }
                        }

                        listObject.add(sb);
                    } while (cursor.moveToNext());
                }
            }else if (tableName.equals(UserModel.TABLE_NAME)) {
                if (cursor != null && cursor.moveToFirst()) {
                    do {
                        UserModel sb = new UserModel();
                        for (int i = 0; i < cursor.getColumnCount(); i++) {
                            if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_USERID)) {
                                sb.setUSERID(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_USERNAME)) {
                                sb.setUSERNAME(cursor.getString(i));
                            } else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_SURNAME)) {
                                sb.setSURNAME(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_EMAIL)) {
                                sb.setEMAIL(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_LDAP)) {
                                sb.setLDAP(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_DOMAIN)) {
                                sb.setDOMAIN(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_USERDESCRIPTION)) {
                                sb.setDESCRIPTION(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_COMPANY)) {
                                sb.setCOMPANY(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_PROVINCE)) {
                                sb.setPROVINCE(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_DEPARTMENT)) {
                                sb.setDEPARTMENT(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_EMPLOYEEID)) {
                                sb.setEMPLOYEEID(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_USERFULLNAME)) {
                                sb.setFULLNAME(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_USERPHONE)) {
                                sb.setPHONE(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_USERPASSWORD)) {
                                sb.setPASSWORD(cursor.getString(i));
                            }else if (cursor.getColumnName(i).equalsIgnoreCase(UserModel.XML_USERKEY)) {
                                sb.setKEY(cursor.getString(i));
                            }
                        }

                        listObject.add(sb);
                    } while (cursor.moveToNext());
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }finally {
            cursor.close();
        }

        return listObject;
    }



    public long setData(Object object) {
        String tableName = null;
        ContentValues values = new ContentValues();
        if(object.getClass().getName().equals(TreasuryArModel.class.getName())) {
            TreasuryArModel md = (TreasuryArModel) object;
            tableName = TreasuryArModel.TABLE_NAME;
            values.put(TreasuryArModel.XML_IDX, md.getIdx());
            values.put(TreasuryArModel.XML_GLACCOUNT, md.getHkont());
            values.put(TreasuryArModel.XML_BA, md.getGsber());
            values.put(TreasuryArModel.XML_CUSTOMERCODE, md.getKunnr());
            values.put(TreasuryArModel.XML_CUSTOMERNAME, md.getNamE1());
            values.put(TreasuryArModel.XML_ACCOUTNNAME, md.getTxT50());
            values.put(TreasuryArModel.XML_GROUPNONGROUP, md.getKvgR5());
            values.put(TreasuryArModel.XML_COMPANYGROUP, md.getBukrs());
            values.put(TreasuryArModel.XML_CURRENCY, md.getWaers());
            values.put(TreasuryArModel.XML_CUSTOMERGROUP, md.getBezei());
            values.put(TreasuryArModel.XML_CURRENTDEBT, md.getDeB00());
            values.put(TreasuryArModel.XML_OVERDUE10, md.getDeB10());
            values.put(TreasuryArModel.XML_OVERDUE20, md.getDeB20());
            values.put(TreasuryArModel.XML_OVERDUE30, md.getDeB30());
            values.put(TreasuryArModel.XML_OVERDUE60, md.getDeB60());
            values.put(TreasuryArModel.XML_OVERDUE90, md.getDeB90());
            values.put(TreasuryArModel.XML_OVERDUE99, md.getDeB99());
            values.put(TreasuryArModel.XML_OVERDUETOTAL, md.getDebtt());
            values.put(TreasuryArModel.XML_CREATEDDATE, md.getErdat());
            values.put(TreasuryArModel.XML_LASTYEAR, md.getLastyear());
        }

        long rowId = 0;

        try{
            rowId = db.insertOrThrow(tableName, null, values);
        }catch(Exception e){
            e.printStackTrace();
        }

        return rowId;
    }


}